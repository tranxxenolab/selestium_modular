#ifndef _W4VES_INTERFACE_H
#define _W4VES_INTERFACE_H

extern ADC *adc;

extern int ADC0_MAX;
extern int ADC1_MAX;


extern uint8_t CV_PIN_LIST[NUM_PARAMS];
extern uint8_t muxChannel[16][4];
extern uint16_t mux01Values[NUM_MUX01_CHANNELS];

extern int8_t prevOct[NUM_VOICES];

void setupDigitalPins();
void setupTriggers();
int processPot(int adcPin);
void processSwitches();
void processPots();
void processCVs();
void processTriggers();
void readMuxes();
void readMux01();
void readMux02();
void printSwitchValues();
void printPotValues();
uint16_t readMux01Channel(uint8_t channel);
uint16_t readMux02Channel(uint8_t channel);

#endif