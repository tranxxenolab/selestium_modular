#ifndef _W4VES_SYNTHESIS_H
#define _W4VES_SYNTHESIS_H

/*
#include <Audio.h>
#include <Wire.h>
#include <SPI.h>
#include <SD.h>
#include <SerialFlash.h>

// GUItool: begin automatically generated code
AudioSynthWaveform       waveform3; //xy=106.20001220703125,460.1999969482422
AudioSynthWaveform       waveform4;  //xy=116.20001220703125,515.1999969482422
AudioSynthWaveform       waveform1;      //xy=119.20001220703125,131.1999969482422
AudioSynthWaveform       waveform2;      //xy=129.20001220703125,186.1999969482422
AudioSynthWaveformModulated waveformMod4; //xy=270.20001220703125,682.2000122070312
AudioSynthWaveformModulated waveformMod3; //xy=271.20001220703125,631.2000122070312
AudioSynthWaveformModulated waveformMod2; //xy=283.20001220703125,353.20001220703125
AudioSynthWaveformModulated waveformMod1;   //xy=284.20001220703125,302.20001220703125
AudioMixer4              mixer3; //xy=289.20001220703125,453.1999969482422
AudioMixer4              mixer1;         //xy=302.20001220703125,124.19999694824219
AudioMixer4              mixer4; //xy=461.20001220703125,643.2000122070312
AudioMixer4              mixer2; //xy=474.20001220703125,314.20001220703125
AudioOutputPT8211        pt8211_1;       //xy=707.1999969482422,236.1999969482422
AudioOutputPT8211_2      pt8211_2_1;     //xy=737.1999969482422,614.1999969482422
AudioConnection          patchCord1(waveform3, 0, mixer3, 0);
AudioConnection          patchCord2(waveform4, 0, mixer3, 1);
AudioConnection          patchCord3(waveform1, 0, mixer1, 0);
AudioConnection          patchCord4(waveform2, 0, mixer1, 1);
AudioConnection          patchCord5(waveformMod4, 0, mixer4, 1);
AudioConnection          patchCord6(waveformMod3, 0, mixer4, 0);
AudioConnection          patchCord7(waveformMod2, 0, mixer2, 1);
AudioConnection          patchCord8(waveformMod1, 0, mixer2, 0);
AudioConnection          patchCord9(mixer3, 0, waveformMod3, 0);
AudioConnection          patchCord10(mixer3, 0, waveformMod4, 0);
AudioConnection          patchCord11(mixer3, 0, pt8211_2_1, 0);
AudioConnection          patchCord12(mixer1, 0, waveformMod1, 0);
AudioConnection          patchCord13(mixer1, 0, waveformMod2, 0);
AudioConnection          patchCord14(mixer1, 0, pt8211_1, 0);
AudioConnection          patchCord15(mixer4, 0, pt8211_2_1, 1);
AudioConnection          patchCord16(mixer2, 0, pt8211_1, 1);
// GUItool: end automatically generated code

*/

/*
#include <Audio.h>
#include <Wire.h>
#include <SPI.h>
#include <SD.h>
#include <SerialFlash.h>

// GUItool: begin automatically generated code
AudioSynthWaveform       waveform3;      //xy=245.1999969482422,725.0000152587891
AudioSynthWaveform       waveform4;      //xy=255.1999969482422,780.0000152587891
AudioSynthWaveform       waveform1;      //xy=258.1999969482422,396.00001525878906
AudioSynthWaveform       waveform2;      //xy=268.1999969482422,451.00001525878906
AudioSynthWaveformModulated waveformMod4;   //xy=409.1999969482422,947.0000152587891
AudioSynthWaveformModulated waveformMod3;   //xy=410.1999969482422,896.0000152587891
AudioSynthWaveformModulated waveformMod2;   //xy=422.1999969482422,618.0000152587891
AudioSynthWaveformModulated waveformMod1;   //xy=423.1999969482422,567.0000152587891
AudioMixer4              mixer3;         //xy=428.1999969482422,718.0000152587891
AudioMixer4              mixer1;         //xy=441.1999969482422,389.00001525878906
AudioSynthWaveformDc     dc1;            //xy=542.2000122070312,488.20001220703125
AudioMixer4              mixer4;         //xy=600.1999969482422,908.0000152587891
AudioMixer4              mixer2;         //xy=613.1999969482422,579.0000152587891
AudioEffectWaveFolder    wavefolder1;    //xy=685.2000122070312,436.20001220703125
AudioOutputPT8211        pt8211_1;       //xy=846.1999969482422,501.00001525878906
AudioOutputPT8211_2      pt8211_2_1;     //xy=876.1999969482422,879.0000152587891
AudioConnection          patchCord1(waveform3, 0, mixer3, 0);
AudioConnection          patchCord2(waveform4, 0, mixer3, 1);
AudioConnection          patchCord3(waveform1, 0, mixer1, 0);
AudioConnection          patchCord4(waveform2, 0, mixer1, 1);
AudioConnection          patchCord5(waveformMod4, 0, mixer4, 1);
AudioConnection          patchCord6(waveformMod3, 0, mixer4, 0);
AudioConnection          patchCord7(waveformMod2, 0, mixer2, 1);
AudioConnection          patchCord8(waveformMod1, 0, mixer2, 0);
AudioConnection          patchCord9(mixer3, 0, waveformMod3, 0);
AudioConnection          patchCord10(mixer3, 0, waveformMod4, 0);
AudioConnection          patchCord11(mixer3, 0, pt8211_2_1, 0);
AudioConnection          patchCord12(mixer1, 0, waveformMod1, 0);
AudioConnection          patchCord13(mixer1, 0, waveformMod2, 0);
AudioConnection          patchCord14(mixer1, 0, wavefolder1, 0);
AudioConnection          patchCord15(dc1, 0, wavefolder1, 1);
AudioConnection          patchCord16(mixer4, 0, pt8211_2_1, 1);
AudioConnection          patchCord17(mixer2, 0, pt8211_1, 1);
AudioConnection          patchCord18(wavefolder1, 0, pt8211_1, 0);
// GUItool: end automatically generated code


*/

// GUItool: begin automatically generated code
/*
extern AudioSynthWaveform       waveform3; //xy=106.20001220703125,460.1999969482422
extern AudioSynthWaveform       waveform4;  //xy=116.20001220703125,515.1999969482422
extern AudioSynthWaveform       waveform1;      //xy=119.20001220703125,131.1999969482422
extern AudioSynthWaveform       waveform2;      //xy=129.20001220703125,186.1999969482422
extern AudioSynthWaveformModulated waveformMod4; //xy=270.20001220703125,682.2000122070312
extern AudioSynthWaveformModulated waveformMod3; //xy=271.20001220703125,631.2000122070312
extern AudioSynthWaveformModulated waveformMod2; //xy=283.20001220703125,353.20001220703125
extern AudioSynthWaveformModulated waveformMod1;   //xy=284.20001220703125,302.20001220703125
extern AudioMixer4              mixer3; //xy=289.20001220703125,453.1999969482422
extern AudioMixer4              mixer1;         //xy=302.20001220703125,124.19999694824219
extern AudioMixer4              mixer4; //xy=461.20001220703125,643.2000122070312
extern AudioMixer4              mixer2; //xy=474.20001220703125,314.20001220703125
extern AudioOutputPT8211        pt8211_1;       //xy=707.1999969482422,236.1999969482422
extern AudioOutputPT8211_2      pt8211_2_1;     //xy=737.1999969482422,614.1999969482422
// GUItool: end automatically generated code
*/

/*
// GUItool: begin automatically generated code
extern AudioSynthWaveform       waveform3;      //xy=245.1999969482422,725.0000152587891
extern AudioSynthWaveform       waveform4;      //xy=255.1999969482422,780.0000152587891
extern AudioSynthWaveform       waveform1;      //xy=258.1999969482422,396.00001525878906
extern AudioSynthWaveform       waveform2;      //xy=268.1999969482422,451.00001525878906
extern AudioSynthWaveformModulated waveformMod4;   //xy=409.1999969482422,947.0000152587891
extern AudioSynthWaveformModulated waveformMod3;   //xy=410.1999969482422,896.0000152587891
extern AudioSynthWaveformModulated waveformMod2;   //xy=422.1999969482422,618.0000152587891
extern AudioSynthWaveformModulated waveformMod1;   //xy=423.1999969482422,567.0000152587891
extern AudioMixer4              mixer3;         //xy=428.1999969482422,718.0000152587891
extern AudioMixer4              mixer1;         //xy=441.1999969482422,389.00001525878906
extern AudioSynthWaveformDc     dc1;            //xy=542.2000122070312,488.20001220703125
extern AudioMixer4              mixer4;         //xy=600.1999969482422,908.0000152587891
extern AudioMixer4              mixer2;         //xy=613.1999969482422,579.0000152587891
extern AudioEffectWaveFolder    wavefolder1;    //xy=685.2000122070312,436.20001220703125
extern AudioOutputPT8211        pt8211_1;       //xy=846.1999969482422,501.00001525878906
extern AudioOutputPT8211_2      pt8211_2_1;     //xy=876.1999969482422,879.0000152587891
// GUItool: end automatically generated code
*/



/*
// GUItool: begin automatically generated code
extern AudioSynthWaveformModulated waveformMod1;   //xy=339.20001220703125,248.1999969482422
extern AudioSynthWaveformModulated waveformMod2; //xy=351.20001220703125,318.20001220703125
extern AudioOutputPT8211        pt8211_1;       //xy=708.2000122070312,300.20001220703125
// GUItool: end automatically generated code
*/

// MOST RECENT VERSION WITH MULTIPLE VOICES FOR MORPHING
// GUItool: begin automatically generated code
extern AudioSynthWaveform       wfSubVoice03; //xy=235.55557250976562,1640.666748046875
extern AudioSynthWaveform       wfSubVoice01;      //xy=240.22222137451172,667.9999504089355
extern AudioSynthWaveform       wfArb02Voice03; //xy=240.00001525878906,1550.6666870117188
extern AudioSynthWaveform       wfArb01Voice03; //xy=241.00001525878906,1510.6666870117188
extern AudioSynthWaveform       wfArb03Voice03; //xy=241.00001525878906,1590.6666870117188
extern AudioSynthWaveformModulated wfModArb01Voice04; //xy=241.00001525878906,1905.6666870117188
extern AudioSynthWaveformModulated wfModSinVoice04; //xy=243.00001525878906,1692.6666870117188
extern AudioSynthWaveform       wfSinVoice03;   //xy=245.00001525878906,1315.6666870117188
extern AudioSynthWaveform       wfArb02Voice01; //xy=248.00001525878906,580.6666870117188
extern AudioSynthWaveform       wfArb01Voice01; //xy=249.00001525878906,540.6666870117188
extern AudioSynthWaveform       wfArb03Voice01; //xy=249.00001525878906,620.6666870117188
extern AudioSynthWaveform       wfSawVoice03;   //xy=246.00001525878906,1405.6666870117188
extern AudioSynthWaveformModulated wfModArb02Voice04; //xy=244.00001525878906,1955.6666870117188
extern AudioSynthWaveformModulated wfModTriVoice04; //xy=245.00001525878906,1742.6666870117188
extern AudioSynthWaveformModulated wfModSawVoice04; //xy=245.00001525878906,1793.6666870117188
extern AudioSynthWaveformModulated wfModArb01Voice02; //xy=249.00001525878906,936.6666870117188
extern AudioSynthWaveform       wfPulVoice03;   //xy=248.00001525878906,1450.6666870117188
extern AudioSynthWaveformModulated wfModSinVoice02; //xy=251.00001525878906,722.6666870117188
extern AudioSynthWaveformModulated wfModPulVoice04; //xy=247.00001525878906,1841.6666870117188
extern AudioSynthWaveform       wfSawVoice01;   //xy=254.00003051757812,436.77779960632324
extern AudioSynthWaveformModulated wfModArb02Voice02; //xy=252.00001525878906,986.6666870117188
extern AudioSynthWaveformModulated wfModArb03Voice04; //xy=248.00001525878906,2008.6666870117188
extern AudioSynthWaveformModulated wfModTriVoice02; //xy=253.00001525878906,772.6666870117188
extern AudioSynthWaveformModulated wfModSawVoice02; //xy=253.00001525878906,823.6666870117188
extern AudioSynthWaveform       wfTriVoice03;   //xy=252.00001525878906,1360.6666870117188
extern AudioSynthWaveform       wfPulVoice01;   //xy=256.00001525878906,480.66668701171875
extern AudioSynthWaveformModulated wfModPulVoice02; //xy=255.00001525878906,871.6666870117188
extern AudioSynthWaveform       wfSubVoice02; //xy=254.4444580078125,1087.77783203125
extern AudioSynthWaveformModulated wfModArb03Voice02; //xy=256.00001525878906,1038.6666870117188
extern AudioSynthWaveform       wfSinVoice01;   //xy=259.6666793823242,356.6666793823242
extern AudioSynthWaveform       wfTriVoice01;   //xy=259.99998474121094,396.1111240386963
extern AudioSynthWaveform       wfSubVoice04; //xy=266.6666793823242,2058.2222003936768
extern AudioMixer4              mixPart02Voice03; //xy=450.00001525878906,1552.6666870117188
extern AudioMixer4              mixPart02Voice01; //xy=458.00001525878906,582.6666870117188
extern AudioMixer4              mixPart01Voice03; //xy=456.00001525878906,1420.6666870117188
extern AudioMixer4              mixPart01Voice01; //xy=464.00001525878906,451.66668701171875
extern AudioMixer4              mixPart01Voice04; //xy=460.00001525878906,1725.6666870117188
extern AudioMixer4              mixPart01Voice02; //xy=468.00001525878906,755.6666870117188
extern AudioMixer4              mixPart02Voice04; //xy=467.00001525878906,1909.6666870117188
extern AudioMixer4              mixPart02Voice02; //xy=475.00001525878906,939.6666870117188
extern AudioMixer4              mixVoice03;     //xy=638.0000152587891,1477.6666870117188
extern AudioMixer4              mixVoice01;     //xy=646.0000152587891,507.66668701171875
extern AudioMixer4              mixVoice04;     //xy=670.0000152587891,1750.6666870117188
extern AudioMixer4              mixVoice02;     //xy=678.0000152587891,780.6666870117188
extern AudioSynthWaveformDc     dcWavefolderVoice03; //xy=763.0000152587891,1624.6666870117188
extern AudioSynthWaveformDc     dcWavefolderVoice01; //xy=771.0000152587891,654.6666870117188
extern AudioSynthWaveformDc     dcWavefolderVoice04; //xy=782.0000152587891,1813.6666870117188
extern AudioSynthWaveformDc     dcWavefolderVoice02; //xy=790.0000152587891,843.6666870117188
extern AudioFilterStateVariable voice01Filter;        //xy=844.6667404174805,509.1110649108887
extern AudioEffectWaveFolder    wavefolderVoice04; //xy=979.0000152587891,1736.6666870117188
extern AudioEffectWaveFolder    wavefolderVoice02; //xy=987.0000152587891,766.6666870117188
extern AudioEffectWaveFolder    wavefolderVoice03; //xy=988.0000152587891,1605.6666870117188
extern AudioEffectWaveFolder    wavefolderVoice01; //xy=996.0000152587891,635.6666870117188
extern AudioOutputPT8211_2      pt8211_2_1;     //xy=1156.000015258789,1653.6666870117188
extern AudioOutputPT8211        pt8211_1;       //xy=1164.000015258789,695.6666870117188
// GUItool: end automatically generated code





extern float freqs[NUM_VOICES];
extern float octaves[NUM_VOICES];
extern AudioSynthWaveform* Voice01Waveforms[NUM_WAVEFORMS];
extern AudioSynthWaveform* Voice03Waveforms[NUM_WAVEFORMS];
extern AudioSynthWaveform* VoicesSubsWaveforms[NUM_VOICES];
extern AudioSynthWaveformModulated* Voice02Waveforms[NUM_WAVEFORMS];
extern AudioSynthWaveformModulated* Voice04Waveforms[NUM_WAVEFORMS];
extern float mixerWeights[NUM_VOICES][NUM_WAVEFORMS];
extern float defaultMixerWeights[NUM_VOICES][NUM_WAVEFORMS];
extern float phaseAngles[NUM_VOICES][NUM_WAVEFORMS];
extern int8_t defaultWaveformBreakpoints[NUM_WAVEFORMS+1];
extern float mixerWeightsExpanded[NUM_VOICES][NUM_MIXER_WEIGHTS_EXPANDED];
extern float defaultMixerWeightsExpanded[NUM_VOICES][NUM_MIXER_WEIGHTS_EXPANDED];

extern float hanningWindow10[11];
extern float hanningWindow20[21];
extern bool voicesSubsValues[NUM_VOICES];

void muteAll();
void unityAll();
void startSynthesis();
void updateWaveformParams();
void processW4ves();
void showProcessorUsage();
void setupWavefolderValues();
void setupMixerWeightsExpanded();
void clearMixerWeights();
void printMixerWeightsExpanded(int voice);
void printMixerWeights();
void calculateMixerWeights(int voice, int8_t value);
void updateMixerWeights();
void updateSubs();
void calculatePhaseAngles();


const int16_t AKWF_flute_0001_256_DATA [256] = 
{393, 1117, 1910, 2643, 3256, 3720, 4055, 4337, 4681, 5102, 5492, 5744, 5842, 5844, 5820, 5856, 6001, 6195, 6372, 6514, 6675, 6900, 7233, 7693, 8239, 8782, 9249, 9659, 10053, 10462, 10863, 11221, 11498, 11680, 11778, 11840, 11935, 12121, 12375, 12614, 12759, 12817, 12856, 12979, 13203, 13495, 13830, 14210, 14613, 15002, 15370, 15732, 16125, 16575, 17112, 17729, 18382, 19026, 19662, 20308, 20949, 21539, 22053, 22556, 23105, 23683, 24200, 24646, 25065, 25548, 26128, 26774, 27415, 28018, 28556, 29045, 29508, 29977, 30484, 31007, 31483, 31841, 32050, 32179, 32288, 32370, 32325, 32124, 31805, 31432, 30975, 30426, 29846, 29341, 28955, 28654, 28408, 28177, 27923, 27620, 27311, 27039, 26805, 26535, 26160, 25657, 25031, 24303, 23480, 22562, 21495, 20264, 18902, 17461, 15998, 14554, 13156, 11837, 10590, 9345, 8076, 6765, 5424, 4062, 2698, 1366, 120, -1027, -2057, -2963, -3740, -4379, -4893, -5308, -5638, -5871, -5983, -5990, -5955, -6011, -6213, -6506, -6767, -6965, -7168, -7450, -7822, -8264, -8765, -9320, -9917, -10548, -11196, -11821, -12374, -12814, -13127, -13326, -13426, -13486, -13570, -13720, -13905, -14095, -14292, -14542, -14824, -15094, -15335, -15581, -15873, -16212, -16561, -16882, -17160, -17410, -17662, -17945, -18243, -18509, -18732, -19001, -19392, -19883, -20350, -20736, -21122, -21653, -22355, -23109, -23767, -24320, -24847, -25430, -26104, -26848, -27581, -28202, -28694, -29062, -29325, -29469, -29517, -29508, -29476, -29455, -29484, -29598, -29786, -30019, -30266, -30529, -30807, -31082, -31308, -31450, -31503, -31490, -31422, -31289, -31070, -30733, -30247, -29612, -28872, -28073, -27242, -26413, -25615, -24854, -24119, -23411, -22719, -22043, -21414, -20854, -20366, -19899, -19369, -18738, -18035, -17254, -16374, -15362, -14258, -13109, -11922, -10655, -9319, -7942, -6552, -5168, -3794, -2502, -1299, -280};

const int16_t AKWF_epiano_0034_256_DATA [256] = 
{388, 1067, 1704, 2306, 2862, 3371, 3827, 4228, 4584, 4902, 5193, 5472, 5748, 6028, 6324, 6638, 6971, 7329, 7710, 8110, 8528, 8963, 9413, 9880, 10353, 10833, 11317, 11808, 12302, 12794, 13283, 13774, 14265, 14750, 15232, 15711, 16189, 16665, 17137, 17616, 18099, 18588, 19085, 19595, 20120, 20656, 21212, 21782, 22370, 22974, 23585, 24210, 24847, 25485, 26124, 26762, 27390, 28002, 28596, 29166, 29707, 30212, 30675, 31096, 31468, 31782, 32037, 32225, 32343, 32376, 32337, 32196, 31951, 31589, 31097, 30458, 29653, 28668, 27487, 26079, 24434, 22539, 20394, 18019, 15420, 12623, 9703, 6703, 3699, 798, -1982, -4553, -6868, -8898, -10638, -12099, -13298, -14268, -15043, -15656, -16154, -16565, -16920, -17238, -17542, -17847, -18163, -18494, -18846, -19216, -19599, -19989, -20377, -20759, -21121, -21447, -21734, -21980, -22170, -22298, -22361, -22355, -22281, -22132, -21913, -21625, -21269, -20849, -20371, -19840, -19262, -18642, -18002, -17336, -16655, -15970, -15294, -14635, -14000, -13390, -12821, -12294, -11816, -11397, -11036, -10732, -10495, -10329, -10240, -10225, -10287, -10430, -10661, -10978, -11394, -11905, -12518, -13233, -14048, -14955, -15959, -17037, -18178, -19360, -20544, -21711, -22818, -23825, -24692, -25382, -25871, -26142, -26183, -25997, -25594, -24998, -24227, -23303, -22250, -21091, -19833, -18506, -17107, -15648, -14143, -12606, -11053, -9499, -7951, -6428, -4953, -3535, -2186, -909, 283, 1387, 2397, 3316, 4140, 4876, 5522, 6080, 6551, 6948, 7268, 7516, 7700, 7823, 7890, 7903, 7863, 7781, 7659, 7500, 7305, 7076, 6820, 6535, 6223, 5881, 5511, 5118, 4703, 4268, 3814, 3336, 2846, 2341, 1820, 1294, 764, 230, -299, -820, -1329, -1816, -2281, -2714, -3112, -3467, -3770, -4019, -4209, -4335, -4390, -4372, -4277, -4105, -3855, -3529, -3130, -2661, -2131, -1547, -924, -256};

const int16_t AKWF_cello_0002_256_DATA [256] = 
{364, 1719, 2914, 3924, 4570, 5630, 6630, 6974, 7337, 7350, 7460, 8938, 10166, 11059, 12003, 12487, 12963, 13688, 14228, 15005, 15864, 16783, 18451, 20346, 22158, 23534, 24807, 25548, 26183, 26477, 26339, 26597, 26958, 27287, 27430, 26863, 25844, 24461, 22228, 19533, 16792, 13836, 10831, 8213, 5883, 3680, 1336, -1446, -4569, -7766, -10901, -13815, -16547, -18477, -19408, -19863, -20075, -20625, -21719, -22905, -24080, -25345, -26282, -26529, -26336, -25957, -25050, -23744, -22084, -20363, -18840, -17539, -16440, -15398, -14210, -13016, -11865, -10430, -8739, -6909, -5122, -3401, -1829, -480, 667, 1582, 2405, 3369, 4308, 5366, 6387, 7202, 8277, 9410, 10362, 11193, 11967, 12921, 14196, 15702, 17181, 18352, 19239, 19575, 19535, 19248, 18974, 19038, 19380, 19778, 19924, 19616, 19021, 18271, 17078, 15508, 13827, 12128, 10391, 8959, 7882, 7002, 6327, 5900, 5718, 6089, 6760, 7613, 8491, 9184, 9606, 9588, 9455, 9154, 9044, 9336, 10298, 11712, 13368, 15330, 17033, 18077, 18355, 18057, 17472, 17066, 16786, 16430, 16160, 15963, 15754, 15594, 15427, 15058, 14574, 13597, 12605, 11819, 11018, 10404, 9850, 9181, 8584, 7926, 7152, 6247, 5327, 4324, 3270, 2199, 951, -455, -2282, -4668, -7330, -9958, -12447, -14736, -16678, -18504, -20091, -21570, -23021, -24471, -25794, -27228, -28725, -30179, -31559, -32269, -32368, -32122, -31362, -30432, -29383, -28503, -27951, -27861, -28078, -28263, -28377, -28436, -28323, -28070, -27743, -27592, -27581, -27334, -26924, -26324, -25447, -24252, -22699, -20967, -19130, -17472, -15766, -13878, -11999, -10164, -8416, -6587, -4667, -2581, -353, 1445, 2933, 4218, 5167, 5494, 5148, 4536, 3919, 3213, 2409, 1635, 992, 711, 195, -1013, -1631, -2023, -2333, -2546, -2960, -3351, -3832, -4045, -4241, -3759, -3309, -2743, -2402, -2249, -1744, -605};

#endif
