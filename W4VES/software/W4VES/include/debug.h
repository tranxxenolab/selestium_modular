// heavily indebted to: https://github.com/FOSSASystems/XENOKINETICS-2/blob/master/software/XENOKINETICS2/src/Debug.h

#ifndef _W4VES_DEBUG_H
#define _W4VES_DEBUG_H

#define W4VES_DEBUG_PORT Serial
#define W4VES_DEBUG_PORT_SPEED 115200

#ifdef W4VES_DEBUG
// add a while loop for waiting, later, once watchdog is setup
#define W4VES_DEBUG_BEGIN(...) {W4VES_DEBUG_PORT.begin(__VA_ARGS__); delay(500);}
#define W4VES_DEBUG_PRINT(...) { W4VES_DEBUG_PORT.print(__VA_ARGS__); }
#define W4VES_DEBUG_PRINTLN(...) { W4VES_DEBUG_PORT.println(__VA_ARGS__); }
#define W4VES_DEBUG_PRINTF(...) { W4VES_DEBUG_PORT.printf(__VA_ARGS__); }
#define W4VES_DEBUG_WRITE(...) { W4VES_DEBUG_PORT.write(__VA_ARGS__); }

#else

#define W4VES_DEBUG_BEGIN(...) {}
#define W4VES_DEBUG_PRINT(...) {}
#define W4VES_DEBUG_PRINTLN(...) {}
#define W4VES_DEBUG_WRITE(...) {}

#endif

#endif