#ifndef _W4VES_CV_TRANSFORM_H
#define _W4VES_CV_TRANSFORM_H

// Inspired heavily by <https://github.com/pichenettes/eurorack/blob/master/clouds/cv_scaler.cc>

struct CVTransform {
    bool invert;
    bool removeOffset;
    float filterCoeff;
};

extern CVTransform CVTransforms[NUM_PARAMS];
extern CVTransform PotTransforms[NUM_POTS];
extern float smoothedCVADCValues[NUM_PARAMS];
extern float smoothedPotValues[NUM_POTS];

#endif