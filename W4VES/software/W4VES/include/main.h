#ifndef _W4VES_MAIN_H
#define _W4VES_MAIN_H

#include <Arduino.h>
#include <Audio.h>
#include <ADC.h>
#include <SPI.h>
#include <SD.h>
#include <Bounce2.h>

#include "config.h"
#include "debug.h"
#include "synthesis.h"
#include "interface.h"
#include "cv_transform.h"
#include "file_handling.h"

#endif