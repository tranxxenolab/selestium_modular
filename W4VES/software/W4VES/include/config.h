#ifndef _W4VES_CONFIG_H
#define _W4VES_CONFIG_H

// Pin defines
// Analog inputs
#define WAVE001_VOCT_CV_ADC         14
#define WAVE001_WF_CV_ADC           15
#define WAVE001_MOD_CV_ADC          16
#define WAVE002_VOCT_CV_ADC         17
#define WAVE002_WF_CV_ADC           18
#define WAVE002_MOD_CV_ADC          19
#define WAVE003_VOCT_CV_ADC         22
#define WAVE003_WF_CV_ADC           23
#define WAVE003_MOD_CV_ADC          24
#define WAVE004_VOCT_CV_ADC         25
#define WAVE004_WF_CV_ADC           26
#define WAVE004_MOD_CV_ADC          27
#define MUX01_SIGNAL                40
#define MUX02_SIGNAL                41

// Digital inputs
#define ENV_SW                      11
#define SCALE_SW                    12
#define WAVE001_TRIG                28
#define WAVE002_TRIG                29
#define WAVE003_TRIG                30
#define WAVE004_TRIG                31

// Digial outputs
#define MUX01_S0                    33
#define MUX01_S1                    34
#define MUX01_S2                    35
#define MUX01_S3                    36
#define MUX02_S0                    37
#define MUX02_S1                    38
#define MUX02_S2                    39
#define MUX02_S3                    13
#define SR_DATA                     8
#define SR_CLK                      9
#define SR_LATCH                    10

// I2S outputs
#define DIN0                        7
#define WS0                         20
#define BCK0                        21
#define DIN1                        2
#define WS1                         3
#define BCK1                        4




// 
#define NUM_VOICES              4
#define NUM_PARAMS              12
#define NUM_POTS                14
#define NUM_SCL_FILES           8
#define NUM_WAVEFORMS           7
#define NUM_MIXER_WEIGHTS_EXPANDED  100
#define HANNING_RANGE           10
#define HANNING_HALF            5

#define ADC_RESOLUTION          16
#define ADC_DESIRED_RESOLUTION  12
#define ADC_AVERAGES            64

#define AUDIO_MEMORY    12

#define MAX_NOTES_OCT           60

// Mux01 defines
#define NUM_MUX01_CHANNELS          14
#define WAVE001_SUB_SW              0
#define WAVE002_SUB_SW              1
#define WAVE003_SUB_SW              2
#define WAVE004_SUB_SW              3
#define WAVE001_OCTSW_POS1          4
#define WAVE001_OCTSW_POS2          5
#define WAVE002_OCTSW_POS1          6
#define WAVE002_OCTSW_POS2          7
#define WAVE003_OCTSW_POS1          8
#define WAVE003_OCTSW_POS2          9
#define WAVE004_OCTSW_POS1          10
#define WAVE004_OCTSW_POS2          11
#define FM_WAVE001_002_SW           12
#define FM_WAVE003_004_SW           13

// Mux02 defines
#define NUM_MUX02_CHANNELS          14
#define WAVE001_VOCT_POT            0
#define WAVE001_WF_POT              1
#define WAVE001_MOD_POT             2
#define WAVE002_VOCT_POT            3
#define WAVE002_WF_POT              4
#define WAVE002_MOD_POT             5
#define WAVE003_VOCT_POT            6
#define WAVE003_WF_POT              7
#define WAVE003_MOD_POT             8
#define WAVE004_VOCT_POT            9
#define WAVE004_WF_POT              10
#define WAVE004_MOD_POT             11
#define FM_WAVE_001_002_POT         12
#define FM_WAVE_003_004_POT         13

#define VOICE_SIN                   0
#define VOICE_TRI                   1
#define VOICE_SAW                   2
#define VOICE_PUL                   3
#define VOICE_ARB01                 4
#define VOICE_ARB02                 5
#define VOICE_ARB03                 6

#define C1_HZ                       32.70
#define C2_HZ                       65.41
#define C4_HZ                       261.63
#define C8_HZ                       4186.01

#define WAVE001_VOCT_CV_ADC_INDEX       0
#define WAVE001_WF_CV_ADC_INDEX         1
#define WAVE001_MOD_CV_ADC_INDEX        2 
#define WAVE002_VOCT_CV_ADC_INDEX       3 
#define WAVE002_WF_CV_ADC_INDEX         4
#define WAVE002_MOD_CV_ADC_INDEX        5
#define WAVE003_VOCT_CV_ADC_INDEX       6
#define WAVE003_WF_CV_ADC_INDEX         7
#define WAVE003_MOD_CV_ADC_INDEX        8
#define WAVE004_VOCT_CV_ADC_INDEX       9
#define WAVE004_WF_CV_ADC_INDEX         10
#define WAVE004_MOD_CV_ADC_INDEX        11

// 0.142857 repeating
#define VOCT_ADC_VALUE              0.143
#define ONE_AND_HALF_VOCT_ADC_VALUE 1.5*VOCT_ADC_VALUE

// From Emilie Gillet: https://github.com/pichenettes/stmlib/blob/d18def816c51d1da0c108236928b2bbd25c17481/dsp/dsp.h
#define ONE_POLE(out, in, coefficient) out += (coefficient) * ((in) - out);

#endif

