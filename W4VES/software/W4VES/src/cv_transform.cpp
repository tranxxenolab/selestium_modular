#include "main.h"

CVTransform CVTransforms[NUM_PARAMS] = {
    // WAVE001_VOCT_CV_ADC,
    {true, false, 1.0f},
    // WAVE001_WF_CV_ADC,
    {true, false, 0.05f},
    // WAVE001_MOD_CV_ADC,
    {true, false, 0.05f},
    // WAVE002_VOCT_CV_ADC,
    {true, false, 1.0f},
    // WAVE002_WF_CV_ADC,
    {true, false, 0.05f},
    // WAVE002_MOD_CV_ADC,
    {true, false, 0.05f},
    // WAVE003_VOCT_CV_ADC,
    {true, false, 1.0f},
    // WAVE003_WF_CV_ADC,
    {true, false, 0.05f},
    // WAVE003_MOD_CV_ADC,
    {true, false, 0.05f},
    // WAVE004_VOCT_CV_ADC,
    {true, false, 1.0f},
    // WAVE004_WF_CV_ADC,
    {true, false, 0.05f},
    // WAVE004_MOD_CV_ADC,
    {true, false, 0.05f}
};

CVTransform PotTransforms[NUM_POTS] = {
    // WAVE001_VOCT_POT,
    {true, false, 0.008f},
    // WAVE001_WF_POT,
    {true, false, 0.008f},
    // WAVE001_MOD_POT,
    {true, false, 0.05f},
    // WAVE002_VOCT_POT,
    {true, false, 0.05f},
    // WAVE002_WF_POT,
    {true, false, 0.05f},
    // WAVE002_MOD_POT,
    {true, false, 0.05f},
    // WAVE003_VOCT_POT,
    {true, false, 0.05f},
    // WAVE003_WF_POT,
    {true, false, 0.05f},
    // WAVE003_MOD_POT,
    {true, false, 0.05f},
    // WAVE004_VOCT_POT,
    {true, false, 0.05f},
    // WAVE004_WF_POT,
    {true, false, 0.05f},
    // WAVE004_MOD_POT,
    {true, false, 0.05f},
    // FM_WAVE_001_002_POT
    {true, false, 0.05f},
    // FM_WAVE_003_004_POT
    {true, false, 0.05f}

};


float smoothedCVADCValues[NUM_PARAMS] = {
    // WAVE001_VOCT_CV_ADC,
    0.0f,
    // WAVE001_WF_CV_ADC,
    0.0f,
    // WAVE001_MOD_CV_ADC,
    0.0f,
    // WAVE002_VOCT_CV_ADC,
    0.0f,
    // WAVE002_WF_CV_ADC,
    0.0f,
    // WAVE002_MOD_CV_ADC,
    0.0f,
    // WAVE003_VOCT_CV_ADC,
    0.0f,
    // WAVE003_WF_CV_ADC,
    0.0f,
    // WAVE003_MOD_CV_ADC,
    0.0f,
    // WAVE004_VOCT_CV_ADC,
    0.0f,
    // WAVE004_WF_CV_ADC,
    0.0f,
    // WAVE004_MOD_CV_ADC,
    0.0f
};

float smoothedPotValues[NUM_POTS] = {
    // WAVE001_VOCT_POT,
    0.0f,
    // WAVE001_WF_POT,
    0.0f,
    // WAVE001_MOD_POT,
    0.0f,
    // WAVE002_VOCT_POT,
    0.0f,
    // WAVE002_WF_POT,
    0.0f,
    // WAVE002_MOD_POT,
    0.0f,
    // WAVE003_VOCT_POT,
    0.0f,
    // WAVE003_WF_POT,
    0.0f,
    // WAVE003_MOD_POT,
    0.0f,
    // WAVE004_VOCT_POT,
    0.0f,
    // WAVE004_WF_POT,
    0.0f,
    // WAVE004_MOD_POT,
    0.0f,
    // FM_WAVE_001_002_POT
    0.0f,
    // FM_WAVE_003_004_POT
    0.0f
};