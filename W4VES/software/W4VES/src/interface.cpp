#include "main.h"

ADC *adc = new ADC();

int ADC0_MAX = 4096;
int ADC1_MAX = 4096;

uint8_t CV_PIN_LIST[NUM_PARAMS] = {
    WAVE001_VOCT_CV_ADC,
    WAVE001_WF_CV_ADC,
    WAVE001_MOD_CV_ADC,
    WAVE002_VOCT_CV_ADC,
    WAVE002_WF_CV_ADC,
    WAVE002_MOD_CV_ADC,
    WAVE003_VOCT_CV_ADC,
    WAVE003_WF_CV_ADC,
    WAVE003_MOD_CV_ADC,
    WAVE004_VOCT_CV_ADC,
    WAVE004_WF_CV_ADC,
    WAVE004_MOD_CV_ADC,
};

uint8_t digitalInputs[] = {
    ENV_SW,
    SCALE_SW
};

Bounce Wave001Trig = Bounce();
Bounce Wave002Trig = Bounce();
Bounce Wave003Trig = Bounce();
Bounce Wave004Trig = Bounce();

uint8_t digitalOutputs[] = {
	MUX01_S0,
	MUX01_S1,
	MUX01_S2,
	MUX01_S3,
	MUX02_S0,
	MUX02_S1,
	MUX02_S2,
	MUX02_S3,
	SR_DATA,
	SR_CLK,
	SR_LATCH
};


uint8_t muxChannel[16][4] = {
    {0,0,0,0}, //channel 0
    {1,0,0,0}, //channel 1
    {0,1,0,0}, //channel 2
    {1,1,0,0}, //channel 3
    {0,0,1,0}, //channel 4
    {1,0,1,0}, //channel 5
    {0,1,1,0}, //channel 6
    {1,1,1,0}, //channel 7
    {0,0,0,1}, //channel 8
    {1,0,0,1}, //channel 9
    {0,1,0,1}, //channel 10
    {1,1,0,1}, //channel 11
    {0,0,1,1}, //channel 12
    {1,0,1,1}, //channel 13
    {0,1,1,1}, //channel 14
    {1,1,1,1}  //channel 15
};

uint8_t mux01Pins[4] = {
	MUX01_S0,
	MUX01_S1,
	MUX01_S2,
	MUX01_S3
};

uint8_t mux02Pins[4] = {
	MUX02_S0,
	MUX02_S1,
	MUX02_S2,
	MUX02_S3
};


uint16_t mux01Values[NUM_MUX01_CHANNELS] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

int8_t prevOct[NUM_VOICES] = {0, 0, 0, 0};

void setupTriggers() {
    Wave001Trig.attach(WAVE001_TRIG, INPUT_PULLUP);
    Wave002Trig.attach(WAVE002_TRIG, INPUT_PULLUP);
    Wave003Trig.attach(WAVE003_TRIG, INPUT_PULLUP);
    Wave004Trig.attach(WAVE004_TRIG, INPUT_PULLUP);
}

void setupDigitalPins() {
    for (int i : digitalInputs) {
        pinMode(digitalInputs[i], INPUT_PULLDOWN);
    }

    for (int i: digitalOutputs) {
        pinMode(digitalOutputs[i], OUTPUT);
    }
}

void processTriggers() {
    Wave001Trig.update();
    Wave002Trig.update();
    Wave003Trig.update();
    Wave004Trig.update();


    if (Wave001Trig.rose()) {
        W4VES_DEBUG_PRINTLN("Trig001 rising edge...");
    } else if (Wave001Trig.fell()) {
        W4VES_DEBUG_PRINTLN("Trig001 falling edge...");
    }

    if (Wave002Trig.rose()) {
        W4VES_DEBUG_PRINTLN("Trig002 rising edge...");
    } else if (Wave002Trig.fell()) {
        W4VES_DEBUG_PRINTLN("Trig002 falling edge...");
    }


}

int processPot(int adcPin) {
    int value = adc->adc0->analogRead(adcPin);

    if(adc->adc0->fail_flag != ADC_ERROR::CLEAR) {
        W4VES_DEBUG_PRINT("Error in ADC0: ");
        // Not using the prettify method because it causes linker
        // issues for some reason that I haven't been able to figure out.
        // Should post something about this on the ADC library
        // github page.
        //W4VES_DEBUG_PRINTLN(getStringADCError(adc->adc0->fail_flag));
        W4VES_DEBUG_PRINTLN(int(adc->adc0->fail_flag));
        adc->resetError();
    }

    return value;
}

void processCVs() {
    int tempADC;
    float tempValue;

    for (uint8_t i = 0; i < NUM_PARAMS; i++) {
        tempADC = adc->adc0->analogRead(CV_PIN_LIST[i]);
        if (CVTransforms[i].invert) {
            tempADC = ADC0_MAX - tempADC;
        }

        tempValue = tempADC / float(ADC0_MAX);
        smoothedCVADCValues[i] += CVTransforms[i].filterCoeff * (tempValue - smoothedCVADCValues[i]);

        constrain(smoothedCVADCValues[i], 0.0f, 1.0f);
    }
}


void processPots() {
    int tempADC;
    float tempValue;

    for (uint8_t i = 0; i < NUM_MUX02_CHANNELS; i++) {
        tempADC = readMux02Channel(i);
        if (PotTransforms[i].invert) {
            tempADC = ADC0_MAX - tempADC;
        }

        tempValue = tempADC / float(ADC0_MAX);
        smoothedPotValues[i] += PotTransforms[i].filterCoeff * (tempValue - smoothedPotValues[i]);
        constrain(smoothedPotValues[i], 0.0f, 1.0f);
    }
}

void processSwitches() {
    // TODO
    // for testing only
    uint16_t tempValue = 0;

    for (int i = 0; i < NUM_MUX01_CHANNELS; i++) {
        tempValue = readMux01Channel(i);
        // TODO change so that it changes with a change in the ADC resolution
        if (tempValue < 2048) {
            mux01Values[i] = 0;
        } else {
            mux01Values[i] = 1;
        }
    }

    // TODO
    // streamline this
    if ((mux01Values[WAVE001_OCTSW_POS1] == 1) && (mux01Values[WAVE001_OCTSW_POS2] == 0)) {
        octaves[0] = 2.0f;
    } else if  ((mux01Values[WAVE001_OCTSW_POS1] == 0) && (mux01Values[WAVE001_OCTSW_POS2] == 1)) {
        octaves[0] = 0.5f;
    } else {
        octaves[0] = 1.0f;
    }

    if ((mux01Values[WAVE002_OCTSW_POS1] == 1) && (mux01Values[WAVE002_OCTSW_POS2] == 0)) {
        octaves[1] = 2.0f;
    } else if  ((mux01Values[WAVE002_OCTSW_POS1] == 0) && (mux01Values[WAVE002_OCTSW_POS2] == 1)) {
        octaves[1] = 0.5f;
    } else {
        octaves[1] = 1.0f;
    }

    if ((mux01Values[WAVE003_OCTSW_POS1] == 1) && (mux01Values[WAVE003_OCTSW_POS2] == 0)) {
        octaves[2] = 2.0f;
    } else if  ((mux01Values[WAVE003_OCTSW_POS1] == 0) && (mux01Values[WAVE003_OCTSW_POS2] == 1)) {
        octaves[2] = 0.5f;
    } else {
        octaves[2] = 1.0f;
    }

    if ((mux01Values[WAVE004_OCTSW_POS1] == 1) && (mux01Values[WAVE004_OCTSW_POS2] == 0)) {
        octaves[3] = 2.0f;
    } else if  ((mux01Values[WAVE004_OCTSW_POS1] == 0) && (mux01Values[WAVE004_OCTSW_POS2] == 1)) {
        octaves[3] = 0.5f;
    } else {
        octaves[3] = 1.0f;
    }

    if (mux01Values[WAVE001_SUB_SW] == 1) {
        voicesSubsValues[0] = 1;
    } else {
        voicesSubsValues[0] = 0;
    }

    if (mux01Values[WAVE002_SUB_SW] == 1) {
        voicesSubsValues[1] = 1;
    } else {
        voicesSubsValues[1] = 0;
    }

    if (mux01Values[WAVE003_SUB_SW] == 1) {
        voicesSubsValues[2] = 1;
    } else {
        voicesSubsValues[2] = 0;
    }

    if (mux01Values[WAVE004_SUB_SW] == 1) {
        voicesSubsValues[3] = 1;
    } else {
        voicesSubsValues[3] = 0;
    }








}

void readMux02() {
    W4VES_DEBUG_PRINT("WAVE001_VOCT_POT from mux: ");
    W4VES_DEBUG_PRINTLN(readMux02Channel(0));
    W4VES_DEBUG_PRINT("WAVE001_WF_POT from mux: ");
    W4VES_DEBUG_PRINTLN(readMux02Channel(1));
    W4VES_DEBUG_PRINT("WAVE001_MOD_POT from mux: ");
    W4VES_DEBUG_PRINTLN(readMux02Channel(2));

}

uint16_t readMux02Channel(uint8_t channel) {
    for (int i = 0; i < 4; i++) {
        digitalWrite(mux02Pins[i], muxChannel[channel][i]);
    }

    // TODO
    // check to see if this is sufficient or too long
    delayMicroseconds(10);

    return uint16_t(adc->adc0->analogRead(MUX02_SIGNAL));
}

uint16_t readMux01Channel(uint8_t channel) {
    for (int i = 0; i < 4; i++) {
        digitalWrite(mux01Pins[i], muxChannel[channel][i]);
    }

    // TODO
    // check to see if this is sufficient or too long
    delayMicroseconds(10);

    return uint16_t(adc->adc0->analogRead(MUX01_SIGNAL));
}

void printSwitchValues() {
   W4VES_DEBUG_PRINT(mux01Values[WAVE001_SUB_SW]);
   W4VES_DEBUG_PRINT(" ");
   W4VES_DEBUG_PRINT(mux01Values[WAVE002_SUB_SW]);
   W4VES_DEBUG_PRINT(" ");
   W4VES_DEBUG_PRINT(mux01Values[WAVE003_SUB_SW]);
   W4VES_DEBUG_PRINT(" ");
   W4VES_DEBUG_PRINT(mux01Values[WAVE004_SUB_SW]);
   W4VES_DEBUG_PRINT(" ");
   W4VES_DEBUG_PRINT(mux01Values[WAVE001_OCTSW_POS1]);
   W4VES_DEBUG_PRINT(" ");
   W4VES_DEBUG_PRINT(mux01Values[WAVE001_OCTSW_POS2]);
   W4VES_DEBUG_PRINT(" ");
   W4VES_DEBUG_PRINT(mux01Values[WAVE002_OCTSW_POS1]);
   W4VES_DEBUG_PRINT(" ");
   W4VES_DEBUG_PRINT(mux01Values[WAVE002_OCTSW_POS2]);
   W4VES_DEBUG_PRINT(" ");
   W4VES_DEBUG_PRINT(mux01Values[WAVE003_OCTSW_POS1]);
   W4VES_DEBUG_PRINT(" ");
   W4VES_DEBUG_PRINT(mux01Values[WAVE003_OCTSW_POS2]);
   W4VES_DEBUG_PRINT(" ");
   W4VES_DEBUG_PRINT(mux01Values[WAVE004_OCTSW_POS1]);
   W4VES_DEBUG_PRINT(" ");
   W4VES_DEBUG_PRINT(mux01Values[WAVE004_OCTSW_POS2]);
   W4VES_DEBUG_PRINT(" ");
   W4VES_DEBUG_PRINT(mux01Values[FM_WAVE001_002_SW]);
   W4VES_DEBUG_PRINT(" ");
   W4VES_DEBUG_PRINT(mux01Values[FM_WAVE003_004_SW]);
   W4VES_DEBUG_PRINTLN("");

}

void printPotValues() {
   W4VES_DEBUG_PRINT(smoothedPotValues[WAVE001_VOCT_POT]);
   W4VES_DEBUG_PRINT(" ");
   W4VES_DEBUG_PRINT(smoothedPotValues[WAVE001_WF_POT]);
   W4VES_DEBUG_PRINT(" ");
   W4VES_DEBUG_PRINT(smoothedPotValues[WAVE001_MOD_POT]);
   W4VES_DEBUG_PRINT(" ");
   W4VES_DEBUG_PRINT(smoothedPotValues[WAVE002_VOCT_POT]);
   W4VES_DEBUG_PRINT(" ");
   W4VES_DEBUG_PRINT(smoothedPotValues[WAVE002_WF_POT]);
   W4VES_DEBUG_PRINT(" ");
   W4VES_DEBUG_PRINT(smoothedPotValues[WAVE002_MOD_POT]);
   W4VES_DEBUG_PRINT(" ");
   W4VES_DEBUG_PRINT(smoothedPotValues[WAVE003_VOCT_POT]);
   W4VES_DEBUG_PRINT(" ");
   W4VES_DEBUG_PRINT(smoothedPotValues[WAVE003_WF_POT]);
   W4VES_DEBUG_PRINT(" ");
   W4VES_DEBUG_PRINT(smoothedPotValues[WAVE003_MOD_POT]);
   W4VES_DEBUG_PRINT(" ");
   W4VES_DEBUG_PRINT(smoothedPotValues[WAVE004_VOCT_POT]);
   W4VES_DEBUG_PRINT(" ");
   W4VES_DEBUG_PRINT(smoothedPotValues[WAVE004_WF_POT]);
   W4VES_DEBUG_PRINT(" ");
   W4VES_DEBUG_PRINT(smoothedPotValues[WAVE004_MOD_POT]);
   W4VES_DEBUG_PRINTLN(" ");
   //W4VES_DEBUG_PRINT(smoothedPotValues[FM_WAVE_001_002_POT]);
   //W4VES_DEBUG_PRINT(" ");
   //W4VES_DEBUG_PRINT(smoothedPotValues[FM_WAVE_003_004_POT]);
   //W4VES_DEBUG_PRINTLN("");


}