#include "main.h"

/**
 *
 * TODO
 * 
 * * Write scala file processor: https://www.huygens-fokker.org/scala/scl_format.html
 *    * Write class to deal with processing, creating scale, etc
 *    * Create an array of these classes
 * * Test synthesis algorithms
 * 
*/

const int chipSelect = BUILTIN_SDCARD;

// Testing variables...remove later
int adcValue = 0;
uint64_t counter = 0;
int divisor = 1000;

void setup() {

  W4VES_DEBUG_BEGIN(W4VES_DEBUG_PORT_SPEED);
  while (!W4VES_DEBUG) {
    ; // wait for serial port to connect.
  }

  W4VES_DEBUG_PRINTLN("");
  W4VES_DEBUG_PRINTLN("------------------------");
  W4VES_DEBUG_PRINTLN("");
  W4VES_DEBUG_PRINTLN("     W4VES");
  W4VES_DEBUG_PRINTLN("");
  W4VES_DEBUG_PRINTLN("------------------------");
  W4VES_DEBUG_PRINTLN("");
  W4VES_DEBUG_PRINTLN("Starting up...");

  // Audio connections require memory to work.  For more
  // detailed information, see the MemoryAndCpuUsage example
  W4VES_DEBUG_PRINTLN("Setting audio memory....");
  AudioMemory(AUDIO_MEMORY);

  W4VES_DEBUG_PRINTLN("Initializing SD card subsystem...");

  if (!SD.begin(chipSelect)) {
    W4VES_DEBUG_PRINTLN("Initialization failed! Restarting....");
    return;
  }
  W4VES_DEBUG_PRINTLN("Initialization done.");

  W4VES_DEBUG_PRINTLN("Starting ADC....");
  for (int i = 0; i < NUM_PARAMS; i++) {
    pinMode(CV_PIN_LIST[i], INPUT);
  }


  pinMode(MUX01_SIGNAL, INPUT_PULLDOWN);
  pinMode(MUX02_SIGNAL, INPUT);
  pinMode(WAVE001_VOCT_CV_ADC, INPUT);
  pinMode(WAVE001_WF_CV_ADC, INPUT);
  pinMode(WAVE001_MOD_CV_ADC, INPUT);
  

  setupDigitalPins();
  setupTriggers();

  ///// ADC0 ////
  adc->adc0->setAveraging(ADC_AVERAGES); // set number of averages
  adc->adc0->setResolution(ADC_RESOLUTION); // set bits of resolution
  adc->adc0->setSamplingSpeed(ADC_SAMPLING_SPEED::HIGH_SPEED); // change the sampling speed
  adc->adc0->setConversionSpeed(ADC_CONVERSION_SPEED::HIGH_SPEED); // change the conversion speed

  ////// ADC1 /////
  adc->adc1->setAveraging(ADC_AVERAGES); // set number of averages
  adc->adc1->setResolution(ADC_RESOLUTION); // set bits of resolution
  adc->adc1->setSamplingSpeed(ADC_SAMPLING_SPEED::HIGH_SPEED); // change the sampling speed
  adc->adc1->setConversionSpeed(ADC_CONVERSION_SPEED::HIGH_SPEED); // change the conversion speed

  //ADC0_MAX = adc->adc0->getMaxValue();
  //ADC1_MAX = adc->adc1->getMaxValue();

  W4VES_DEBUG_PRINTLN("Reading SCL files....");
  readSCLFiles();

  // CONSIDER PHASE SHIFTING ALL OF THE WAVES 180
  // DUE TO THE WAY WE DO THE OUTPUT

  W4VES_DEBUG_PRINTLN("Starting synthesis....");
  startSynthesis();

// Simple waveform test
/*
  waveformMod1.amplitude(0.1);
  waveformMod1.frequency(440);
  waveformMod1.begin(WAVEFORM_SINE);
  waveformMod2.amplitude(0.1);
  waveformMod2.frequency(440);
  waveformMod2.begin(WAVEFORM_SINE);
*/

}

void loop() {
  // put your main code here, to run repeatedly:

  /*
  adcValue = processPot(WAVE001_VOCT_ADC);
  W4VES_DEBUG_PRINT("WAVE001_VOCT_ADC value: ");
  W4VES_DEBUG_PRINTLN(adcValue);
  */
  /*
  processPots();
  // Need better way to do this, of course
  // Frequencies taken from: <https://en.wikipedia.org/wiki/Piano_key_frequencies>
  waveform1.frequency(int(map(smoothed_adc_values[0], 0.1, 1.0, 32.70320, 4186.009)));
  waveformIndex = map(smoothed_adc_values[1], 0.4, 1.0, 1, 10);
  fmIndex = map(smoothed_adc_values[2], 0.5, 1.0, 0.0, 10.0 );

  if ((waveformIndex >= 1.0) && (waveformIndex < 2.5)) {
    waveform1.begin(WAVEFORM_SINE);
  } else if ((waveformIndex >= 2.5) && (waveformIndex < 5.0)) {
    waveform1.begin(WAVEFORM_TRIANGLE);
  } else if ((waveformIndex >= 5.0) && (waveformIndex < 7.5)) {
    waveform1.begin(WAVEFORM_SAWTOOTH);
  } else if ((waveformIndex >= 7.5) && (waveformIndex <= 10.0)) {
    waveform1.begin(WAVEFORM_PULSE);
  }

  waveformMod1.frequencyModulation(fmIndex);

  */

  processTriggers();
  processSwitches();
  processPots();
  processCVs();
  processW4ves();

  if ((counter % divisor) == 0) {
    //W4VES_DEBUG_PRINTLN(readMux02Channel(14));
    //W4VES_DEBUG_PRINTLN(readMux02Channel(15));
    //W4VES_DEBUG_PRINT(freqs[0]);
    //W4VES_DEBUG_PRINT(" ");
    //W4VES_DEBUG_PRINTLN(freqs[1]);

    //W4VES_DEBUG_PRINT("WAVE001_MOD_POT: ")
    //W4VES_DEBUG_PRINTLN(smoothed_adc_values[2]);
    //readMux02();
    //W4VES_DEBUG_PRINTLN(readMux02Channel(0));
    /*
    digitalWrite(MUX02_S0, 0);
    digitalWrite(MUX02_S1, 0);
    digitalWrite(MUX02_S2, 0);
    digitalWrite(MUX02_S3, 0);
    W4VES_DEBUG_PRINT("WAVE001_VOCT_POT: ");
    W4VES_DEBUG_PRINTLN(adc->adc0->analogRead(MUX02_SIGNAL));

    digitalWrite(MUX02_S0, 1);
    digitalWrite(MUX02_S1, 0);
    digitalWrite(MUX02_S2, 0);
    digitalWrite(MUX02_S3, 0);
    W4VES_DEBUG_PRINT("WAVE001_WF_POT: ");
    W4VES_DEBUG_PRINTLN(adc->adc0->analogRead(MUX02_SIGNAL));

    digitalWrite(MUX02_S0, 0);
    digitalWrite(MUX02_S1, 1);
    digitalWrite(MUX02_S2, 0);
    digitalWrite(MUX02_S3, 0);
    W4VES_DEBUG_PRINT("WAVE001_MOD_POT: ");
    W4VES_DEBUG_PRINTLN(adc->adc0->analogRead(MUX02_SIGNAL));
    */
   //readMux02();
    //printPotValues();
    //printSwitchValues();
    //W4VES_DEBUG_PRINTLN(octaves[0]);

    showProcessorUsage();
    //W4VES_DEBUG_PRINTLN(smoothedPotValues[WAVE001_WF_POT]);
    //printMixerWeightsExpanded(1);
    //printMixerWeights();
    //W4VES_DEBUG_PRINTLN(smoothedCVADCValues[3]);
    //W4VES_DEBUG_PRINTLN(freqs[0]);
    //W4VES_DEBUG_PRINTLN(readMux02Channel(1));
    //W4VES_DEBUG_PRINTLN(adc->adc0->analogRead(WAVE001_WF_CV_ADC));
    W4VES_DEBUG_PRINTLN(phaseAngles[0][2]);
  }

  counter += 1;
}
