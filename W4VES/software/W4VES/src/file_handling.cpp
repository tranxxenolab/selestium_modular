#include "main.h"

// See about converting this to something like a c string
String sclFilenames [NUM_SCL_FILES] = {
    "/scl/scale001.scl",
    "/scl/scale002.scl",
    "/scl/scale003.scl",
    "/scl/scale004.scl",
    "/scl/scale005.scl",
    "/scl/scale006.scl",
    "/scl/scale007.scl",
    "/scl/scale008.scl",
};

enum SCLState {STATE_DESC, STATE_NUM_VALUES, STATE_VALUES};
SCLState currentSCLState = STATE_DESC;

void readSCLFile(String filename) {
    File sclFile = SD.open(filename.c_str(), FILE_READ);
    String currentLine = "";
    String substr = "";
    int numScaleValues = 0;
    int currentScaleIndex = 0;
    float currentFrequency = 0.0;
    unsigned long long numerator = 0;
    unsigned long long denominator = 1;
    float fraction = 0.0f;
    int divideIndex = -1;

    currentSCLState = STATE_DESC;
    if (sclFile) {
        char *pEnd;
        // This might be a brittle way of doing it, but let's give it a go
        while (sclFile.available()) {
            currentLine = sclFile.readStringUntil('\n');
            if (currentLine.indexOf('!') != 0) {
                if (currentSCLState == STATE_DESC) {
                    if (W4VES_MEGA_DEBUG) {
                        W4VES_DEBUG_PRINT("Scale description: ");
                        W4VES_DEBUG_PRINTLN(currentLine);
                    }
                    currentSCLState = STATE_NUM_VALUES;
                } else if (currentSCLState == STATE_NUM_VALUES) {
                    numScaleValues = int(strtof(currentLine.c_str(), &pEnd));
                    if (W4VES_MEGA_DEBUG) {
                        W4VES_DEBUG_PRINT("Num scale values: ");
                        W4VES_DEBUG_PRINTLN(numScaleValues);
                    }
                    currentSCLState = STATE_VALUES;
                } else if (currentSCLState == STATE_VALUES) {
                    divideIndex = currentLine.indexOf('/');
                    if (divideIndex == -1) {
                        currentFrequency = strtof(currentLine.c_str(), &pEnd);
                        W4VES_DEBUG_PRINT("Cents value: ");
                        W4VES_DEBUG_PRINTLN(currentFrequency);
                    } else {
                        substr = currentLine.substring(0, divideIndex);
                        //numerator = substr.toFloat();
                        numerator = strtoull(substr.c_str(), &pEnd, 10);
                        substr = currentLine.substring(divideIndex + 1, currentLine.length());
                        //denominator = substr.toFloat();
                        denominator = strtoull(substr.c_str(), &pEnd, 10);
                        fraction = float(numerator) / float(denominator);
                        W4VES_DEBUG_PRINT("Fractional value: ");
                        W4VES_DEBUG_PRINT(numerator);
                        W4VES_DEBUG_PRINT("/");
                        W4VES_DEBUG_PRINTLN(denominator);
                        W4VES_DEBUG_PRINT("Calculated fraction: ");
                        W4VES_DEBUG_PRINTLN(fraction);
                    }                   
                    currentScaleIndex += 1;
                }
            } 
        }

        sclFile.close();
    } else {
        W4VES_DEBUG_PRINTLN("Error reading scl file!");
    }
}

void readSCLFiles() {
    for (int i = 0; i < NUM_SCL_FILES; i++) {
        readSCLFile(sclFilenames[i]);
        delay(100);
        W4VES_DEBUG_PRINTLN("");
        W4VES_DEBUG_PRINTLN("");
    }
}