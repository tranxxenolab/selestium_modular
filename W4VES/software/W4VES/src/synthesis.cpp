#include "main.h"

float tempFreq = 432.0;
float waveformIndex = 0.0;
float waveformIndex001 = 0.0;
float waveformIndex002 = 0.0;
float waveformIndex003 = 0.0;
float waveformIndex004 = 0.0;
float waveformModValue001 = 0.0;
float waveformModValue002 = 0.0;
float waveformModValue003 = 0.0;
float waveformModValue004 = 0.0;
float fmIndex = 0.0;
//int8_t prevOct = 0;
bool octaveChanged = false;

bool voicesSubsValues[NUM_VOICES] = {false, false, false, false};

float defaultMixerWeights[NUM_VOICES][NUM_WAVEFORMS] = {
    {0.25, 0.25, 0.25, 0.25, 0.33, 0.33, 0.33},
    {0.25, 0.25, 0.25, 0.25, 0.33, 0.33, 0.33},
    {0.25, 0.25, 0.25, 0.25, 0.33, 0.33, 0.33},
    {0.25, 0.25, 0.25, 0.25, 0.33, 0.33, 0.33}
};

float mixerWeights[NUM_VOICES][NUM_WAVEFORMS] = {
    {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
    {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
    {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
    {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0}
};

float phaseAngles[NUM_VOICES][NUM_WAVEFORMS] = {
    {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
    {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
    {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
    {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0}
};


/*
0.0
    0.166,
    0.332,
    0.498,
    0.664,
    0.83,
    0.996

*/
int8_t defaultWaveformBreakpoints[NUM_WAVEFORMS + 1] = {
    0,
    14,
    29,
    43,
    57,
    71,
    84,
    100
};

float mixerWeightsExpanded[NUM_VOICES][NUM_MIXER_WEIGHTS_EXPANDED];
float defaultMixerWeightsExpanded[NUM_VOICES][NUM_MIXER_WEIGHTS_EXPANDED];

float hanningWindow10[11] = {
    0.0,
    0.0954915, 
    0.3454915, 
    0.6545085, 
    0.9045085, 
    1.0,
    0.9045085, 
    0.6545085, 
    0.3454915, 
    0.0954915, 
    0.0
};

float hanningWindow20[21] = {
    0.0, 
    0.02447174,
    0.0954915,
    0.20610737,
    0.3454915,
    0.5,
    0.6545085,
    0.79389263,
    0.9045085,
    0.97552826,
    1.0,
    0.97552826,
    0.9045085,
    0.79389263,
    0.6545085,
    0.5,
    0.3454915,
    0.20610737,
    0.0954915,
    0.02447174,
    0.0
};

/*

use hanning window
from: https://numpy.org/doc/stable/reference/generated/numpy.hanning.html

np.hanning(12)
array([0.        , 0.07937323, 0.29229249, 0.57115742, 0.82743037,
       0.97974649, 0.97974649, 0.82743037, 0.57115742, 0.29229249,
       0.07937323, 0.        ])

np.hanning(8)
array([0.        , 0.1882551 , 0.61126047, 0.95048443, 0.95048443,
       0.61126047, 0.1882551 , 0.        ])

np.hanning(11)
array([0.       , 0.0954915, 0.3454915, 0.6545085, 0.9045085, 1.       ,
       0.9045085, 0.6545085, 0.3454915, 0.0954915, 0.       ])

So basically, let's say my pot value is 0.5. I want to take the hanning window
around this value, +- 0.1. So that means we want to take the range 0.4 to 0.6,
divide it into 11 values (with 0.5 at the center), and then multiply by the 
value of the hanning window. That determines the relative values for the amplitudes
of the waves. From there, we lookup in the breakpoints and say (in this case), that
Pul is at 1 and Arb01 is at some fraction, due to the hanning window

*/

AudioSynthWaveform* Voice01Waveforms[NUM_WAVEFORMS];
AudioSynthWaveform* Voice03Waveforms[NUM_WAVEFORMS];
AudioSynthWaveform* VoicesSubsWaveforms[NUM_VOICES];
AudioSynthWaveformModulated* Voice02Waveforms[NUM_WAVEFORMS];
AudioSynthWaveformModulated* Voice04Waveforms[NUM_WAVEFORMS];


/*
// GUItool: begin automatically generated code
AudioSynthWaveform       waveform3; //xy=106.20001220703125,460.1999969482422
AudioSynthWaveform       waveform4;  //xy=116.20001220703125,515.1999969482422
AudioSynthWaveform       waveform1;      //xy=119.20001220703125,131.1999969482422
AudioSynthWaveform       waveform2;      //xy=129.20001220703125,186.1999969482422
AudioSynthWaveformModulated waveformMod4; //xy=270.20001220703125,682.2000122070312
AudioSynthWaveformModulated waveformMod3; //xy=271.20001220703125,631.2000122070312
AudioSynthWaveformModulated waveformMod2; //xy=283.20001220703125,353.20001220703125
AudioSynthWaveformModulated waveformMod1;   //xy=284.20001220703125,302.20001220703125
AudioMixer4              mixer3; //xy=289.20001220703125,453.1999969482422
AudioMixer4              mixer1;         //xy=302.20001220703125,124.19999694824219
AudioMixer4              mixer4; //xy=461.20001220703125,643.2000122070312
AudioMixer4              mixer2; //xy=474.20001220703125,314.20001220703125
AudioOutputPT8211        pt8211_1;       //xy=707.1999969482422,236.1999969482422
AudioOutputPT8211_2      pt8211_2_1;     //xy=737.1999969482422,614.1999969482422
AudioConnection          patchCord1(waveform3, 0, mixer3, 0);
AudioConnection          patchCord2(waveform4, 0, mixer3, 1);
AudioConnection          patchCord3(waveform1, 0, mixer1, 0);
AudioConnection          patchCord4(waveform2, 0, mixer1, 1);
AudioConnection          patchCord5(waveformMod4, 0, mixer4, 1);
AudioConnection          patchCord6(waveformMod3, 0, mixer4, 0);
AudioConnection          patchCord7(waveformMod2, 0, mixer2, 1);
AudioConnection          patchCord8(waveformMod1, 0, mixer2, 0);
AudioConnection          patchCord9(mixer3, 0, waveformMod3, 0);
AudioConnection          patchCord10(mixer3, 0, waveformMod4, 0);
AudioConnection          patchCord11(mixer3, 0, pt8211_2_1, 0);
//AudioConnection          patchCord12(mixer1, 0, waveformMod1, 0);
//AudioConnection          patchCord13(mixer1, 0, waveformMod2, 0);
AudioConnection          patchCord14(mixer1, 0, pt8211_1, 0);
AudioConnection          patchCord15(mixer4, 0, pt8211_2_1, 1);
AudioConnection          patchCord16(mixer2, 0, pt8211_1, 1);
// GUItool: end automatically generated code
*/

/*
// GUItool: begin automatically generated code
AudioSynthWaveform       waveform3;      //xy=245.1999969482422,725.0000152587891
AudioSynthWaveform       waveform4;      //xy=255.1999969482422,780.0000152587891
AudioSynthWaveform       waveform1;      //xy=258.1999969482422,396.00001525878906
AudioSynthWaveform       waveform2;      //xy=268.1999969482422,451.00001525878906
AudioSynthWaveformModulated waveformMod4;   //xy=409.1999969482422,947.0000152587891
AudioSynthWaveformModulated waveformMod3;   //xy=410.1999969482422,896.0000152587891
AudioSynthWaveformModulated waveformMod2;   //xy=422.1999969482422,618.0000152587891
AudioSynthWaveformModulated waveformMod1;   //xy=423.1999969482422,567.0000152587891
AudioMixer4              mixer3;         //xy=428.1999969482422,718.0000152587891
AudioMixer4              mixer1;         //xy=441.1999969482422,389.00001525878906
AudioSynthWaveformDc     dc1;            //xy=542.2000122070312,488.20001220703125
AudioMixer4              mixer4;         //xy=600.1999969482422,908.0000152587891
AudioMixer4              mixer2;         //xy=613.1999969482422,579.0000152587891
AudioEffectWaveFolder    wavefolder1;    //xy=685.2000122070312,436.20001220703125
AudioOutputPT8211        pt8211_1;       //xy=846.1999969482422,501.00001525878906
AudioOutputPT8211_2      pt8211_2_1;     //xy=876.1999969482422,879.0000152587891
AudioConnection          patchCord1(waveform3, 0, mixer3, 0);
AudioConnection          patchCord2(waveform4, 0, mixer3, 1);
AudioConnection          patchCord3(waveform1, 0, mixer1, 0);
AudioConnection          patchCord4(waveform2, 0, mixer1, 1);
AudioConnection          patchCord5(waveformMod4, 0, mixer4, 1);
AudioConnection          patchCord6(waveformMod3, 0, mixer4, 0);
AudioConnection          patchCord7(waveformMod2, 0, mixer2, 1);
AudioConnection          patchCord8(waveformMod1, 0, mixer2, 0);
AudioConnection          patchCord9(mixer3, 0, waveformMod3, 0);
AudioConnection          patchCord10(mixer3, 0, waveformMod4, 0);
AudioConnection          patchCord11(mixer3, 0, pt8211_2_1, 0);
AudioConnection          patchCord12(mixer1, 0, waveformMod1, 0);
AudioConnection          patchCord13(mixer1, 0, waveformMod2, 0);
AudioConnection          patchCord14(mixer1, 0, wavefolder1, 0);
AudioConnection          patchCord15(dc1, 0, wavefolder1, 1);
AudioConnection          patchCord16(mixer4, 0, pt8211_2_1, 1);
AudioConnection          patchCord17(mixer2, 0, pt8211_1, 1);
AudioConnection          patchCord18(wavefolder1, 0, pt8211_1, 0);
// GUItool: end automatically generated code
*/


/*
// GUItool: begin automatically generated code
AudioSynthWaveformModulated waveformMod1;   //xy=339.20001220703125,248.1999969482422
AudioSynthWaveformModulated waveformMod2; //xy=351.20001220703125,318.20001220703125
AudioOutputPT8211        pt8211_1;       //xy=708.2000122070312,300.20001220703125
AudioConnection          patchCord1(waveformMod1, 0, pt8211_1, 0);
AudioConnection          patchCord2(waveformMod2, 0, pt8211_1, 1);
// GUItool: end automatically generated code
*/

// MOST RECENT VERSION WITH MORPHING OF WAVEFORMS
// GUItool: begin automatically generated code
AudioSynthWaveform       wfSubVoice03; //xy=235.55557250976562,1640.666748046875
AudioSynthWaveform       wfSubVoice01;      //xy=240.22222137451172,667.9999504089355
AudioSynthWaveform       wfArb02Voice03; //xy=240.00001525878906,1550.6666870117188
AudioSynthWaveform       wfArb01Voice03; //xy=241.00001525878906,1510.6666870117188
AudioSynthWaveform       wfArb03Voice03; //xy=241.00001525878906,1590.6666870117188
AudioSynthWaveformModulated wfModArb01Voice04; //xy=241.00001525878906,1905.6666870117188
AudioSynthWaveformModulated wfModSinVoice04; //xy=243.00001525878906,1692.6666870117188
AudioSynthWaveform       wfSinVoice03;   //xy=245.00001525878906,1315.6666870117188
AudioSynthWaveform       wfArb02Voice01; //xy=248.00001525878906,580.6666870117188
AudioSynthWaveform       wfArb01Voice01; //xy=249.00001525878906,540.6666870117188
AudioSynthWaveform       wfArb03Voice01; //xy=249.00001525878906,620.6666870117188
AudioSynthWaveform       wfSawVoice03;   //xy=246.00001525878906,1405.6666870117188
AudioSynthWaveformModulated wfModArb02Voice04; //xy=244.00001525878906,1955.6666870117188
AudioSynthWaveformModulated wfModTriVoice04; //xy=245.00001525878906,1742.6666870117188
AudioSynthWaveformModulated wfModSawVoice04; //xy=245.00001525878906,1793.6666870117188
AudioSynthWaveformModulated wfModArb01Voice02; //xy=249.00001525878906,936.6666870117188
AudioSynthWaveform       wfPulVoice03;   //xy=248.00001525878906,1450.6666870117188
AudioSynthWaveformModulated wfModSinVoice02; //xy=251.00001525878906,722.6666870117188
AudioSynthWaveformModulated wfModPulVoice04; //xy=247.00001525878906,1841.6666870117188
AudioSynthWaveform       wfSawVoice01;   //xy=254.00003051757812,436.77779960632324
AudioSynthWaveformModulated wfModArb02Voice02; //xy=252.00001525878906,986.6666870117188
AudioSynthWaveformModulated wfModArb03Voice04; //xy=248.00001525878906,2008.6666870117188
AudioSynthWaveformModulated wfModTriVoice02; //xy=253.00001525878906,772.6666870117188
AudioSynthWaveformModulated wfModSawVoice02; //xy=253.00001525878906,823.6666870117188
AudioSynthWaveform       wfTriVoice03;   //xy=252.00001525878906,1360.6666870117188
AudioSynthWaveform       wfPulVoice01;   //xy=256.00001525878906,480.66668701171875
AudioSynthWaveformModulated wfModPulVoice02; //xy=255.00001525878906,871.6666870117188
AudioSynthWaveform       wfSubVoice02; //xy=254.4444580078125,1087.77783203125
AudioSynthWaveformModulated wfModArb03Voice02; //xy=256.00001525878906,1038.6666870117188
AudioSynthWaveform       wfSinVoice01;   //xy=259.6666793823242,356.6666793823242
AudioSynthWaveform       wfTriVoice01;   //xy=259.99998474121094,396.1111240386963
AudioSynthWaveform       wfSubVoice04; //xy=266.6666793823242,2058.2222003936768
AudioMixer4              mixPart02Voice03; //xy=450.00001525878906,1552.6666870117188
AudioMixer4              mixPart02Voice01; //xy=458.00001525878906,582.6666870117188
AudioMixer4              mixPart01Voice03; //xy=456.00001525878906,1420.6666870117188
AudioMixer4              mixPart01Voice01; //xy=464.00001525878906,451.66668701171875
AudioMixer4              mixPart01Voice04; //xy=460.00001525878906,1725.6666870117188
AudioMixer4              mixPart01Voice02; //xy=468.00001525878906,755.6666870117188
AudioMixer4              mixPart02Voice04; //xy=467.00001525878906,1909.6666870117188
AudioMixer4              mixPart02Voice02; //xy=475.00001525878906,939.6666870117188
AudioMixer4              mixVoice03;     //xy=638.0000152587891,1477.6666870117188
AudioMixer4              mixVoice01;     //xy=646.0000152587891,507.66668701171875
AudioMixer4              mixVoice04;     //xy=670.0000152587891,1750.6666870117188
AudioMixer4              mixVoice02;     //xy=678.0000152587891,780.6666870117188
AudioSynthWaveformDc     dcWavefolderVoice03; //xy=763.0000152587891,1624.6666870117188
AudioSynthWaveformDc     dcWavefolderVoice01; //xy=771.0000152587891,654.6666870117188
AudioSynthWaveformDc     dcWavefolderVoice04; //xy=782.0000152587891,1813.6666870117188
AudioSynthWaveformDc     dcWavefolderVoice02; //xy=790.0000152587891,843.6666870117188
AudioFilterStateVariable voice01Filter;        //xy=844.6667404174805,509.1110649108887
AudioEffectWaveFolder    wavefolderVoice04; //xy=979.0000152587891,1736.6666870117188
AudioEffectWaveFolder    wavefolderVoice02; //xy=987.0000152587891,766.6666870117188
AudioEffectWaveFolder    wavefolderVoice03; //xy=988.0000152587891,1605.6666870117188
AudioEffectWaveFolder    wavefolderVoice01; //xy=996.0000152587891,635.6666870117188
AudioOutputPT8211_2      pt8211_2_1;     //xy=1156.000015258789,1653.6666870117188
AudioOutputPT8211        pt8211_1;       //xy=1164.000015258789,695.6666870117188
AudioConnection          patchCord1(wfSubVoice03, 0, mixPart02Voice03, 3);
AudioConnection          patchCord2(wfSubVoice01, 0, mixPart02Voice01, 3);
AudioConnection          patchCord3(wfArb02Voice03, 0, mixPart02Voice03, 1);
AudioConnection          patchCord4(wfArb01Voice03, 0, mixPart02Voice03, 0);
AudioConnection          patchCord5(wfArb03Voice03, 0, mixPart02Voice03, 2);
AudioConnection          patchCord6(wfModArb01Voice04, 0, mixPart02Voice04, 0);
AudioConnection          patchCord7(wfModSinVoice04, 0, mixPart01Voice04, 0);
AudioConnection          patchCord8(wfSinVoice03, 0, mixPart01Voice03, 0);
AudioConnection          patchCord9(wfArb02Voice01, 0, mixPart02Voice01, 1);
AudioConnection          patchCord10(wfArb01Voice01, 0, mixPart02Voice01, 0);
AudioConnection          patchCord11(wfArb03Voice01, 0, mixPart02Voice01, 2);
AudioConnection          patchCord12(wfSawVoice03, 0, mixPart01Voice03, 2);
AudioConnection          patchCord13(wfModArb02Voice04, 0, mixPart02Voice04, 1);
AudioConnection          patchCord14(wfModTriVoice04, 0, mixPart01Voice04, 1);
AudioConnection          patchCord15(wfModSawVoice04, 0, mixPart01Voice04, 2);
AudioConnection          patchCord16(wfModArb01Voice02, 0, mixPart02Voice02, 0);
AudioConnection          patchCord17(wfPulVoice03, 0, mixPart01Voice03, 3);
AudioConnection          patchCord18(wfModSinVoice02, 0, mixPart01Voice02, 0);
AudioConnection          patchCord19(wfModPulVoice04, 0, mixPart01Voice04, 3);
AudioConnection          patchCord20(wfSawVoice01, 0, mixPart01Voice01, 2);
AudioConnection          patchCord21(wfModArb02Voice02, 0, mixPart02Voice02, 1);
AudioConnection          patchCord22(wfModArb03Voice04, 0, mixPart02Voice04, 2);
AudioConnection          patchCord23(wfModTriVoice02, 0, mixPart01Voice02, 1);
AudioConnection          patchCord24(wfModSawVoice02, 0, mixPart01Voice02, 2);
AudioConnection          patchCord25(wfTriVoice03, 0, mixPart01Voice03, 1);
AudioConnection          patchCord26(wfPulVoice01, 0, mixPart01Voice01, 3);
AudioConnection          patchCord27(wfModPulVoice02, 0, mixPart01Voice02, 3);
AudioConnection          patchCord28(wfSubVoice02, 0, mixPart02Voice02, 3);
AudioConnection          patchCord29(wfModArb03Voice02, 0, mixPart02Voice02, 2);
AudioConnection          patchCord30(wfSinVoice01, 0, mixPart01Voice01, 0);
AudioConnection          patchCord31(wfTriVoice01, 0, mixPart01Voice01, 1);
AudioConnection          patchCord32(wfSubVoice04, 0, mixPart02Voice04, 3);
AudioConnection          patchCord33(mixPart02Voice03, 0, mixVoice03, 1);
AudioConnection          patchCord34(mixPart02Voice01, 0, mixVoice01, 1);
AudioConnection          patchCord35(mixPart01Voice03, 0, mixVoice03, 0);
AudioConnection          patchCord36(mixPart01Voice01, 0, mixVoice01, 0);
AudioConnection          patchCord37(mixPart01Voice04, 0, mixVoice04, 0);
AudioConnection          patchCord38(mixPart01Voice02, 0, mixVoice02, 0);
AudioConnection          patchCord39(mixPart02Voice04, 0, mixVoice04, 1);
AudioConnection          patchCord40(mixPart02Voice02, 0, mixVoice02, 1);
AudioConnection          patchCord41(mixVoice03, 0, wfModSinVoice04, 1);
AudioConnection          patchCord42(mixVoice03, 0, wfModTriVoice04, 1);
AudioConnection          patchCord43(mixVoice03, 0, wfModSawVoice04, 1);
AudioConnection          patchCord44(mixVoice03, 0, wfModPulVoice04, 1);
AudioConnection          patchCord45(mixVoice03, 0, wfModArb01Voice04, 1);
AudioConnection          patchCord46(mixVoice03, 0, wfModArb02Voice04, 1);
AudioConnection          patchCord47(mixVoice03, 0, wfModArb03Voice04, 1);
AudioConnection          patchCord48(mixVoice03, 0, wavefolderVoice03, 0);
AudioConnection          patchCord49(mixVoice01, 0, wfModSinVoice02, 1);
AudioConnection          patchCord50(mixVoice01, 0, wfModTriVoice02, 1);
AudioConnection          patchCord51(mixVoice01, 0, wfModSawVoice02, 1);
AudioConnection          patchCord52(mixVoice01, 0, wfModPulVoice02, 1);
AudioConnection          patchCord53(mixVoice01, 0, wfModArb01Voice02, 1);
AudioConnection          patchCord54(mixVoice01, 0, wfModArb02Voice02, 1);
AudioConnection          patchCord55(mixVoice01, 0, wfModArb03Voice02, 1);
AudioConnection          patchCord56(mixVoice01, 0, voice01Filter, 0);
AudioConnection          patchCord57(mixVoice04, 0, wavefolderVoice04, 0);
AudioConnection          patchCord58(mixVoice02, 0, wavefolderVoice02, 0);
AudioConnection          patchCord59(dcWavefolderVoice03, 0, wavefolderVoice03, 1);
AudioConnection          patchCord60(dcWavefolderVoice01, 0, wavefolderVoice01, 1);
AudioConnection          patchCord61(dcWavefolderVoice04, 0, wavefolderVoice04, 1);
AudioConnection          patchCord62(dcWavefolderVoice02, 0, wavefolderVoice02, 1);
AudioConnection          patchCord63(voice01Filter, 0, wavefolderVoice01, 0);
AudioConnection          patchCord64(wavefolderVoice04, 0, pt8211_2_1, 1);
AudioConnection          patchCord65(wavefolderVoice02, 0, pt8211_1, 1);
AudioConnection          patchCord66(wavefolderVoice03, 0, pt8211_2_1, 0);
AudioConnection          patchCord67(wavefolderVoice01, 0, pt8211_1, 0);
// GUItool: end automatically generated code

float octaves[NUM_VOICES] = {1.0f, 1.0f, 1.0f, 1.0f};
float freqs[NUM_VOICES] = {220.0f, 523.2511f, 220.0f, 220.0f};

/**
 * Set all gains to 0 on the mixers; 
 * not sure if this causes a click or not, but we
 * primarily use it on startup.
*/
void muteAll() {
    mixPart01Voice01.gain(0, 0);
    mixPart01Voice01.gain(1, 0);
    mixPart01Voice01.gain(2, 0);
    mixPart01Voice01.gain(3, 0);
    mixPart02Voice01.gain(0, 0);
    mixPart02Voice01.gain(1, 0);
    mixPart02Voice01.gain(2, 0);
    mixPart02Voice01.gain(3, 0);
    mixVoice01.gain(0, 0);
    mixVoice01.gain(1, 0);
    mixVoice01.gain(2, 0);
    mixVoice01.gain(3, 0);

    mixPart01Voice02.gain(0, 0);
    mixPart01Voice02.gain(1, 0);
    mixPart01Voice02.gain(2, 0);
    mixPart01Voice02.gain(3, 0);
    mixPart02Voice02.gain(0, 0);
    mixPart02Voice02.gain(1, 0);
    mixPart02Voice02.gain(2, 0);
    mixPart02Voice02.gain(3, 0);
    mixVoice02.gain(0, 0);
    mixVoice02.gain(1, 0);
    mixVoice02.gain(2, 0);
    mixVoice02.gain(3, 0);
}

/**
 * Set all gains to 1 on the mixers; 
*/
void unityAll() {
    mixPart01Voice01.gain(0, 1);
    mixPart01Voice01.gain(1, 1);
    mixPart01Voice01.gain(2, 1);
    mixPart01Voice01.gain(3, 1);
    mixPart02Voice01.gain(0, 1);
    mixPart02Voice01.gain(1, 1);
    mixPart02Voice01.gain(2, 1);
    mixPart02Voice01.gain(3, 1);
    mixVoice01.gain(0, 1);
    mixVoice01.gain(1, 1);
    mixVoice01.gain(2, 1);
    mixVoice01.gain(3, 1);

    mixPart01Voice02.gain(0, 1);
    mixPart01Voice02.gain(1, 1);
    mixPart01Voice02.gain(2, 1);
    mixPart01Voice02.gain(3, 1);
    mixPart02Voice02.gain(0, 1);
    mixPart02Voice02.gain(1, 1);
    mixPart02Voice02.gain(2, 1);
    mixPart02Voice02.gain(3, 1);
    mixVoice02.gain(0, 1);
    mixVoice02.gain(1, 1);
    mixVoice02.gain(2, 1);
    mixVoice02.gain(3, 1);
}

void setupWavefolderValues() {
    dcWavefolderVoice01.amplitude(0.0);
    dcWavefolderVoice02.amplitude(0.0);
    dcWavefolderVoice03.amplitude(0.0);
    dcWavefolderVoice04.amplitude(0.0);
}

void setupMixerWeightsExpanded() {
    for (int i = 0; i < NUM_VOICES; i++) {
        for (int j = 0; j < NUM_MIXER_WEIGHTS_EXPANDED; j++) {
            mixerWeightsExpanded[i][j] = 0.0;
            defaultMixerWeightsExpanded[i][j] = 1.0;
        }
    }
}

void clearMixerWeights() {
    for (int i = 0; i < NUM_VOICES; i++) {
        for (int j = 0; j < NUM_MIXER_WEIGHTS_EXPANDED; j++) {
            mixerWeightsExpanded[i][j] = 0.0;
        }
    }
}

void startSynthesis() {
    // Mute all of the voices
    muteAll();

    // Setup wavefolder values
    setupWavefolderValues();

    // Setup mixer weights
    setupMixerWeightsExpanded();

    //unityAll();
    voice01Filter.frequency(15000);

    // Start all of the waveforms

    // TODO
    // Probably need a way to keep track of all of these, like in
    // an array or something, so that we can
    // easily update the frequencies of them when they change.
    wfSinVoice01.begin(WAVEFORM_SINE);
    wfTriVoice01.begin(WAVEFORM_TRIANGLE);
    wfSawVoice01.begin(WAVEFORM_SAWTOOTH);
    wfPulVoice01.begin(WAVEFORM_PULSE);

    // TODO
    // For now set the "arbitrary" waveforms to
    // the basic waveform types, only for testing
    //wfArb01Voice01.begin(WAVEFORM_TRIANGLE);
    wfArb01Voice01.arbitraryWaveform(AKWF_cello_0002_256_DATA, 172.0);
    wfArb01Voice01.begin(WAVEFORM_ARBITRARY);

    wfArb02Voice01.arbitraryWaveform(AKWF_epiano_0034_256_DATA, 172.0);
    wfArb02Voice01.begin(WAVEFORM_ARBITRARY);

    wfArb03Voice01.arbitraryWaveform(AKWF_flute_0001_256_DATA, 172.0);
    wfArb03Voice01.begin(WAVEFORM_ARBITRARY);


    // now we just repeat

    wfModSinVoice02.begin(WAVEFORM_SINE);
    wfModTriVoice02.begin(WAVEFORM_TRIANGLE);
    wfModSawVoice02.begin(WAVEFORM_BANDLIMIT_SAWTOOTH);
    wfModPulVoice02.begin(WAVEFORM_BANDLIMIT_PULSE);

    wfModArb01Voice02.begin(WAVEFORM_TRIANGLE);
    wfModArb02Voice02.begin(WAVEFORM_BANDLIMIT_SAWTOOTH);
    wfModArb03Voice02.begin(WAVEFORM_BANDLIMIT_PULSE);

    wfSinVoice03.begin(WAVEFORM_SINE);
    wfTriVoice03.begin(WAVEFORM_TRIANGLE);
    wfSawVoice03.begin(WAVEFORM_BANDLIMIT_SAWTOOTH);
    wfPulVoice03.begin(WAVEFORM_BANDLIMIT_PULSE);

    // TODO
    // For now set the "arbitrary" waveforms to
    // the basic waveform types, only for testing
    wfArb01Voice03.begin(WAVEFORM_TRIANGLE);
    wfArb02Voice03.begin(WAVEFORM_BANDLIMIT_SAWTOOTH);
    wfArb03Voice03.begin(WAVEFORM_BANDLIMIT_PULSE);

    wfModSinVoice04.begin(WAVEFORM_SINE);
    wfModTriVoice04.begin(WAVEFORM_TRIANGLE);
    wfModSawVoice04.begin(WAVEFORM_BANDLIMIT_SAWTOOTH);
    wfModPulVoice04.begin(WAVEFORM_BANDLIMIT_PULSE);

    wfModArb01Voice04.begin(WAVEFORM_TRIANGLE);
    wfModArb02Voice04.begin(WAVEFORM_BANDLIMIT_SAWTOOTH);
    wfModArb03Voice04.begin(WAVEFORM_BANDLIMIT_PULSE);

    Voice01Waveforms[0] = &wfSinVoice01;
    Voice01Waveforms[1] = &wfTriVoice01;
    Voice01Waveforms[2] = &wfSawVoice01;
    Voice01Waveforms[3] = &wfPulVoice01;
    Voice01Waveforms[4] = &wfArb01Voice01;
    Voice01Waveforms[5] = &wfArb02Voice01;
    Voice01Waveforms[6] = &wfArb03Voice01;

    Voice02Waveforms[0] = &wfModSinVoice02;
    Voice02Waveforms[1] = &wfModTriVoice02;
    Voice02Waveforms[2] = &wfModSawVoice02;
    Voice02Waveforms[3] = &wfModPulVoice02;
    Voice02Waveforms[4] = &wfModArb01Voice02;
    Voice02Waveforms[5] = &wfModArb02Voice02;
    Voice02Waveforms[6] = &wfModArb03Voice02;

    Voice03Waveforms[0] = &wfSinVoice03;
    Voice03Waveforms[1] = &wfTriVoice03;
    Voice03Waveforms[2] = &wfSawVoice03;
    Voice03Waveforms[3] = &wfPulVoice03;
    Voice03Waveforms[4] = &wfArb01Voice03;
    Voice03Waveforms[5] = &wfArb02Voice03;
    Voice03Waveforms[6] = &wfArb03Voice03;

    Voice04Waveforms[0] = &wfModSinVoice04;
    Voice04Waveforms[1] = &wfModTriVoice04;
    Voice04Waveforms[2] = &wfModSawVoice04;
    Voice04Waveforms[3] = &wfModPulVoice04;
    Voice04Waveforms[4] = &wfModArb01Voice04;
    Voice04Waveforms[5] = &wfModArb02Voice04;
    Voice04Waveforms[6] = &wfModArb03Voice04;

    VoicesSubsWaveforms[0] = &wfSubVoice01;
    VoicesSubsWaveforms[1] = &wfSubVoice02;
    VoicesSubsWaveforms[2] = &wfSubVoice03;
    VoicesSubsWaveforms[3] = &wfSubVoice04;

    // Set all to the same frequency and amplitude
    for (int i = 0; i < NUM_WAVEFORMS; i++) {
        Voice01Waveforms[i]->frequency(440.0);
        Voice01Waveforms[i]->amplitude(0.125);
        Voice02Waveforms[i]->frequency(440.0);
        Voice02Waveforms[i]->amplitude(0.8);
        Voice03Waveforms[i]->frequency(440.0);
        Voice03Waveforms[i]->amplitude(0.0);
        Voice04Waveforms[i]->frequency(440.0);
        Voice04Waveforms[i]->amplitude(0.0);

        // Turn off frequency mod
        Voice02Waveforms[i]->frequencyModulation(0.0);
        Voice04Waveforms[i]->frequencyModulation(0.0);
    }

    for (int i = 0; i < NUM_VOICES; i++) {
        VoicesSubsWaveforms[i]->frequency(440.0);
        VoicesSubsWaveforms[i]->amplitude(0.0);
        VoicesSubsWaveforms[i]->begin(WAVEFORM_BANDLIMIT_SQUARE);
    }




    mixVoice01.gain(0, 0.5);
    mixVoice01.gain(1, 0.5);
    mixVoice02.gain(0, 0.5);
    mixVoice02.gain(1, 0.5);
    mixVoice03.gain(0, 0.5);
    mixVoice03.gain(1, 0.5);
    mixVoice04.gain(0, 0.5);
    mixVoice04.gain(1, 0.5);

}

void showProcessorUsage() {
    W4VES_DEBUG_PRINT(AudioProcessorUsage());
    W4VES_DEBUG_PRINT(" (");    
    W4VES_DEBUG_PRINT(AudioProcessorUsageMax());
    W4VES_DEBUG_PRINT("),  Mem = ");
    W4VES_DEBUG_PRINT(AudioMemoryUsage());
    W4VES_DEBUG_PRINT(" (");    
    W4VES_DEBUG_PRINT(AudioMemoryUsageMax());
    W4VES_DEBUG_PRINTLN(")");
}

void printMixerWeightsExpanded(int voice) {
    for (int i = 0; i < NUM_MIXER_WEIGHTS_EXPANDED; i++) {
        W4VES_DEBUG_PRINT(mixerWeightsExpanded[voice][i]);
        W4VES_DEBUG_PRINT(" ");
    }
    W4VES_DEBUG_PRINTLN("");
}

void printMixerWeights() {
    for (int i = 0; i < NUM_VOICES; i++) {
        for (int j = 0; j < NUM_WAVEFORMS; j++) {
            W4VES_DEBUG_PRINT(mixerWeights[i][j]);
            W4VES_DEBUG_PRINT(" ");
        }
    }
    W4VES_DEBUG_PRINTLN("");
}

void calculatePhaseAngles() {
    int remainder = millis() % 1000;
    float frac = 360.0f/1000.0f;
    for (int i = 0; i < NUM_VOICES; i++) {
        for (int j = 0; j < NUM_WAVEFORMS; j++) {
            // TODO
            // this needs to be tuned, along with UI decisions, but it works!
            //phaseAngles[i][j] = remainder * frac * (j+1) * map(smoothedPotValues[FM_WAVE_001_002_POT], 0.0, 1.0, 0.01, 10);
        }
    }
    //W4VES_DEBUG_PRINTLN(phaseAngles[0][2]);
}

void calculateMixerWeights(int voice, int8_t value) {
    int8_t hanningIndexLow = 0;
    int8_t hanningIndexHigh = HANNING_RANGE;
    int8_t hanningIndexStart = 0;
    bool hanningLow = false;

    if ((value - HANNING_HALF) < 0) {
        hanningIndexLow = 0;
        hanningIndexHigh = HANNING_RANGE - abs(value- HANNING_HALF);
        hanningIndexStart = HANNING_RANGE - hanningIndexHigh;
        hanningLow = true;
    } else if ((value + HANNING_HALF) > NUM_MIXER_WEIGHTS_EXPANDED) {
        hanningIndexLow = value - HANNING_HALF;
        hanningIndexHigh = NUM_MIXER_WEIGHTS_EXPANDED;
        //hanningIndexStart = NUM_MIXER_WEIGHTS_EXPANDED - hanningIndexLow;
        hanningIndexStart = 0;
    } else {
        hanningIndexLow = value - HANNING_HALF;
        hanningIndexHigh = value + HANNING_HALF;
        hanningIndexStart = 0;
    }

    for (int i = hanningIndexLow; i < hanningIndexHigh; i++) {
        if (hanningLow) {
            mixerWeightsExpanded[voice][i] = defaultMixerWeightsExpanded[voice][i] * hanningWindow10[i - hanningIndexLow + hanningIndexStart];
        } else {
            mixerWeightsExpanded[voice][i] = defaultMixerWeightsExpanded[voice][i] * hanningWindow10[i - hanningIndexLow];
        }
    }

    int startIndex = 0;
    int endIndex = 0;
    float tempSum = 0.0;
    float tempMixerWeight = 0.0;
    for (int i = 0; i < NUM_WAVEFORMS; i++) {
        endIndex = defaultWaveformBreakpoints[i+1];
        for (int j = startIndex; j < endIndex; j++) {
            tempSum += mixerWeightsExpanded[voice][j];
        }
        tempMixerWeight = (float)(tempSum / (endIndex - startIndex));
        mixerWeights[voice][i] = tempMixerWeight;
        
        // TODO
        // this is a bit annoying, it limits it to only 4 voices now
        // and doesn't react to he NUM_VOICES parameter...but it's the best way
        // I can think of doing this without OOP'ing everything
        if (tempMixerWeight > 0) {
            if (voice == 0) {
                Voice01Waveforms[i]->amplitude(1.0);
            } else if (voice == 1) {
                Voice02Waveforms[i]->amplitude(1.0);
            } else if (voice == 2) {
                Voice03Waveforms[i]->amplitude(1.0);
            } else if (voice == 3) {
                Voice04Waveforms[i]->amplitude(1.0);
            }
        } else {
            if (voice == 0) {
                Voice01Waveforms[i]->amplitude(0.0);
            } else if (voice == 1) {
                Voice02Waveforms[i]->amplitude(0.0);
            } else if (voice == 2) {
                Voice03Waveforms[i]->amplitude(0.0);
            } else if (voice == 3) {
                Voice04Waveforms[i]->amplitude(0.0);
            }
        }

        startIndex = endIndex;
        tempSum = 0.0;
    }

}

void updateWaveformParams() {
    int8_t tempWFParam = 0;

    clearMixerWeights();

    tempWFParam = (int8_t) round(smoothedPotValues[WAVE001_WF_POT] * 100);
    // TODO
    // this works, but my morphing is not super good right now
    // and i need to adjust this so that we take care of offsets, etc
    //tempWFParam = (int8_t) round(smoothedPotValues[WAVE001_WF_POT] * 100 + smoothedCVADCValues[WAVE001_WF_CV_ADC_INDEX] * 100);
    calculateMixerWeights(0, tempWFParam);

    tempWFParam = (int8_t) round(smoothedPotValues[WAVE002_WF_POT] * 100);
    calculateMixerWeights(1, tempWFParam);

    tempWFParam = (int8_t) round(smoothedPotValues[WAVE003_WF_POT] * 100);
    calculateMixerWeights(2, tempWFParam);

    tempWFParam = (int8_t) round(smoothedPotValues[WAVE004_WF_POT] * 100);
    calculateMixerWeights(3, tempWFParam);

    calculatePhaseAngles();

    AudioNoInterrupts();

    updateMixerWeights();
    updateSubs();
    for (int i = 0; i < NUM_WAVEFORMS; i++) {
        //Voice01Waveforms[i]->frequency(freqs[0] + map(smoothedCVADCValues[0] - 0.21, 0.0, 1.0, 40, 3000));
        Voice01Waveforms[i]->frequency(freqs[0]);
        // TODO
        // This works, but needs tuning and some UI decisions
        //Voice01Waveforms[i]->phase(phaseAngles[0][i]);
    }

    for (int i = 0; i < NUM_WAVEFORMS; i++) {
        Voice02Waveforms[i]->frequency(freqs[1]);
    }

    for (int i = 0; i < NUM_WAVEFORMS; i++) {
        Voice03Waveforms[i]->frequency(freqs[2]);
    }

    for (int i = 0; i < NUM_WAVEFORMS; i++) {
        Voice04Waveforms[i]->frequency(freqs[3]);
    }

    dcWavefolderVoice01.amplitude(smoothedPotValues[WAVE001_MOD_POT]);
    // TODO
    // this basically works, but I need to adjust it to deal with offsets
    // and to allow for a configuration value to allow for unipolar CV
    //dcWavefolderVoice01.amplitude(smoothedPotValues[WAVE001_MOD_POT] + smoothedCVADCValues[WAVE001_MOD_CV_ADC_INDEX]);
    dcWavefolderVoice02.amplitude(smoothedPotValues[WAVE002_MOD_POT]);

    AudioInterrupts();

}

/*
void updateWaveformParams() {
    int8_t tempWF01Param = 0;
    int8_t hanningIndexLow = 0;
    int8_t hanningIndexHigh = HANNING_RANGE;
    int8_t hanningIndexStart = 0;
    bool hanningLow = false;

    tempWF01Param = (int8_t) round(smoothedPotValues[WAVE001_WF_POT] * 100);

    clearMixerWeights();

    if ((tempWF01Param - HANNING_HALF) < 0) {
        hanningIndexLow = 0;
        hanningIndexHigh = HANNING_RANGE - abs(tempWF01Param - HANNING_HALF);
        hanningIndexStart = HANNING_RANGE - hanningIndexHigh;
        hanningLow = true;
    } else if ((tempWF01Param + HANNING_HALF) > NUM_MIXER_WEIGHTS_EXPANDED) {
        hanningIndexLow = tempWF01Param - HANNING_HALF;
        hanningIndexHigh = NUM_MIXER_WEIGHTS_EXPANDED;
        //hanningIndexStart = NUM_MIXER_WEIGHTS_EXPANDED - hanningIndexLow;
        hanningIndexStart = 0;
    } else {
        hanningIndexLow = tempWF01Param - HANNING_HALF;
        hanningIndexHigh = tempWF01Param + HANNING_HALF;
        hanningIndexStart = 0;
    }

    for (int i = hanningIndexLow; i < hanningIndexHigh; i++) {
        if (hanningLow) {
            mixerWeightsExpanded[i] = defaultMixerWeightsExpanded[i] * hanningWindow10[i - hanningIndexLow + hanningIndexStart];
        } else {
            mixerWeightsExpanded[i] = defaultMixerWeightsExpanded[i] * hanningWindow10[i - hanningIndexLow];
        }
    }

    int startIndex = 0;
    int endIndex = 0;
    float tempSum = 0.0;
    float tempMixerWeight = 0.0;
    for (int i = 0; i < NUM_WAVEFORMS; i++) {
        endIndex = defaultWaveformBreakpoints[i+1];
        for (int j = startIndex; j < endIndex; j++) {
            tempSum += mixerWeightsExpanded[j];
        }
        tempMixerWeight = (float)(tempSum / (endIndex - startIndex));
        mixerWeights[i] = tempMixerWeight;

        startIndex = endIndex;
        tempSum = 0.0;
    }


    AudioNoInterrupts();
    for (int i = 0; i < NUM_WAVEFORMS; i++) {
        //Voice01Waveforms[i]->frequency(freqs[0] + map(smoothedCVADCValues[0] - 0.21, 0.0, 1.0, 40, 3000));
        Voice01Waveforms[i]->frequency(freqs[0]);
    }

    for (int i = 0; i < NUM_WAVEFORMS; i++) {
        Voice02Waveforms[i]->frequency(freqs[1]);
    }

    for (int i = 0; i < NUM_WAVEFORMS; i++) {
        Voice03Waveforms[i]->frequency(freqs[2]);
    }

    for (int i = 0; i < NUM_WAVEFORMS; i++) {
        Voice04Waveforms[i]->frequency(freqs[3]);
    }

    mixPart01Voice01.gain(0, abs(mixerWeights[0]));
    mixPart01Voice01.gain(1, abs(mixerWeights[1]));
    mixPart01Voice01.gain(2, abs(mixerWeights[2]));
    mixPart01Voice01.gain(3, abs(mixerWeights[3]));
    mixPart02Voice01.gain(0, abs(mixerWeights[4]));
    mixPart02Voice01.gain(1, abs(mixerWeights[5]));
    mixPart02Voice01.gain(2, abs(mixerWeights[6]));

    dcWavefolderVoice01.amplitude(smoothedPotValues[WAVE001_MOD_POT]);
    dcWavefolderVoice02.amplitude(smoothedPotValues[WAVE002_MOD_POT]);

    AudioInterrupts();

}
*/

void updateSubs() {
    float tempFreq = 0.0f;

    if (voicesSubsValues[0]) {
        tempFreq = freqs[0] * 0.5;

        if (tempFreq < 30.0) {tempFreq = freqs[0];}
        VoicesSubsWaveforms[0]->amplitude(1.0);
        VoicesSubsWaveforms[0]->frequency(tempFreq);
        mixPart02Voice01.gain(3, 0.10);
    } else {
        VoicesSubsWaveforms[0]->amplitude(0.0);
        mixPart02Voice01.gain(3, 0.0);
    }

    if (voicesSubsValues[1]) {
        tempFreq = freqs[1] * 0.5;

        if (tempFreq < 30.0) {tempFreq = freqs[1];}
        VoicesSubsWaveforms[1]->amplitude(1.0);
        VoicesSubsWaveforms[1]->frequency(tempFreq);
        mixPart02Voice02.gain(3, 0.10);
    } else {
        VoicesSubsWaveforms[1]->amplitude(0.0);
        mixPart02Voice02.gain(3, 0.0);
    }

    if (voicesSubsValues[2]) {
        tempFreq = freqs[2] * 0.5;

        if (tempFreq < 30.0) {tempFreq = freqs[2];}
        VoicesSubsWaveforms[2]->amplitude(1.0);
        VoicesSubsWaveforms[2]->frequency(tempFreq);
        mixPart02Voice03.gain(3, 0.10);
    } else {
        VoicesSubsWaveforms[2]->amplitude(0.0);
        mixPart02Voice03.gain(3, 0.0);
    }

    if (voicesSubsValues[3]) {
        tempFreq = freqs[3] * 0.5;

        if (tempFreq < 30.0) {tempFreq = freqs[3];}
        VoicesSubsWaveforms[3]->amplitude(1.0);
        VoicesSubsWaveforms[3]->frequency(tempFreq);
        mixPart02Voice04.gain(3, 0.10);
    } else {
        VoicesSubsWaveforms[3]->amplitude(0.0);
        mixPart02Voice04.gain(3, 0.0);
    }

}

void updateMixerWeights() {
    mixPart01Voice01.gain(0, abs(mixerWeights[0][0]));
    mixPart01Voice01.gain(1, abs(mixerWeights[0][1]));
    mixPart01Voice01.gain(2, abs(mixerWeights[0][2]));
    mixPart01Voice01.gain(3, abs(mixerWeights[0][3]));
    mixPart02Voice01.gain(0, abs(mixerWeights[0][4]));
    mixPart02Voice01.gain(1, abs(mixerWeights[0][5]));
    mixPart02Voice01.gain(2, abs(mixerWeights[0][6]));

    mixPart01Voice02.gain(0, abs(mixerWeights[1][0]));
    mixPart01Voice02.gain(1, abs(mixerWeights[1][1]));
    mixPart01Voice02.gain(2, abs(mixerWeights[1][2]));
    mixPart01Voice02.gain(3, abs(mixerWeights[1][3]));
    mixPart02Voice02.gain(0, abs(mixerWeights[1][4]));
    mixPart02Voice02.gain(1, abs(mixerWeights[1][5]));
    mixPart02Voice02.gain(2, abs(mixerWeights[1][6]));

    mixPart01Voice03.gain(0, abs(mixerWeights[2][0]));
    mixPart01Voice03.gain(1, abs(mixerWeights[2][1]));
    mixPart01Voice03.gain(2, abs(mixerWeights[2][2]));
    mixPart01Voice03.gain(3, abs(mixerWeights[2][3]));
    mixPart02Voice03.gain(0, abs(mixerWeights[2][4]));
    mixPart02Voice03.gain(1, abs(mixerWeights[2][5]));
    mixPart02Voice03.gain(2, abs(mixerWeights[2][6]));

    mixPart01Voice04.gain(0, abs(mixerWeights[3][0]));
    mixPart01Voice04.gain(1, abs(mixerWeights[3][1]));
    mixPart01Voice04.gain(2, abs(mixerWeights[3][2]));
    mixPart01Voice04.gain(3, abs(mixerWeights[3][3]));
    mixPart02Voice04.gain(0, abs(mixerWeights[3][4]));
    mixPart02Voice04.gain(1, abs(mixerWeights[3][5]));
    mixPart02Voice04.gain(2, abs(mixerWeights[3][6]));

}

void processW4ves() {
    // TODO
    // for testing only
    for (int i = 0; i < NUM_VOICES; i++) {
        if (prevOct[i] != octaves[i]) {
            if (prevOct[i] < octaves[i]) {
                freqs[i] = freqs[i] * 2.0f;
            } else if (prevOct[i] > octaves[i]) {
                freqs[i] = freqs[i] / 2.0f;
            }
            prevOct[i] = octaves[i];

        }
    }

    tempFreq = map(smoothedPotValues[WAVE001_VOCT_POT], 0.02, 1.0, C2_HZ, C8_HZ);
    //tempFreq = exp2f(log2f(smoothedPotValues[WAVE001_VOCT_POT] + 1)/(log2f(ADC0_MAX)));
    //tempFreq = map(smoothedPotValues[WAVE001_VOCT_POT], 0.0, 1.0, 32.70320, 4186.009);

    // TODO
    // this is *roughly* correct, but I think we should work with raw
    // adc values when doing the scaling, so we need to fix that
    // also need to ground the switch on the hardware itself!
    tempFreq = tempFreq * exp2f((smoothedCVADCValues[WAVE001_VOCT_CV_ADC_INDEX] - ONE_AND_HALF_VOCT_ADC_VALUE) / VOCT_ADC_VALUE);
    //tempFreq = tempFreq * smoothedCVADCValues[0];
    freqs[0] = tempFreq * octaves[0];
    //freqs[0] = 4000; 
    //W4VES_DEBUG_PRINTLN(freqs[0]);

    tempFreq = map(smoothedPotValues[WAVE002_VOCT_POT], 0.02, 1.0, C2_HZ, C8_HZ);
    tempFreq = tempFreq * exp2f((smoothedCVADCValues[WAVE002_VOCT_CV_ADC_INDEX] - ONE_AND_HALF_VOCT_ADC_VALUE) / VOCT_ADC_VALUE);
    freqs[1] = tempFreq * octaves[1];

    updateWaveformParams();
}

/*
void processW4ves() {



    // TODO
    // for testing only
    for (int i = 0; i < NUM_VOICES; i++) {
        if (prevOct[i] != octaves[i]) {
            if (prevOct[i] < octaves[i]) {
                freqs[i] = freqs[i] * 2.0f;
            } else if (prevOct[i] > octaves[i]) {
                freqs[i] = freqs[i] / 2.0f;
            }
            prevOct[i] = octaves[i];

        }
    }

    tempFreq = map(smoothed_pot_values[WAVE001_VOCT_POT], 0.1, 1.0, 32.70320, 4186.009);
    freqs[0] = tempFreq * octaves[0];

    tempFreq = map(smoothed_pot_values[WAVE002_VOCT_POT], 0.1, 1.0, 32.70320, 4186.009);
    freqs[1] = tempFreq * octaves[1];

    // end testing

    waveformIndex001 = map(smoothed_pot_values[WAVE001_WF_POT], 0.1, 1.0, 1, 10);
    waveformIndex002 = map(smoothed_pot_values[WAVE002_WF_POT], 0.1, 1.0, 1, 10);

    waveformModValue001 = smoothed_pot_values[WAVE001_MOD_POT];
    constrain(waveformModValue001, 0.0, 1.0);
    waveformModValue002 = smoothed_pot_values[WAVE002_MOD_POT];
    constrain(waveformModValue002, 0.0, 1.0);
    waveformModValue003 = smoothed_pot_values[WAVE003_MOD_POT];
    constrain(waveformModValue003, 0.0, 1.0);
    waveformModValue004 = smoothed_pot_values[WAVE004_MOD_POT];
    constrain(waveformModValue004, 0.0, 1.0);

    //fmIndex = map(smoothed_pot_values[2], 0.1, 1.0, 0.0, 10.0 );

    AudioNoInterrupts();
    waveform1.frequency(freqs[0]);
    waveformMod1.frequency(freqs[1]);

    waveformIndex = waveformIndex001;
    // This logic eventually should be moved to outside of the AudioNoInterrupts()
    if ((waveformIndex >= 1.0) && (waveformIndex < 2.5)) {
        waveform1.begin(WAVEFORM_SINE);
    } else if ((waveformIndex >= 2.5) && (waveformIndex < 5.0)) {
        waveform1.begin(WAVEFORM_TRIANGLE);
    } else if ((waveformIndex >= 5.0) && (waveformIndex < 7.5)) {
        waveform1.begin(WAVEFORM_SAWTOOTH);
    } else if ((waveformIndex >= 7.5) && (waveformIndex <= 10.0)) {
        waveform1.begin(WAVEFORM_PULSE);
        waveform1.pulseWidth(waveformModValue001);
    }

    //waveformMod1.frequencyModulation(fmIndex);

    waveformIndex = waveformIndex002;
    // This logic eventually should be moved to outside of the AudioNoInterrupts()
    if ((waveformIndex >= 1.0) && (waveformIndex < 2.5)) {
        waveformMod1.begin(WAVEFORM_SINE);
    } else if ((waveformIndex >= 2.5) && (waveformIndex < 5.0)) {
        waveformMod1.begin(WAVEFORM_TRIANGLE);
    } else if ((waveformIndex >= 5.0) && (waveformIndex < 7.5)) {
        waveformMod1.begin(WAVEFORM_SAWTOOTH);
    } else if ((waveformIndex >= 7.5) && (waveformIndex <= 10.0)) {
        waveformMod1.begin(WAVEFORM_PULSE);
        //waveformMod1.pulseWidth(waveformModValue002);
    }



    AudioInterrupts();

}
*/