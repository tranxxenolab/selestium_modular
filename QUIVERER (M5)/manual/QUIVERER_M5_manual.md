<!-- Being considered as part of the selestium, this chapter describes the QUIVERER (M5)  module: its motivations, its usage, and some suggested avenues of exploration. -->

*ARCHIVAL DISCLAIMER*: As is common with the other artifacts from the dig AG-105, we believe this "module" also comes from "selestium modular", the producer of a set of sound production modules from the early 21st century. For how to link the modules together, please refer to the appropriate chapters of the *selestium* or the æther-net. Our re-creation of it for this edition has tried to follow as closely as possible the design of the module found in the dig, but built using contemporary processes. As a result, there may be some minor deviations from the versions that are seen in the archives.

# Rationale

Everything is in motion; this we have known for a long time, even since before the Transitioning Times. Their understanding of physics was quite close to ours, except they were missing the basic understanding of *how* the nonlinearity of time works, and the integration of the biological with the silicon. But we digress.

Since at the time of the making of these modules there was the understanding that everything was in motion, there were ways, crude or sophisticated, to pick up this motion. One such way, crude but effective, was the piezoelectric element. This device would pick up *vibrations* and translate it into electrical signals. Such a device could thus pick up the vibrations of a string, or a spring, or something hitting the surface that the piezo element was mounted to.

As a result we have seen mention of a number of these kinds of modules in some of the remaining records of the time. *QUIVERER (M5)* is one such module. The surface design confused us for a while, until we again remembered the importance of celestial elements for the makers. Thus we went into a deep trans-temporal search of various asterisms that could match the designs on the front of *QUIVERER (M5)*. Of course the name should have led us in the right direction; indeed, the design is related to the what was known at the time as the M5 globular cluster of stars. We have reason to believe this design was based on an image taken by one of the people involved in *selestium modular*.

Thus, the screws that stick out of the front panel of *QUIVERER (M5)* align with various stars in the outer edges of the cluster. The height of the screws meant that various things could be connected to them--rubber bands, springs, strings, etc.--to thus vibrate and be picked up by the piezo. This thus means that *QUIVERER (M5)* is a kind of analogue instrument for the electronic modular system.

The interface is very simple: an output jack for the signal and a knob to control the amplification of the signal. However, there is also an input jack that would allow an external piezo element to be amplified by the circuit inside; in this case, the internal piezo element would be bypassed.

We also have records that there were other front panels that could be "swapped out" to offer different attachment points or modes of interaction. We plan to offer different panels in the future. As a result, the piezo element is attached in a removable way.

For this edition we are including a set of contemporary rubber bands for you to experiment with. We are also including something special: a feather of a bird, found in the environs near dig AG-105. We are including this because of some other research people in the dig are doing, mostly around analogue instruments. They have found evidence of what was called a "mountain dulcimer" which was a simple drone-based string instrument that was played using discarded bird feathers. We thought this would be an intriguing juxtaposition to make between the analogue and the electric.


# Datums

These are the measurements we have for the QUIVERER (M5)  module, in the units of the time:

* Width: 12HP (60,6mm)
* Depth: 25mm
* Power: 
    * +12V: 4mA
    * -12V: 4mA

As is customary from the time period in question, align the red stripe on your power cable with the white line on the circuit board.

# Quick usage

*ARCHIVAL NOTE*: As with all of the modules from "selestium modular" very little written text is found on the front of the modules; instead, custom iconography is present, the basics of which are found at the beginnings of each chapter in the *selestium* for the modules. 

* Connect a rubber band between two screws to create subtly different tones.
* Connect a rubber band around three or more screws to create different tones for each segment of the band.
* Connect a rubber band around the knob of another module in the system to create interactions between the modules.
* Strike the surface of the module to create an impulse for other modules.
* Use springs instead of rubber bands to allow for longer decays.
* Use the output of the module as the "strike" input of modules that support such features.
