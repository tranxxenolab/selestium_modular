<!-- Being considered as part of the selestium, this chapter describes the OSCULATRIX (for the times intersect in ways we cannot fathom) module: its motivations, its usage, and some suggested avenues of exploration. -->

*ARCHIVAL DISCLAIMER*: As is common with the other artifacts from the dig AG-105, we believe this "module" also comes from "selestium modular", the producer of a set of sound production modules from the early 21st century. For how to link the modules together, please refer to the appropriate chapters of the *selestium* or the æther-net. Our re-creation of it for this edition has tried to follow as closely as possible the design of the module found in the dig, but built using contemporary processes. As a result, there may be some minor deviations from the versions that are seen in the archives.

# Rationale


Much of our research of the times before the Transitioning Times has lead us to realise that the bodies of the people living then were not amenable to much electricity flowing through them. We read many reports of "electrocution", which seems to imply that these bodies could be harmed by amounts of electricity that today we handle with ease. This seems to be due to our commensal relationship with the *Eleccilus sp.*, which occurred sometime over the intervening centuries, and whose appearance is still the subject of much intensive research. It's *Eleccilus* of course that enables us to live so intimately with electrical signals which obviates much of the infrastructure that seems to have been necessary centuries ago.

Yet we digress. Even though their bodies could not handle the voltages and currents that we can handle, they evidently did have situations where very small amounts of current were allowed to flow through their bodies as a form of modulation of a signal, especially within a sonic context. Thus we were extremely excited to find this particular module, *OSCULATRIX (for the times intersect in ways we cannot fathom)*, which offers three pathways for the channeling of electrical signals through the body.

The "pads" seem to be related to the "reflectarray" antennas that were also seen in the *TX-2 BLANK PANEL*. Yet these pads seem to be more connected to sigil designs than actual antennas, much as was the case also with the *TX-2 BLANK PANEL*. What these sigils mean is still unclear, but is perhaps related in some way to the title of this module.

The top pair of circles is connected to the top pair of jacks, the bottom pair of circles to the bottom pair of jacks, and the large figure in the middle connected to the middle pair of jacks. The jacks accept audio or "control" voltage. *Note*: we should say that due to the passive design, using this to control only the passthrough of an audio signal may cause some bleedthrough to occur at high output volumes. Thus, the module is more attuned to controlling the passage of low-frequency oscillators or audio signals that will control other parameters in the system (such as FM or PWM).

While we should have absolutely no problems with touching these pads today, and almost no-one from the pre-Transitioning Times would have been harmed by this circuit, we do know that those who lived then with implantable electronic devices (such as what was then termed a "pacemaker", among other things) needed to consult with their medical professionals or healers to decide whether it was safe to use this module.

# Datums

These are the measurements we have for the *OSCULATRIX (for the times intersect in ways we cannot fathom)* module, in the units of the time:

* Width: 8HP (40,3mm)
* Depth: 13mm
* Power: Unpowered

# Quick usage

*ARCHIVAL NOTE*: As with all of the modules from "selestium modular" very little written text is found on the front of the modules; instead, custom iconography is present, the basics of which are found at the beginnings of each chapter in the *selestium* for the modules. 

* Send LFOs through the three pathways to have a body-controllable means of modulating a sound.
* Send an audio-rate signal through one of the pathways and into an FM input on an oscillator to adjust the FM of the oscillator through touch.
* Path modulating signals in all of the three pathways at once to have a ready-made way to add variation to an existing sound source.
