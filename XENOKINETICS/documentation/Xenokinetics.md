# TODOs

TL074CDR

TL062CDT

Look for this later for a pushbutton switch: https://nl.farnell.com/en-NL/e-switch/1rblk/cap-round-black-pushbutton-switch/dp/2843903

B3F-1050, omron

HT16K33: <https://www.partsnotincluded.com/controlling-led-matrix-with-the-ht16k33/>

<https://nl.farnell.com/vishay/mal215376479e3/cap-47-f-25v-radial-smd/dp/1834250?st=aluminum%2047uf%20smt%2025v>

<https://nl.farnell.com/panasonic/eee1ea100sr/cap-10-f-25v-radial-smd/dp/9696962?st=aluminum%2010uf%20smt%2025v>

Surface mount 1N4001 equivalent: <https://nl.farnell.com/en-NL/multicomp/s1a/rectifier-single-1a-50v-do-214ac/dp/2675036?st=s1a%2050v>

Mini LED Matrix: <https://www.adafruit.com/product/454>

Arranging 

Switch cap testing: B32-1010, B32-2010

* Seems to be okay? ~~Run for a while, see how hot it gets at the lower speed~~
* maybe do the thermal calculations as suggested?
* Order tantalum capacitors, change specifications
* Check on what other parts I need to build more copies
* ~~Buy a couple of sets of heatsinks~~
* Mostly working, need to fix encoder mapping ~~Create sets of voicescales for each set, initialize to 1.0, then save and recall for each set~~
* check on chaos data, see what we can do with that 
* ~~(also, cut vusb and vcc trace so we can still do serial debugging?)~~
* ~~dremel the opening~~
* check on readings for each chaos intensity pot
* ~~Create boot up images at the beginning, of just a sequence of lines going from left to right~~
* ~~Create file for divisors, one on each line, factors of 2, 5, 10~~
* ~~Wire up rotary encoder and switch, ensure they're working~~
* ~~Bring in LM1117 3.3v regulator, wire up from 5V line. Add to schematic, including passives.~~
* ~~Bring into the teensy the two chaos voltages~~
    * ~~Have to buffer and clamp the other output~~
    * This works, although I think I still need to add the resistor ~~Use the Schottky diodes when I get them to ensure they don't go over 3.3v~~
* Tried two channels (first two, should try the others) ~~Try to setup a few pots with the regulated 3.3V, connect into corresponding pins on Teensy~~
* Seems to work, should try all of them ~~Check the trigger outs/gate outs~~
* Check on the MIDI...is this actually working? Or what is going on there? Try with the USB-MIDI cable
    * MIDI works, but not in conjunction with my code for some reason
    * Implemented a #DEFINE right now for turning it on and off
* ~~LM4040...damn datasheet! "bottom view"!!! remember this, it's now working~~
* Done, needed to be around 22 for the gain ~~Up the gain on the audio-out opamp, add to schematic.~~
* ~~Implement CV-in opamp, test. Add to schematic.~~
* Implemented switch, all good ~~Decide on whether to use a switch on the front for trigger/gate? Add to schematic.~~
* ~~Implement jumper for RTC, add to schematic~~
* Okay, not sure what I was doing wrong, but now it's working! Also the basic interface is done. ~~What the heck is going on with the display?~~
* Work on using the pots to both change the divisors (only for test) as well as incorporation the chaos value into the equation
* NEXT VERSION ~~Figure out MAX and LED matrix wiring, add to schematic~~
* ~~Add button for switching cycle sets, use the matrix display to show a different sigil for each set, test ~~ ~~added to schematic~~

# Eurorack infos

* Voltage standards: <https://www.reddit.com/r/synthdiy/comments/8iex2l/eurorack_voltage_standards/>
* Kassutronics ASR: <https://kassu2000.blogspot.com/2018/10/asr-envelope.html>
* Opamp circuits: <https://www.youtube.com/watch?v=aR9ILhbG4AE>
* Very useful forum post about choosing different op-amps: <https://modwiggler.com/forum/viewtopic.php?t=165227>
* Looking at how to make PCB faceplates: <https://github.com/AudioMorphology/waft/wiki/Waft-Front-Panel>
* Doepfer A-100 DIY: <https://doepfer.de/DIY/a100_diy.htm>
* Pots: <https://oddvolt.com/products/t18-potentiometer?variant=41375196414132>
* HP to mm converter: <https://gcoulby.github.io/Eurorack-Unit-HP-Converter/>
* Panel hole sizes: <https://www.modwiggler.com/forum/viewtopic.php?p=3569904&hilit=hole+diameter+9mm#p3569904>

* Thonkiconn jack = 240 mils (6.1 mm) diameter
* Thin pots, like tall trimmers = 255 mils (6.5 mm)
* Metal 9mm pots, encoders = 276 mils (7 mm)
* 6 mm pushbuttons with these caps = 205 mils (5.2 mm), or 140 mils (3.6 mm) for those capless ones.
* sub-mini switches = 200 mils (5.1 mm)
* LEDs obviously take the diameter plus some small margin, e.g. for 3mm LEDs I put 122 mils.

# Calendars and things to examine

* Metonic calendar: <https://en.wikipedia.org/wiki/Metonic_cycle>
* Lunisolar calendar: <https://en.wikipedia.org/wiki/Lunisolar_calendar>
* Callipic cycle: <https://en.wikipedia.org/wiki/Callippic_cycle>
* Saros cycle: <https://en.wikipedia.org/wiki/Saros_(astronomy)>
* Carrington rotation: <https://en.wikipedia.org/wiki/Solar_rotation#Carrington_rotation>
* VSOP87A: <https://www.caglow.com/info/compute/vsop87>

# On the size of numbers

How to represent numbers from the quantum to the cosmic, and find a way to scale them so that we can work with them on semi-human timescales, has been a major area of interest in this project.

64bit unsigned integer: 18.446.744.073.709.551.615

Only gets you to ~585 million years if you count in milliseconds

So, have to scale longer numbers...still represent them, but they will be scaled by orders of magnitude.

Same thing for times shorter than a millisecond...scale up, but keep values the same, just changing the order of magnitude.

Everything here by orders of magnitude, not other more arbitrary values (although that is of course up to you to decide)

Order of magnitude physics: <https://web.stanford.edu/class/ee204/SanjoyMahajanIntro-01-1.pdf>.

# Materials

Big question...why 8? Could we do 10? 14? 16? Need to figure out my ins and outs, but could possibly get by with a multiplexer if need be.

Another idea for dealing with the divisor, without requiring pots for each channel:

* Rotary pot with switch
* Initial state: choosing which channel, select from set of 3 or 4 LEDs (8 or 16 values)
* Press pot to enter modification mode
* Either same set of LEDs, another set, or an LED strip, to select which divisor to use
* LEDs show current value
* Press pot to exit modification mode, save result
* Return to initial state (showing a sequence of sigils)

Or...8x8 LED matrix! Something like this: <https://www.amazon.de/-/en/gp/product/B07YWNLDXW/ref=ox_sc_act_title_2?smid=A1A7E5ILEFA1R3&psc=1>. See here for an editor: <https://xantorohara.github.io/led-matrix-editor/>

## Pots

* 8 for chaotic values
    * What sizes? 10k? 100k? Need to play around with the opamps to know.
    * But wait...if it's not going into the opamps, does it matter?
* 8 for divisor values
    * Are these needed? Or are we going to set via the LEDs?

## Jumpers

* One for gate/trigger selection
* One for RTC usage or not

## Jacks

* 8 mono outs for trigger/gate
* 1 mono out for audio
* 1 CV in for scaling of chaotic values?
* 1 CV in for adjusting overall divisors?
* 1 stereo out for TRS MIDI
* 1 stereo out for DMX?

## OpAmps

* 8 amplifiers for each of the trigger/gates
    * Buffers needed for this?
* 1 amplifier for the audio out
    * Buffer needed for this?
* What passives are needed for each of these circuits?
* Can we get by with 2 TL074s for the triggers and one TL072 for the audio out?

## Voltage regulators

* Could use the 5V rail from the power supply, but maybe better to just create it from the +12V using a LM1117-5.0...might make the module more usable
* Should also get a LM1117-3.3 so that we don't pull too much current from the Teensy

## RTC

* If there is the VBAT for the RTC, the date will be taken from my laptop when I upload
* To change this, do the following:
    * Check RTC jumper, if set:
    * Check for rtc.txt file on bootup
    * Read this file for ISO-formatted date/time
    * Set RTC, remove rtc.txt
    * Will need to be updated for daylight savings time, maybe?

## Ephemeris

* Use this library: <https://github.com/MarScaper/ephemeris>
* Allow for updating based on lat-long coordinates. This can then be saved to EEPROM?
    * Check if loc.txt file exists on boot
    * Read this for lat-long values (lat on first line, long on second)
    * Save to eeprom
    * Remove loc.txt
    * Default lat-long set to null point (0, 0)

## Chaotic circuit

* What do I need here?

## DAC

* PT8211
* What passives do I need? Check datasheet

## CSV files

* Add column for "type" (quantum, cellular, quotidian, human, non-human, geologic, cosmic, feminist)
    * Update code to account for this!
    * Type of course can be adjusted to taste for people, but is useful for organizational purposes
