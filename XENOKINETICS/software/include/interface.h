#ifndef _XENOKINETICS_INTERFACE_H
#define _XENOKINETICS_INTERFACE_H

#define STATE_SIGIL         0
#define STATE_DIVISORS      1
#define STATE_NODIV         -1
#define STATE_DIV1          1
#define STATE_DIV2          2
#define STATE_DIV3          3
#define STATE_DIV4          4
#define STATE_DIV5          5
#define STATE_DIV6          6
#define STATE_DIV7          7
#define STATE_DIV8          8

extern uint8_t currentInterfaceState;


extern int64_t inputKnobValues[MAX_CYCLES];
extern float divisors[MAX_CYCLES];

void checkInterface();
void processInterfaceState(uint8_t state);
void processChaosPins();
void processChaosIntensityPots();
void processCVSs();
void processDisplay();
void processButtons();
void processTrigGateSwitch();
void updateSet();
void initEncoderKnobValues();
uint8_t processSetCVValue();
void displayImage(uint64_t image);
uint16_t readADC(int adcChannel);
void printChaosIntensityPotValues();
float readChaosIntensityPot(uint8_t i);

#endif
