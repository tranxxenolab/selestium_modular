#ifndef _XENOKINETICS_CV_TRANSFORM_H
#define _XENOKINETICS_CV_TRANSFORM_H

// Inspired heavily by <https://github.com/pichenettes/eurorack/blob/master/clouds/cv_scaler.cc>

struct CVTransform {
    bool invert;
    bool removeOffset;
    float filterCoeff;
};

extern CVTransform chaosIntensitiesTransforms[MAX_CYCLES];
extern CVTransform chaosPinsTransforms[MAX_CHAOS_PINS];
extern CVTransform chaosCVTransform;
extern CVTransform setCVTransform;
//extern float smoothed_cv_adc_values[NUM_PARAMS];
//extern float smoothed_pot_values[NUM_PARAMS];

#endif