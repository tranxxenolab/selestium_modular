#ifndef _XENOKINETICS_MAIN_H
#define _XENOKINETICS_MAIN_H

#include <Arduino.h>

#include <Audio.h>
#include <Wire.h>
#include <SPI.h>
#include <SD.h>
#include <SerialFlash.h>
#include <TimeLib.h>
#include <Bounce.h>
#include <ResponsiveAnalogRead.h>
//#include <TaskScheduler.h>
#include <TaskSchedulerDeclarations.h>
#include <MIDI.h>
#include <EEPROM.h>
#include <HardwareSerial.h>
#include <Encoder.h>
#include <LedControl.h>
#include <LedControl_HW_SPI.h>
//#include <MD_Parola.h>
//#include <MD_MAX72xx.h>
#include <Smoothed.h>
#include <ADC.h>
//#include <ADC_util.h>

#include "config.h"
#include "debug.h"
#include "cv_transform.h"
#include "scheduling.h"
#include "synthesis.h"
#include "eeprom_addr.h"
#include "interface.h"
#include "file_handling.h"




#endif