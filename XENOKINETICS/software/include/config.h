#ifndef _XENOKINETICS_CONFIG_H
#define _XENOKINETICS_CONFIG_H

/*

Pin assignments:

* 41-34: Trigers 1-8 out
* 7: DIN
* 21: BCK
* 20: WS
* MIDI Serial:
  * 7: RX2
  * 8: TX2
* Rotary encoder: 3 pins, all digital
  * 2
  * 3
  * 4
* Display:
  * 10: CS
  * 11: DIN (MOSI?)
  * 13: SCK
* HC4067 multiplexer:
  * 8 channels of pots for chaos divisor (one per trigger out)
  * 1 CV in for control of chaos
  * 6 analog inputs remaining
  * SIG: 27 (A13)
  * 28: S0
  * 29: S1
  * 30: S2
  * 31: S3
* 5: Jumper pin for gate/trigger...rather than a jumper, shall we use a switch? and maybe a trim pot on the back?
* 6: Juper pin for using RTC or not
* 

*/

#define AUDIO_MEMORY        24

#define GATE_TRIG_JUMPER    5
#define RTC_JUMPER          6

#define CHAOS_CV_PIN        A12
#define SET_CV_PIN        A13

#define ENCODER_1           2
#define ENCODER_2           3
#define ENCODER_BUTTON      4

#define CS_PIN              10
#define DIN_PIN             11
#define CLK_PIN             13

#define TRIG_GATE_SW        5
#define TRIG_STATE          1
#define GATE_STATE          0

#define SET_BUTTON          8
#define DIVISOR_BUTTON      9

#define MAX_CHAOS_PINS      3
#define CHAOS_PINS_RESOLUTION 4096
#define MAX_CHAOS_VALUE     300.0

// How many cycles are we working with?
#define MAX_CYCLES  8
#define MAX_VOICES  8
#define MAX_SETS    8

#define DEFAULT_WAVEFORM WAVEFORM_SAWTOOTH

// trigger/gate length in ms
#define DEFAULT_TRIGGER_LENGTH  10
#define DEFAULT_GATE_LENGTH     100

// how many milliseconds to let the pots settle before we update
// the interval
#define CHAOS_POT_SETTLE_DELAY  300

// what range of values is considered "0"?
#define CHAOS_POT_ZERO_RANGE  240
#define CHAOS_POT_ZERO_LOW    2048 - CHAOS_POT_ZERO_RANGE
#define CHAOS_POT_ZERO_HIGH   2048 + CHAOS_POT_ZERO_RANGE

// C4
#define DEFAULT_FUNAMDENTAL_FREQUENCY 261.63

#define NEVER_RUN_STATE       0x00
#define POST_FIRST_RUN_STATE  0xFF

// Default latitude and longitude
// Given as floats, so decimal notation
#define DEFAULT_LAT   0.00
#define DEFAULT_LON   0.00

// Output trigger/gate channels and their correspondences in MIDI
#define ENABLE_MIDI         0
#define DEFAULT_MIDI_CHAN   5
#define DEFAULT_MIDI_VEL    127
#define TRIG1_MIDI          0
#define TRIG2_MIDI          1
#define TRIG3_MIDI          2
#define TRIG4_MIDI          3
#define TRIG5_MIDI          4
#define TRIG6_MIDI          5
#define TRIG7_MIDI          6
#define TRIG8_MIDI          7

#define KNOB_MIN            0
#define KNOB_MAX            64
#define ENCODER_KNOB_UNIT        8

#define MAX_DISPLAY_ROWS    8

#define MAX_FREQS           32
#define MAX_COMMENT_LEN     128

#define ADC_AVGS            16
#define ADC_RESOLUTION      16

extern int midiNotes[MAX_CYCLES];
extern int8_t midiFlags[MAX_CYCLES];

//using Transport = MIDI_NAMESPACE::SerialMIDI<HardwareSerial>;
//HardwareSerial mySerial = HardwareSerial(Serial1);
//Transport serialMIDI(mySerial);
//typedef MIDI_NAMESPACE::MidiInterface<Transport> MyMIDI;
//extern MyMIDI TestMIDI;

// Data structure for holding information about each cycle
struct CycleInfo {
  uint64_t period;
  float scale;
  uint64_t periodCorrected;
  uint32_t offset;
  char comment[MAX_COMMENT_LEN];
};



struct CyclesSet {
  //Cycle *(cyclesSet[MAX_CYCLES]);
  //int testing[8];
  CycleInfo cyclesSet[MAX_CYCLES];
};


// Envelope structure, along with length of NoteOn
typedef struct {
  int attack;
  int decay;
  float sustain;
  int release;
  int duration;
} Env;


class Cycle {
  private:
    uint64_t period;
    float scale;
    uint64_t periodCorrected;
    uint32_t offset;
    char comment[MAX_COMMENT_LEN];
    CycleInfo cycle;
    int waveform;
    Env envelope;
    float frequencies[MAX_FREQS];
    uint16_t frequencyIndex;

  public:
    char commentPublic[MAX_COMMENT_LEN];

    Cycle();

    void setPeriod(uint64_t period);
    void setScale(float scale);
    void setPeriodCorrected(uint64_t periodCorrected);
    void setOffset(uint32_t offset);
    void setComment(char comment[MAX_COMMENT_LEN]);
    void setCycle(CycleInfo cycleInfo);
    void setWaveform(int waveform);
    void setEnvelope(Env envelope);
    uint64_t getPeriod();
    float getScale();
    uint64_t getPeriodCorrected();
    uint32_t getOffset();
    void getComment(char comment[MAX_COMMENT_LEN]);
    CycleInfo getCycle();
    int getWaveform();
    Env getEnvelope();

};

struct CyclesSetClass {
  //Cycle *(cyclesSet[MAX_CYCLES]);
  //int testing[8];
  Cycle cyclesSet[MAX_CYCLES];
};

struct Settings {
  uint16_t gateLength = DEFAULT_GATE_LENGTH;
  float latValue = DEFAULT_LAT;
  float lonValue = DEFAULT_LON;
  bool knobsRandom = false;
  bool setCVTrigger = false;

  bool envelopesDefault[MAX_CYCLES] = {1, 1, 1, 1, 1, 1, 1, 1};
  bool waveformsDefault[MAX_CYCLES] = {1, 1, 1, 1, 1, 1, 1, 1};
  bool frequenciesDefault[MAX_CYCLES] = {1, 1, 1, 1, 1, 1, 1, 1};
  bool gatesDefault[MAX_CYCLES] = {1, 1, 1, 1, 1, 1, 1, 1};
  bool voicesEnabled[MAX_CYCLES] = {1, 1, 1, 1, 1, 1, 1, 1};
  int divisorsIndexDefault[MAX_SETS][MAX_CYCLES] = {
    {4, 4, 4, 4, 4, 4, 4, 4},
    {4, 4, 4, 4, 4, 4, 4, 4},
    {4, 4, 4, 4, 4, 4, 4, 4},
    {4, 4, 4, 4, 4, 4, 4, 4},
    {4, 4, 4, 4, 4, 4, 4, 4},
    {4, 4, 4, 4, 4, 4, 4, 4},
    {4, 4, 4, 4, 4, 4, 4, 4},
    {4, 4, 4, 4, 4, 4, 4, 4},
  };
  // TODO
  // Redo the naming of these constants, as it's confusing
  // it's number of sets (8) and number of divisor options (8 + 1)
  float divisors[MAX_SETS][MAX_CYCLES + 1] = {
    {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f},
    {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f},
    {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f},
    {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f},
    {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f},
    {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f},
    {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f},
    {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f}
  };

};

extern Settings settings;



// Initialize our cycles array
extern CycleInfo cycles[MAX_CYCLES];

extern CycleInfo set001[MAX_CYCLES];
extern CycleInfo set002[MAX_CYCLES];
extern CycleInfo set003[MAX_CYCLES];
extern CycleInfo set004[MAX_CYCLES];
extern CycleInfo set005[MAX_CYCLES];
extern CycleInfo set006[MAX_CYCLES];
extern CycleInfo set007[MAX_CYCLES];
extern CycleInfo set008[MAX_CYCLES];

extern CyclesSet cyclesSet001;
extern CyclesSet cyclesSet002;
extern CyclesSet cyclesSet003;
extern CyclesSet cyclesSet004;
extern CyclesSet cyclesSet005;
extern CyclesSet cyclesSet006;
extern CyclesSet cyclesSet007;
extern CyclesSet cyclesSet008;
extern CyclesSet cyclesSets[MAX_CYCLES];

extern Cycle currentCyclesListClass[MAX_CYCLES];
extern Cycle set001Class[MAX_CYCLES];
extern Cycle set002Class[MAX_CYCLES];
extern Cycle set003Class[MAX_CYCLES];
extern Cycle set004Class[MAX_CYCLES];
extern Cycle set005Class[MAX_CYCLES];
extern Cycle set006Class[MAX_CYCLES];
extern Cycle set007Class[MAX_CYCLES];
extern Cycle set008Class[MAX_CYCLES];
extern CyclesSetClass cyclesSet001Class;
extern CyclesSetClass cyclesSet002Class;
extern CyclesSetClass cyclesSet003Class;
extern CyclesSetClass cyclesSet004Class;
extern CyclesSetClass cyclesSet005Class;
extern CyclesSetClass cyclesSet006Class;
extern CyclesSetClass cyclesSet007Class;
extern CyclesSetClass cyclesSet008Class;
extern CyclesSetClass cyclesSetsClass[MAX_CYCLES];


extern float cycleScales[MAX_CYCLES];

extern ResponsiveAnalogRead chaosCV;
extern ResponsiveAnalogRead scaleCV;


extern Env envelopeValues[MAX_CYCLES];

extern int noteDurations[MAX_CYCLES];
extern int noteDurationsCounter[MAX_CYCLES];

extern float defaultNoteRatios[MAX_CYCLES];
extern float noteRatios[MAX_CYCLES];

extern float fundamentalFrequency;

extern int triggerDurations[MAX_CYCLES];

// ADC infos
// A10, A11 only on ADC1
// A12, A13, A14, A15 only on ADC2

extern ADC *adc;
extern uint16_t adc0MaxValue;
extern uint16_t adc1MaxValue;
extern int chaosIntensityPotsMapping[MAX_CYCLES];
extern float chaosIntensityPotsValues[MAX_CYCLES];
extern uint32_t chaosIntensityPotsTimes[MAX_CYCLES];
extern float chaosCVValue;
extern float setCVValue;
extern int chaosPinsMapping[MAX_CHAOS_PINS];
extern float chaosPinsValues[MAX_CHAOS_PINS];


extern int chaosPins[MAX_CHAOS_PINS];
extern int triggerPins[MAX_CYCLES];
extern int chaosIntensityPins[MAX_CYCLES];
extern Smoothed <float> *chaosPinsAvgs[MAX_CHAOS_PINS];
extern Smoothed <float> chaosPinXAvg; 
extern Smoothed <float> chaosPinYAvg;
extern Smoothed <float> chaosPinZAvg;



extern float chaosIntensities[MAX_CYCLES];
extern float defaultDivisorsSet[MAX_SETS + 1];
extern float divisorsSet[MAX_SETS + 1];
extern float allDivisors[MAX_SETS][MAX_CYCLES];

extern uint16_t firstRun;

extern bool trigGate;

extern float latValue;
extern float lonValue;

//extern char muxValue;
//extern uint16_t chaosCVValue;
//extern uint16_t scaleCVValue;


//extern LedControl display;
extern LedControl_HW_SPI display;
//#define HARDWARE_TYPE MD_MAX72XX::PAROLA_HW
//#define HARDWARE_TYPE MD_MAX72XX::GENERIC_HW
#define MAX_DEVICES 1
//extern MD_Parola display;
const uint8_t F_SIGIL = 1;
const uint8_t W_SIGIL = 8;
extern uint64_t sigil_image000;
extern uint64_t sigil_image001;
extern uint64_t sigil_image002;
extern uint64_t sigil_image003;
extern uint64_t sigil_image004;
extern uint64_t sigil_image005;
extern uint64_t sigil_image006;
extern uint64_t sigil_image007;
extern uint64_t sigilList[MAX_CYCLES];
extern uint64_t defaultSigilList[MAX_CYCLES];
extern uint8_t waveformList[MAX_CYCLES];
extern uint8_t defaultWaveformList[MAX_CYCLES];

extern const uint64_t voiceDivisorInterface;
extern const char barValues[MAX_DISPLAY_ROWS + 1];
extern uint64_t scaleImage;
extern const uint8_t sigil[F_SIGIL * W_SIGIL];
extern const uint8_t sigil_test[8];
//extern const uint64_t sigil;

extern const uint64_t bootImages[MAX_DISPLAY_ROWS + 1];

extern Bounce setButton;
extern uint8_t setIndex;
extern Bounce divisorButton;
extern uint8_t divisorIndex;

extern bool trigGateState;

extern const String cyclesSetsFilenames[MAX_CYCLES];
extern const String waveformsFilenames[MAX_CYCLES];
extern const String envelopesFilenames[MAX_CYCLES];
extern const String frequenciesFilenames[MAX_CYCLES];
extern const String gatesFilenames[MAX_CYCLES];

void initializeAllDivisors();
void initializeChaosPinsAvgs();
void initADC();
void initADCInputs();
void initCycleSet();
void initDivisorsSettings();

// Taken from here: <https://stackoverflow.com/questions/4548004/how-to-correctly-and-standardly-compare-floats>
bool areEqualRel(float a, float b, float epsilon = 0.0000001); 


#endif
