#include "main.h"

Settings settings;

float defaultFrequencies[MAX_FREQS] = {
  -1, -1, -1, -1, -1, -1, -1, -1,
  -1, -1, -1, -1, -1, -1, -1, -1,
  -1, -1, -1, -1, -1, -1, -1, -1,
  -1, -1, -1, -1, -1, -1, -1, -1
};

const String cyclesSetsFilenames[MAX_SETS] = {
  "set001.csv",
  "set002.csv",
  "set003.csv",
  "set004.csv",
  "set005.csv",
  "set006.csv",
  "set007.csv",
  "set008.csv"
};

const String waveformsFilenames[MAX_SETS] = {
  "waveforms_set001.txt",
  "waveforms_set002.txt",
  "waveforms_set003.txt",
  "waveforms_set004.txt",
  "waveforms_set005.txt",
  "waveforms_set006.txt",
  "waveforms_set007.txt",
  "waveforms_set008.txt"
};

const String envelopesFilenames[MAX_SETS] = {
  "envelopes_set001.csv",
  "envelopes_set002.csv",
  "envelopes_set003.csv",
  "envelopes_set004.csv",
  "envelopes_set005.csv",
  "envelopes_set006.csv",
  "envelopes_set007.csv",
  "envelopes_set008.csv"
};

const String frequenciesFilenames[MAX_CYCLES] = {
  "frequencies_set001.csv",
  "frequencies_set002.csv",
  "frequencies_set003.csv",
  "frequencies_set004.csv",
  "frequencies_set005.csv",
  "frequencies_set006.csv",
  "frequencies_set007.csv",
  "frequencies_set008.csv"
};

const String gatesFilenames[MAX_CYCLES] = {
  "gates_set001.csv",
  "gates_set002.csv",
  "gates_set003.csv",
  "gates_set004.csv",
  "gates_set005.csv",
  "gates_set006.csv",
  "gates_set007.csv",
  "gates_set008.csv"
};



// TODO
// Define the default cycles here, which
// are then updated in the code by reading the file

Cycle currentCyclesListClass[MAX_CYCLES] = {
  Cycle(),
  Cycle(),
  Cycle(),
  Cycle(),
  Cycle(),
  Cycle(),
  Cycle(),
  Cycle(),
};

Cycle set001Class[MAX_CYCLES] = {
  Cycle(),
  Cycle(),
  Cycle(),
  Cycle(),
  Cycle(),
  Cycle(),
  Cycle(),
  Cycle(),
};

Cycle set002Class[MAX_CYCLES] = {
  Cycle(),
  Cycle(),
  Cycle(),
  Cycle(),
  Cycle(),
  Cycle(),
  Cycle(),
  Cycle(),
};

Cycle set003Class[MAX_CYCLES] = {
  Cycle(),
  Cycle(),
  Cycle(),
  Cycle(),
  Cycle(),
  Cycle(),
  Cycle(),
  Cycle(),
};

Cycle set004Class[MAX_CYCLES] = {
  Cycle(),
  Cycle(),
  Cycle(),
  Cycle(),
  Cycle(),
  Cycle(),
  Cycle(),
  Cycle(),
};

Cycle set005Class[MAX_CYCLES] = {
  Cycle(),
  Cycle(),
  Cycle(),
  Cycle(),
  Cycle(),
  Cycle(),
  Cycle(),
  Cycle(),
};

Cycle set006Class[MAX_CYCLES] = {
  Cycle(),
  Cycle(),
  Cycle(),
  Cycle(),
  Cycle(),
  Cycle(),
  Cycle(),
  Cycle(),
};

Cycle set007Class[MAX_CYCLES] = {
  Cycle(),
  Cycle(),
  Cycle(),
  Cycle(),
  Cycle(),
  Cycle(),
  Cycle(),
  Cycle(),
};

Cycle set008Class[MAX_CYCLES] = {
  Cycle(),
  Cycle(),
  Cycle(),
  Cycle(),
  Cycle(),
  Cycle(),
  Cycle(),
  Cycle(),
};

CyclesSetClass cyclesSet001Class = { *set001Class };
CyclesSetClass cyclesSet002Class = { *set002Class };
CyclesSetClass cyclesSet003Class = { *set003Class };
CyclesSetClass cyclesSet004Class = { *set004Class };
CyclesSetClass cyclesSet005Class = { *set005Class };
CyclesSetClass cyclesSet006Class = { *set006Class };
CyclesSetClass cyclesSet007Class = { *set007Class };
CyclesSetClass cyclesSet008Class = { *set008Class };
CyclesSetClass cyclesSetsClass[MAX_CYCLES] = {
  cyclesSet001Class,
  cyclesSet002Class,
  cyclesSet003Class,
  cyclesSet004Class,
  cyclesSet005Class,
  cyclesSet006Class,
  cyclesSet007Class,
  cyclesSet008Class,
};


CycleInfo cycles[MAX_CYCLES] = {
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""}
};

CycleInfo set001[MAX_CYCLES] = {
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""}
};

CycleInfo set002[MAX_CYCLES] = {
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""}
};

CycleInfo set003[MAX_CYCLES] = {
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""}
};

CycleInfo set004[MAX_CYCLES] = {
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""}
};

CycleInfo set005[MAX_CYCLES] = {
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""}
};

CycleInfo set006[MAX_CYCLES] = {
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""}
};

CycleInfo set007[MAX_CYCLES] = {
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""}
};

CycleInfo set008[MAX_CYCLES] = {
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""}
};


CyclesSet cyclesSet001 = { *set001 };
CyclesSet cyclesSet002 = { *set002 };
CyclesSet cyclesSet003 = { *set003 };
CyclesSet cyclesSet004 = { *set004 };
CyclesSet cyclesSet005 = { *set005 };
CyclesSet cyclesSet006 = { *set006 };
CyclesSet cyclesSet007 = { *set007 };
CyclesSet cyclesSet008 = { *set008 };
CyclesSet cyclesSets[MAX_CYCLES] = {
  cyclesSet001,
  cyclesSet002,
  cyclesSet003,
  cyclesSet004,
  cyclesSet005,
  cyclesSet006,
  cyclesSet007,
  cyclesSet008,
};

//float chaosIntensities[MAX_CYCLES] = {0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1};

Env envelopeValues[MAX_CYCLES] = {
  {60, 30, 0.5, 1000, 1000},
  {60, 30, 0.5, 1000, 1000},
  {60, 30, 0.5, 1000, 1000},
  {60, 30, 0.5, 1000, 1000},
  {60, 30, 0.5, 1000, 1000},
  {60, 30, 0.5, 1000, 1000},
  {60, 30, 0.5, 1000, 1000},
  {60, 30, 0.5, 1000, 1000}
};

int noteDurations[MAX_CYCLES] = {
  500,
  500,
  500,
  500,
  500,
  500,
  500,
  500
};

int noteDurationsCounter[MAX_CYCLES] = {
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0
};

int triggerDurations[MAX_CYCLES] = {
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0
};

//int triggerPins[MAX_CYCLES] = {41, 40, 39, 38, 37, 36, 35, 34};
int chaosPins[MAX_CHAOS_PINS] = {A10, A11, A8};
int triggerPins[MAX_CYCLES] = {39, 38, 37, 36, 35, 34, 33, 32};
// I am so silly...didn't arrange the pots correctly on the board...ooops
int chaosIntensityPins[MAX_CYCLES] = {A0, A4, A1, A5, A2, A16, A3, A17};
float chaosIntensities[MAX_CYCLES] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};

float defaultNoteRatios[MAX_CYCLES] = {
  1.0,
  6.0/5.0,
  4.0/3.0,
  3.0/2.0,
  44.0/27.0,
  40.0/21.0,
  2.0/1.0,
  9.0/4.0
};

float noteRatios[MAX_CYCLES] = {
  1.0,
  6.0/5.0,
  4.0/3.0,
  3.0/2.0,
  44.0/27.0,
  40.0/21.0,
  2.0/1.0,
  9.0/4.0
};

float fundamentalFrequency = DEFAULT_FUNAMDENTAL_FREQUENCY;

//float voiceDivisorsSet[MAX_VOICES + 1] = {0.0001, 0.001, 0.01, 0.1, 1.0, 10, 100, 1000, 10000};
float defaultDivisorsSet[MAX_SETS + 1] = {0.0625, 0.125, 0.25, 0.5, 1.0, 2, 8, 16, 32};
float divisorsSet[MAX_SETS + 1] = {0.0625, 0.125, 0.25, 0.5, 1.0, 2, 8, 16, 32};
float allDivisors[MAX_SETS][MAX_CYCLES];

float latValue = DEFAULT_LAT;
float lonValue = DEFAULT_LON;

bool trigGate = false;

ADC *adc = new ADC(); // adc object;
uint16_t adc0MaxValue = 1024;
uint16_t adc1MaxValue = 1024;

//int chaosIntensityPotsMapping[MAX_CYCLES] = {A0, A1, A2, A3, A4, A5, A7, A8};
int chaosIntensityPotsMapping[MAX_CYCLES] = {A0, A4, A1, A5, A2, A16, A3, A17};
float chaosIntensityPotsValues[MAX_CYCLES] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
uint32_t chaosIntensityPotsTimes[MAX_CYCLES] = {0, 0, 0, 0, 0, 0, 0, 0};
int chaosPinsMapping[MAX_CHAOS_PINS] = {A10, A11, A8};
float chaosPinsValues[MAX_CHAOS_PINS] = {0.0, 0.0, 0.0};
float chaosCVValue = 0.0f;
float setCVValue = 0.0f;
Smoothed <float> *chaosPinsAvgs[MAX_CHAOS_PINS];
Smoothed <float> chaosPinXAvg; 
Smoothed <float> chaosPinYAvg;
Smoothed <float> chaosPinZAvg;


//LedControl display = LedControl(12, 11, 10, 1);
//LedControl(DIN_PIN, CLK_PIN, CS_PIN, 1);

const uint64_t IMAGES[] = {
  0xff000001010000ff, 0xff000003030000ff, 0xff000006060000ff,
  0xff00000c0c0000ff, 0xff000018180000ff, 0xff000030300000ff,
  0xff000060600000ff, 0xff0000c0c00000ff, 0xff000080800000ff,
  0xff0000c0c00000ff, 0xff000060600000ff, 0xff000018180000ff,
  0xff00000c0c0000ff, 0xff000006060000ff, 0xff000003030000ff,
  0xff000001010000ff
};
const int IMAGES_LEN = sizeof(IMAGES)/8;

const uint64_t bootImages[MAX_DISPLAY_ROWS + 1] = {
  0x0000000000000000,
  0x8080808080808080,
  0xc0c0c0c0c0c0c0c0,
  0xe0e0e0e0e0e0e0e0,
  0xf0f0f0f0f0f0f0f0,
  0xf8f8f8f8f8f8f8f8,
  0xfcfcfcfcfcfcfcfc,
  0xfefefefefefefefe,
  0xffffffffffffffff
  
};

/*
  0x0000000000000000,
  0x00000000000000ff,
  0x000000000000ffff,
  0x0000000000ffffff,
  0x00000000ffffffff,
  0x000000ffffffffff,
  0x0000ffffffffffff,
  0x00ffffffffffffff,
  0xffffffffffffffff

*/


/*
  0x0101010101010101,
  0x0303030303030303,
  0x0707070707070707,
  0x0f0f0f0f0f0f0f0f,
  0x1f1f1f1f1f1f1f1f,
  0x3f3f3f3f3f3f3f3f,
  0x7f7f7f7f7f7f7f7f,
  0xffffffffffffffff

*/


LedControl_HW_SPI display = LedControl_HW_SPI();
//MD_Parola display = MD_Parola(HARDWARE_TYPE, CS_PIN, MAX_DEVICES);
//const char barValues[MAX_DISPLAY_ROWS + 1] = {0x00, 0x01, 0x03, 0x07, 0x0f, 0x1f, 0x3f, 0x7f, 0xff};
const char barValues[MAX_DISPLAY_ROWS + 1] = {0x00, 0x80, 0xc0, 0xe0, 0xf0, 0xf8, 0xfc, 0xfe, 0xff};
//const char barValues[MAX_DISPLAY_ROWS + 1] = {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08};
//const char barValues[MAX_DISPLAY_ROWS + 1] = {0x00, 0x10, 0x30, 0x70, 0xf0, 0xf1, 0xf3, 0xf7, 0xff};
//const char barValues[MAX_DISPLAY_ROWS + 1] = {0x00, 0x08, 0x0c, 0x0e, 0x0f, 0xf8, 0xfc, 0xfe, 0xff};
/*
const uint64_t sigil_image000 = 0x83c3a498b8f4e2ff;
const uint64_t sigil_image001 = 0x99a5bd6666bda599;
const uint64_t sigil_image002 = 0xe496a96f0a0a0a0c;
const uint64_t sigil_image003 = 0x08783b1b1d81e3e3;
const uint64_t sigil_image004 = 0x1824dbdb24180000;
const uint64_t sigil_image005 = 0x22a2aaaaaaaaa222;
const uint64_t sigil_image006 = 0x42e54915224a82fe;
const uint64_t sigil_image007 = 0xe44a514e4e4e7e00;



const uint64_t sigil_image000 = 0xff472f1d1925c3c1;
const uint64_t sigil_image001 = 0x99a5bd6666bda599;
const uint64_t sigil_image002 = 0x30505050f6956927;
const uint64_t sigil_image003 = 0xc7c781b8d8dc1e10;
const uint64_t sigil_image004 = 0x00001824dbdb2418;
const uint64_t sigil_image005 = 0x4445555555554544;
const uint64_t sigil_image006 = 0x7f415244a892a742;
const uint64_t sigil_image007 = 0x007e7272728a5227;

*/

uint64_t sigil_image000 = 0xc1c325191d2f47ff;
uint64_t sigil_image001 = 0xe7187ea5a57e18e7;
uint64_t sigil_image002 = 0x305ed13f40b090e0;
uint64_t sigil_image003 = 0x3f3308f878634307;
uint64_t sigil_image004 = 0x303048b4b4483030;
uint64_t sigil_image005 = 0x00ff003c00ff007e;
uint64_t sigil_image006 = 0x708f51251149e543;
uint64_t sigil_image007 = 0x205e9e5e2282fe80;


uint64_t sigilList[MAX_CYCLES] = {
  sigil_image000, 
  sigil_image001, 
  sigil_image002, 
  sigil_image003, 
  sigil_image004, 
  sigil_image005, 
  sigil_image006, 
  sigil_image007 
};

uint64_t defaultSigilList[MAX_CYCLES] = {
  sigil_image000, 
  sigil_image001, 
  sigil_image002, 
  sigil_image003, 
  sigil_image004, 
  sigil_image005, 
  sigil_image006, 
  sigil_image007 
};


uint8_t waveformList[MAX_CYCLES] = {
  WAVEFORM_BANDLIMIT_SAWTOOTH,
  WAVEFORM_BANDLIMIT_SAWTOOTH,
  WAVEFORM_BANDLIMIT_SAWTOOTH,
  WAVEFORM_BANDLIMIT_SAWTOOTH,
  WAVEFORM_BANDLIMIT_SAWTOOTH,
  WAVEFORM_BANDLIMIT_SAWTOOTH,
  WAVEFORM_BANDLIMIT_SAWTOOTH,
  WAVEFORM_BANDLIMIT_SAWTOOTH
};

uint8_t defaultWaveformList[MAX_CYCLES] = {
  WAVEFORM_BANDLIMIT_SAWTOOTH,
  WAVEFORM_BANDLIMIT_SAWTOOTH,
  WAVEFORM_BANDLIMIT_SAWTOOTH,
  WAVEFORM_BANDLIMIT_SAWTOOTH,
  WAVEFORM_BANDLIMIT_SAWTOOTH,
  WAVEFORM_BANDLIMIT_SAWTOOTH,
  WAVEFORM_BANDLIMIT_SAWTOOTH,
  WAVEFORM_BANDLIMIT_SAWTOOTH
};



uint64_t scaleImage = 0; 

int midiNotes[MAX_CYCLES] = {
  TRIG1_MIDI,
  TRIG2_MIDI,
  TRIG3_MIDI,
  TRIG4_MIDI,
  TRIG5_MIDI,
  TRIG6_MIDI,
  TRIG7_MIDI,
  TRIG8_MIDI
};

int8_t midiFlags[MAX_CYCLES] = {-1, -1, -1, -1, -1, -1, -1, -1};


Bounce setButton(SET_BUTTON, 50);
uint8_t setIndex = 0;
Bounce divisorButton(DIVISOR_BUTTON, 50);
uint8_t divisorIndex = 0;


bool trigGateState = true;

//using Transport = MIDI_NAMESPACE::SerialMIDI<HardwareSerial>;
//HardwareSerial mySerial = HardwareSerial(Serial2);
//Transport serialMIDI(mySerial);
//MyMIDI TestMIDI((Transport&)serialMIDI);

// Taken from here: <https://stackoverflow.com/questions/4548004/how-to-correctly-and-standardly-compare-floats>
bool areEqualRel(float a, float b, float epsilon)  {
  return (fabs(a - b) <= epsilon * max(fabs(a), fabs(b)));
}

void initializeChaosPinsAvgs() {
  XENOKINETICS_DEBUG_PRINTLN("Initializing moving average for chaos pins....");
  chaosPinXAvg.begin(SMOOTHED_AVERAGE, 10); 
  chaosPinYAvg.begin(SMOOTHED_AVERAGE, 10);
  chaosPinZAvg.begin(SMOOTHED_AVERAGE, 10);

  chaosPinsAvgs[0] = &chaosPinXAvg;
  chaosPinsAvgs[1] = &chaosPinYAvg;
  chaosPinsAvgs[2] = &chaosPinZAvg;

}

void initializeAllDivisors() {
  for (int i = 0; i < MAX_SETS; i++) {
    for (int j = 0; j < MAX_CYCLES; j++) {
      //allDivisors[i][j] = 1.0;
      allDivisors[i][j] = divisorsSet[settings.divisorsIndexDefault[i][j]];
    }
  }
}

void initADC() {
  XENOKINETICS_DEBUG_PRINTLN("Initializing ADC....");
  adc->adc0->setAveraging(ADC_AVGS); // set number of averages
  adc->adc0->setResolution(ADC_RESOLUTION); // set bits of resolution

  // it can be any of the ADC_CONVERSION_SPEED enum: VERY_LOW_SPEED, LOW_SPEED, MED_SPEED, HIGH_SPEED_16BITS, HIGH_SPEED or VERY_HIGH_SPEED
  // see the documentation for more information
  // additionally the conversion speed can also be ADACK_2_4, ADACK_4_0, ADACK_5_2 and ADACK_6_2,
  // where the numbers are the frequency of the ADC clock in MHz and are independent on the bus speed.
  adc->adc0->setConversionSpeed(ADC_CONVERSION_SPEED::VERY_LOW_SPEED); // change the conversion speed
  // it can be any of the ADC_MED_SPEED enum: VERY_LOW_SPEED, LOW_SPEED, MED_SPEED, HIGH_SPEED or VERY_HIGH_SPEED
  adc->adc0->setSamplingSpeed(ADC_SAMPLING_SPEED::MED_SPEED); // change the sampling speed


  adc->adc1->setAveraging(ADC_AVGS); // set number of averages
  adc->adc1->setResolution(ADC_RESOLUTION); // set bits of resolution

  // it can be any of the ADC_CONVERSION_SPEED enum: VERY_LOW_SPEED, LOW_SPEED, MED_SPEED, HIGH_SPEED_16BITS, HIGH_SPEED or VERY_HIGH_SPEED
  // see the documentation for more information
  // additionally the conversion speed can also be ADACK_2_4, ADACK_4_0, ADACK_5_2 and ADACK_6_2,
  // where the numbers are the frequency of the ADC clock in MHz and are independent on the bus speed.
  adc->adc1->setConversionSpeed(ADC_CONVERSION_SPEED::VERY_LOW_SPEED); // change the conversion speed
  // it can be any of the ADC_MED_SPEED enum: VERY_LOW_SPEED, LOW_SPEED, MED_SPEED, HIGH_SPEED or VERY_HIGH_SPEED
  adc->adc1->setSamplingSpeed(ADC_SAMPLING_SPEED::MED_SPEED); // change the sampling speed

  adc0MaxValue = adc->adc0->getMaxValue();
  adc1MaxValue = adc->adc1->getMaxValue();

  XENOKINETICS_DEBUG_PRINTLN("Done initializing ADC.");
}

void initADCInputs() {
  XENOKINETICS_DEBUG_PRINTLN("Setting ADC input pins....")
  for (int i = 0; i < MAX_CYCLES; i++) {
    pinMode(chaosIntensityPotsMapping[i], INPUT);
  }

  for (int i = 0; i < MAX_CHAOS_PINS; i++) {
    pinMode(chaosPinsMapping[i], INPUT);
  }

  pinMode(CHAOS_CV_PIN, INPUT);
  pinMode(SET_CV_PIN, INPUT);
  XENOKINETICS_DEBUG_PRINTLN("Done setting ADC input pins.")
}

Cycle::Cycle() {
  cycle.period = 0;
  cycle.periodCorrected = 0;
  cycle.scale = 0.0f;
  cycle.offset = 0;
  //cycle.comment = "";
  envelope.attack = 60;
  envelope.decay = 30;
  envelope.sustain = 0.5;
  envelope.release = 1000;
  envelope.duration = 1000;
  waveform = WAVEFORM_BANDLIMIT_SAWTOOTH;

  for (int i = 0; i < MAX_FREQS; i++) {
    frequencies[i] = defaultFrequencies[i];
  }

  frequencyIndex = 0;
}

void Cycle::setPeriod(uint64_t period) {
  cycle.period = period;
}

void Cycle::setScale(float scale) {
  cycle.scale = scale;
}
void Cycle::setPeriodCorrected(uint64_t periodCorrected) {
  cycle.periodCorrected = periodCorrected;
}

void Cycle::setOffset(uint32_t offset) {
  cycle.offset = offset;
}

void Cycle::setComment(char comment[MAX_COMMENT_LEN]) {
  for (int i = 0; i < MAX_COMMENT_LEN; i++) {
    cycle.comment[i] = comment[i];
    commentPublic[i] = comment[i];
  }
}

void Cycle::setWaveform(int waveform) {
  waveform = waveform;
}

void Cycle::setEnvelope(Env envelope) {
  envelope = envelope;
}

void Cycle::setCycle(CycleInfo cycleInfo) {
  cycle = cycleInfo;
}

uint64_t Cycle::getPeriod() {
  return cycle.period;
}
float Cycle::getScale() {
  return cycle.scale;
}

uint64_t Cycle::getPeriodCorrected() {
  return cycle.periodCorrected;
}

uint32_t Cycle::getOffset() {
  return cycle.offset;
}

void Cycle::getComment(char* comment) {
  strncpy(comment, cycle.comment, MAX_COMMENT_LEN);
}

CycleInfo Cycle::getCycle() {
  return cycle;
}
int Cycle::getWaveform() {
  return waveform;
}

Env Cycle::getEnvelope() {
  return envelope;
}

void initDivisorsSettings() {

  for (int i = 0; i < MAX_SETS; i++) {
    for (int j = 0; j < MAX_CYCLES + 1; j++) {
      settings.divisors[i][j] = defaultDivisorsSet[j];
    }
  }
}

void initCycleSet() {
  // Set our initial cycle set to set 0
  char commentVal[MAX_COMMENT_LEN];
  for (int i = 0; i < MAX_CYCLES; i++) {
    cycles[i].period = cyclesSets[0].cyclesSet[i].period;
    cycles[i].periodCorrected = cyclesSets[0].cyclesSet[i].periodCorrected;
    cycles[i].offset = cyclesSets[0].cyclesSet[i].offset;
    cycles[i].scale = cyclesSets[0].cyclesSet[i].scale;
    strncpy(cycles[i].comment, cyclesSets[0].cyclesSet[i].comment, MAX_COMMENT_LEN);

    currentCyclesListClass[i].setPeriod(cyclesSetsClass[0].cyclesSet[i].getPeriod());
    currentCyclesListClass[i].setPeriodCorrected(cyclesSetsClass[0].cyclesSet[i].getPeriodCorrected());
    currentCyclesListClass[i].setOffset(cyclesSetsClass[0].cyclesSet[i].getOffset());
    currentCyclesListClass[i].setScale(cyclesSetsClass[0].cyclesSet[i].getScale());
    cyclesSetsClass[0].cyclesSet[i].getComment(commentVal);
    currentCyclesListClass[i].setComment(commentVal);
    currentCyclesListClass[i].setEnvelope(cyclesSetsClass[0].cyclesSet[i].getEnvelope());
    currentCyclesListClass[i].setWaveform(cyclesSetsClass[0].cyclesSet[i].getWaveform());
  }

}