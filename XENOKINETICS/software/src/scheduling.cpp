#include "main.h"

Scheduler runner; 

/**
 * Modify the release of the given envelop using our chaos values
*/
void modifyRelease(uint8_t index) {
  //envelopes[index]->release(int(chaosPinsAvgs[1]->get() * 6));
}

// When we have the task callback, turn on the voice and init
// the counter
// If we're getting called again, it means the previous note
// hasn't finished and we need to restart the envelope, but
// also add the standard duration to the existing length
void turnVoiceOn(int voice) {
  modifyRelease(voice);
  if (noteDurationsCounter[voice] <= 0) {
    envelopes[voice]->noteOn();
    noteDurationsCounter[voice] = noteDurations[voice];

    if (trigGateState) {
      triggerDurations[voice] = DEFAULT_TRIGGER_LENGTH;
    } else {
      triggerDurations[voice] = DEFAULT_GATE_LENGTH;
    }
  } else {
    envelopes[voice]->noteOn();
    noteDurationsCounter[voice] += noteDurations[voice];
  }

  //MyMIDI.sendNoteOn(midiNotes[voice], DEFAULT_MIDI_VEL, DEFAULT_MIDI_CHAN); 
  midiFlags[voice] = 1;
  digitalWrite(triggerPins[voice], LOW);
}

/**
 * Check the given voice and determine whether we should 
 * turn the note off
*/
void checkVoice(int voice) {
  // Decrement our counter (which is in milliseconds)
  // If we've reached zero we should turn the note off
  noteDurationsCounter[voice] -= 5;
  if (noteDurationsCounter[voice] <= 0) {
    envelopes[voice]->noteOff();
  }
}

/**
 * Check the given voice and determine whether we should 
 * turn the trigger off
*/
void checkTrigger(int voice) {
  triggerDurations[voice] -= 5;
  if (triggerDurations[voice] <= 0) {
    //MyMIDI.sendNoteOff(midiNotes[voice], 0, DEFAULT_MIDI_CHAN); 
    digitalWrite(triggerPins[voice], HIGH);
    triggerDurations[voice] = 0;
    midiFlags[voice] = 0;
  }

}

// Here follows the various callbacks for each task
void t1Callback() {
  //envelopes[0]->noteOn();
  //delay(100);
  //envelopes[0]->noteOff();

  u_int32_t randomVal = random(0, 100);
  uint32_t potVal = (uint32_t)(floor((chaosIntensityPotsValues[0] + 1.0)*100)/2.0);
  if (settings.knobsRandom && (potVal >= randomVal)) {
    turnVoiceOn(0);
  } else if (!settings.knobsRandom) {
    turnVoiceOn(0);
  }
  //display.print("&");
  XENOKINETICS_DEBUG_PRINT("Calling: ");
  XENOKINETICS_DEBUG_PRINTLN(currentCyclesListClass[0].commentPublic);
}

void t2Callback() {
  //envelopes[1]->noteOn();
  //delay(100);
  //envelopes[1]->noteOff();

  u_int32_t randomVal = random(0, 100);
  uint32_t potVal = (uint32_t)(floor((chaosIntensityPotsValues[1] + 1.0)*100)/2.0);
  if (settings.knobsRandom && (potVal >= randomVal)) {
    turnVoiceOn(1);
  } else if (!settings.knobsRandom) {
    turnVoiceOn(1);
  }
  //display.print("V");
  XENOKINETICS_DEBUG_PRINT("Calling: ");
  XENOKINETICS_DEBUG_PRINTLN(currentCyclesListClass[1].commentPublic);
}

void t3Callback() {
  //envelopes[2]->noteOn();
  //delay(4);
  //envelopes[2]->noteOff();
  u_int32_t randomVal = random(0, 100);
  uint32_t potVal = (uint32_t)(floor((chaosIntensityPotsValues[2] + 1.0)*100)/2.0);
  if (settings.knobsRandom && (potVal >= randomVal)) {

    turnVoiceOn(2);
  } else if (!settings.knobsRandom) {
    turnVoiceOn(2);
  }

  XENOKINETICS_DEBUG_PRINT("Calling: ");
  XENOKINETICS_DEBUG_PRINTLN(currentCyclesListClass[2].commentPublic);
}

void t4Callback() {
  //envelopes[3].noteOn();
  //envelopes[3]->noteOn();
  //delay(100);
  //envelopes[3].noteOff();
  //envelopes[3]->noteOff();
  u_int32_t randomVal = random(0, 100);
  uint32_t potVal = (uint32_t)(floor((chaosIntensityPotsValues[3] + 1.0)*100)/2.0);
  if (settings.knobsRandom && (potVal >= randomVal)) {

    modifyRelease(3);
    turnVoiceOn(3);
  } else if (!settings.knobsRandom) {
    modifyRelease(3);
    turnVoiceOn(3);
  }
  //P.print("S");
  XENOKINETICS_DEBUG_PRINT("Calling: ");
  XENOKINETICS_DEBUG_PRINTLN(currentCyclesListClass[3].commentPublic);
}

void t5Callback() {
  //envelopes[4]->noteOn();
  //delay(4);
  //envelopes[4]->noteOff();
  u_int32_t randomVal = random(0, 100);
  uint32_t potVal = (uint32_t)(floor((chaosIntensityPotsValues[4] + 1.0)*100)/2.0);
  if (settings.knobsRandom && (potVal >= randomVal)) {

    turnVoiceOn(4);
  } else if (!settings.knobsRandom) {
    turnVoiceOn(4);
  }
  XENOKINETICS_DEBUG_PRINT("Calling: ");
  XENOKINETICS_DEBUG_PRINTLN(currentCyclesListClass[4].commentPublic);
}

void t6Callback() {
  //envelopes[5]->noteOn();
  //delay(4);
  //envelopes[5]->noteOff();
  u_int32_t randomVal = random(0, 100);
  uint32_t potVal = (uint32_t)(floor((chaosIntensityPotsValues[5] + 1.0)*100)/2.0);
  if (settings.knobsRandom && (potVal >= randomVal)) {

    turnVoiceOn(5);
  } else if (!settings.knobsRandom) {
    turnVoiceOn(5);
  }
  XENOKINETICS_DEBUG_PRINT("Calling: ");
  XENOKINETICS_DEBUG_PRINTLN(currentCyclesListClass[5].commentPublic);
}

void t7Callback() {
  //envelopes[6]->noteOn();
  //delay(4);
  //envelopes[6]->noteOff();
  u_int32_t randomVal = random(0, 100);
  uint32_t potVal = (uint32_t)(floor((chaosIntensityPotsValues[6] + 1.0)*100)/2.0);
  if (settings.knobsRandom && (potVal >= randomVal)) {

    turnVoiceOn(6);
  } else if (!settings.knobsRandom) {
    turnVoiceOn(6);
  }
  XENOKINETICS_DEBUG_PRINT("Calling: ");
  XENOKINETICS_DEBUG_PRINTLN(currentCyclesListClass[6].commentPublic);
}

void t8Callback() {
  //envelopes[7]->noteOn();
  //delay(4);
  //envelopes[7]->noteOff();
  u_int32_t randomVal = random(0, 100);
  uint32_t potVal = (uint32_t)(floor((chaosIntensityPotsValues[7] + 1.0)*100)/2.0);
  if (settings.knobsRandom && (potVal >= randomVal)) {

    turnVoiceOn(7);
  } else if (!settings.knobsRandom) {
    turnVoiceOn(7);
  }
  XENOKINETICS_DEBUG_PRINT("Calling: ");
  XENOKINETICS_DEBUG_PRINTLN(currentCyclesListClass[7].commentPublic);
}

/**
 * Update the period for the given cycle scaled by the given scale
 * This is used by the interface code whenever we adjust the 
 * scale using the encoder knob and switch.
*/
void updatePeriod(int currentCycle, float scale) {
  uint64_t period = 0;
  float oldScale = 0;
  float newScale = 0;
  uint32_t offset = 0;

  period = cycles[currentCycle].period;
  offset = cycles[currentCycle].offset;
  oldScale = cycles[currentCycle].scale;
  //newScale = 0.01 * scale * oldScale;
  newScale = oldScale / scale;

  /* CLASS DEFINITION */
  period = currentCyclesListClass[currentCycle].getPeriod();
  offset = currentCyclesListClass[currentCycle].getOffset();
  oldScale = currentCyclesListClass[currentCycle].getScale();
  //newScale = 0.01 * scale * oldScale;
  newScale = oldScale / scale;


  cycles[currentCycle].periodCorrected = (uint64_t)((float)(period - offset) * newScale);
  tasks[currentCycle]->setInterval(cycles[currentCycle].periodCorrected);

  /* CLASS DEFINTIION */
  currentCyclesListClass[currentCycle].setPeriodCorrected((uint64_t)((float)(period - offset) * newScale));
  tasks[currentCycle]->setInterval(currentCyclesListClass[currentCycle].getPeriodCorrected());

}

/**
 * Update the interface using the current value of the chaos generator
 * and the proportion given by the pot.
*/
void updateIntervalChaos(int index) {
  if (settings.knobsRandom) {
    return;
  }
  XENOKINETICS_DEBUG_PRINTLN("RUNNING");
  float chaosValue = abs(chaosPinsAvgs[1]->get() - chaosPinsAvgs[0]->get());
  float amount = chaosIntensityPotsValues[index];
  float period = cycles[index].periodCorrected;
  float offset = cycles[index].offset;

  /* CLASS DEFINITION */
  period = currentCyclesListClass[index].getPeriodCorrected();
  offset = currentCyclesListClass[index].getOffset();


  // TODO need to figure out the right scaling here
  // not too much and not too little :)
  // do we then need to update the cycle with the new period??
  uint64_t update = uint64_t(period - (0.5f * period * ((chaosValue/MAX_CHAOS_VALUE) * amount) ));
  tasks[index]->setInterval(update);
  //cycles[index].periodCorrected = (uint64_t)((float)(update - offset));
  //tasks[index]->setInterval(cycles[index].periodCorrected);

}

/**
 * Update all of the cycles, usually called after reading a new set
 */
void updateAllCycles() {
  for (int i = 0; i < MAX_CYCLES; i++) {
    tasks[i]->setInterval(cycles[i].periodCorrected);
    tasks[i]->enable();
    // Adjust everything so that we don't have all of the tasks firing
    // immediately on starting the main loop
    tasks[i]->adjust(cycles[i].periodCorrected - 1);

    /* CLASS DEFINITION */
    tasks[i]->setInterval(currentCyclesListClass[i].getPeriodCorrected());
    tasks[i]->enable();
    // Adjust everything so that we don't have all of the tasks firing
    // immediately on starting the main loop
    tasks[i]->adjust(currentCyclesListClass[i].getPeriodCorrected() - 1);


    if (XENOKINETICS_MEGA_DEBUG) {
      XENOKINETICS_DEBUG_PRINT("Task interval: ");
      XENOKINETICS_DEBUG_PRINTLN(tasks[i]->getInterval());
    }

  }
}

Task t1(1000, TASK_FOREVER, &t1Callback);
Task t2(1000, TASK_FOREVER, &t2Callback);
Task t3(1000, TASK_FOREVER, &t3Callback);
Task t4(1000, TASK_FOREVER, &t4Callback);
Task t5(1000, TASK_FOREVER, &t5Callback);
Task t6(1000, TASK_FOREVER, &t6Callback);
Task t7(1000, TASK_FOREVER, &t7Callback);
Task t8(1000, TASK_FOREVER, &t8Callback);

Task *tasks[MAX_CYCLES] = {&t1, &t2, &t3, &t4, &t5, &t6, &t7, &t8};