#include "main.h"

/* this is for pt8211 */
// GUItool: begin automatically generated code
AudioSynthWaveformModulated waveformMod7;   //xy=181.1999969482422,422.20001220703125
AudioSynthWaveformModulated waveformMod6;   //xy=183.1999969482422,370.1999969482422
AudioSynthWaveformModulated waveformMod5;   //xy=187.1999969482422,316.1999969482422
AudioSynthWaveformModulated waveformMod8;   //xy=197.1999969482422,475.1999969482422
AudioSynthWaveformModulated waveformMod3;   //xy=204.1999969482422,200.1999969482422
AudioSynthWaveformModulated waveformMod4;   //xy=204.1999969482422,258.1999969482422
AudioSynthWaveformModulated waveformMod1;   //xy=215.1999969482422,89.19999694824219
AudioSynthWaveformModulated waveformMod2;   //xy=215.1999969482422,148.1999969482422
AudioSynthWaveformModulated waveformMod9;   //xy=247.1999969482422,838.1999969482422
AudioEffectEnvelope      envelope6;      //xy=360.1999969482422,373.1999969482422
AudioEffectEnvelope      envelope8;      //xy=367.1999969482422,488.1999969482422
AudioEffectEnvelope      envelope5;      //xy=381.1999969482422,324.1999969482422
AudioEffectEnvelope      envelope2;      //xy=383.1999969482422,140.1999969482422
AudioEffectEnvelope      envelope7;      //xy=394.1999969482422,429.1999969482422
AudioEffectEnvelope      envelope1;      //xy=403.1999969482422,89.19999694824219
AudioEffectEnvelope      envelope3;      //xy=403.1999969482422,189.1999969482422
AudioEffectEnvelope      envelope4;      //xy=421.1999969482422,251.1999969482422
AudioMixer4              mixer1;         //xy=634.2000122070312,219.1999969482422
AudioMixer4              mixer2;         //xy=645.2000122070312,354.20001220703125
AudioMixer4              mixer3;         //xy=819.2000122070312,267.20001220703125
AudioOutputPT8211        pt8211_1;       //xy=969.2000122070312,265.20001220703125
AudioConnection          patchCord1(waveformMod7, envelope7);
AudioConnection          patchCord2(waveformMod6, envelope6);
AudioConnection          patchCord3(waveformMod5, envelope5);
AudioConnection          patchCord4(waveformMod8, envelope8);
AudioConnection          patchCord5(waveformMod3, envelope3);
AudioConnection          patchCord6(waveformMod4, envelope4);
AudioConnection          patchCord7(waveformMod1, envelope1);
AudioConnection          patchCord8(waveformMod2, envelope2);
AudioConnection          patchCord9(envelope6, 0, mixer2, 1);
AudioConnection          patchCord10(envelope8, 0, mixer2, 3);
AudioConnection          patchCord11(envelope5, 0, mixer2, 0);
AudioConnection          patchCord12(envelope2, 0, mixer1, 1);
AudioConnection          patchCord13(envelope7, 0, mixer2, 2);
AudioConnection          patchCord14(envelope1, 0, mixer1, 0);
AudioConnection          patchCord15(envelope3, 0, mixer1, 2);
AudioConnection          patchCord16(envelope4, 0, mixer1, 3);
AudioConnection          patchCord17(mixer1, 0, mixer3, 0);
AudioConnection          patchCord18(mixer2, 0, mixer3, 1);
AudioConnection          patchCord19(mixer3, 0, pt8211_1, 0);
AudioConnection          patchCord20(mixer3, 0, pt8211_1, 1);
// GUItool: end automatically generated code

AudioSynthWaveformModulated *waveforms[MAX_CYCLES] = {
    &waveformMod1,
    &waveformMod2,
    &waveformMod3,
    &waveformMod4,
    &waveformMod5,
    &waveformMod6,
    &waveformMod7,
    &waveformMod8
};

AudioEffectEnvelope *envelopes[MAX_CYCLES] = {
    &envelope1,
    &envelope2,
    &envelope3,
    &envelope4,
    &envelope5,
    &envelope6,
    &envelope7,
    &envelope8
};

/* this is for using MQS 
// GUItool: begin automatically generated code
AudioSynthWaveformModulated waveformMod2;   //xy=212.1999969482422,253.20001220703125
AudioSynthWaveformModulated waveformMod1;   //xy=213.1999969482422,211.1999969482422
AudioSynthWaveformModulated waveformMod3;   //xy=213.1999969482422,295.1999969482422
AudioSynthWaveformModulated waveformMod4;   //xy=214.1999969482422,338.20001220703125
AudioMixer4              mixer1;         //xy=445.1999969482422,271.1999969482422
AudioEffectEnvelope      envelope1;      //xy=643.2000122070312,219.1999969482422
AudioOutputMQS           mqs1;           //xy=820.2000122070312,224.1999969482422
AudioConnection          patchCord1(waveformMod2, 0, mixer1, 1);
AudioConnection          patchCord2(waveformMod1, 0, mixer1, 0);
AudioConnection          patchCord3(waveformMod3, 0, mixer1, 2);
AudioConnection          patchCord4(waveformMod4, 0, mixer1, 3);
AudioConnection          patchCord5(mixer1, envelope1);
AudioConnection          patchCord6(envelope1, 0, mqs1, 0);
AudioConnection          patchCord7(envelope1, 0, mqs1, 1);
// GUItool: end automatically generated code
*/


/* this is for using the audio shield
// GUItool: begin automatically generated code
AudioSynthWaveformModulated waveformMod4;   //xy=203.1999969482422,343.1999969482422
AudioSynthWaveformModulated waveformMod3;   //xy=213.1999969482422,295.1999969482422
AudioSynthWaveformModulated waveformMod1;   //xy=219.1999969482422,208.1999969482422
AudioSynthWaveformModulated waveformMod2;   //xy=220.1999969482422,251.1999969482422
AudioMixer4              mixer1;         //xy=445.1999969482422,271.1999969482422
AudioEffectEnvelope      envelope1;      //xy=643.2000122070312,219.1999969482422
AudioOutputI2S           i2s1;           //xy=804.1999969482422,216.1999969482422
AudioControlSGTL5000     sgtl5000_1;     //xy=401.1999969482422,103.19999694824219
AudioConnection          patchCord1(waveformMod4, 0, mixer1, 3);
AudioConnection          patchCord2(waveformMod3, 0, mixer1, 2);
AudioConnection          patchCord3(waveformMod1, 0, mixer1, 0);
AudioConnection          patchCord4(waveformMod2, 0, mixer1, 1);
AudioConnection          patchCord5(mixer1, envelope1);
AudioConnection          patchCord6(envelope1, 0, i2s1, 0);
AudioConnection          patchCord7(envelope1, 0, i2s1, 1);
// GUItool: end automatically generated code
*/