#include "main.h"

CVTransform chaosIntensitiesTransforms[MAX_CYCLES] = {
    // Pot 1,
    {true, false, 0.008f},
    // Pot 2,
    {true, false, 0.008f},
    // Pot 3,
    {true, false, 0.008f},
    // Pot 4,
    {true, false, 0.008f},
    // Pot 5,
    {true, false, 0.008f},
    // Pot 6,
    {true, false, 0.008f},
    // Pot 7,
    {true, false, 0.008f},
    // Pot 8,
    {true, false, 0.008f},
};

CVTransform chaosPinsTransforms[MAX_CHAOS_PINS] = {
    // CX,
    {false, false, 0.008f},
    // CY,
    {false, false, 0.008f},
    // CZ,
    {false, false, 0.008f}
};

CVTransform chaosCVTransform = {true, false, 0.008f};
CVTransform setCVTransform = {true, false, 0.008f};


/*
float smoothed_cv_adc_values[NUM_PARAMS] = {
    // WAVE001_VOCT_CV_ADC,
    0.0f,
    // WAVE001_WF_CV_ADC,
    0.0f,
    // WAVE001_MOD_CV_ADC,
    0.0f,
    // WAVE002_VOCT_CV_ADC,
    0.0f,
    // WAVE002_WF_CV_ADC,
    0.0f,
    // WAVE002_MOD_CV_ADC,
    0.0f,
    // WAVE003_VOCT_CV_ADC,
    0.0f,
    // WAVE003_WF_CV_ADC,
    0.0f,
    // WAVE003_MOD_CV_ADC,
    0.0f,
    // WAVE004_VOCT_CV_ADC,
    0.0f,
    // WAVE004_WF_CV_ADC,
    0.0f,
    // WAVE004_MOD_CV_ADC,
    0.0f
};

float smoothed_pot_values[NUM_PARAMS] = {
    // WAVE001_VOCT_POT,
    0.0f,
    // WAVE001_WF_POT,
    0.0f,
    // WAVE001_MOD_POT,
    0.0f,
    // WAVE002_VOCT_POT,
    0.0f,
    // WAVE002_WF_POT,
    0.0f,
    // WAVE002_MOD_POT,
    0.0f,
    // WAVE003_VOCT_POT,
    0.0f,
    // WAVE003_WF_POT,
    0.0f,
    // WAVE003_MOD_POT,
    0.0f,
    // WAVE004_VOCT_POT,
    0.0f,
    // WAVE004_WF_POT,
    0.0f,
    // WAVE004_MOD_POT,
    0.0f
};

*/