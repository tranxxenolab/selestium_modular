#include "main.h"  

/**
 * Read through the settings file. Ensure that it is in the proper format!
 */  
void readSettings(String filename) {
  File settingsFile = SD.open(filename.c_str(), FILE_READ);
  String currentLine;

  if (settingsFile) {
    char* pEnd;
    int tempValue = 0;
    float tempValueFloat = 0.0f;
    while (settingsFile.available()) {
      currentLine = settingsFile.readStringUntil('=');
      if (currentLine.equalsIgnoreCase("gate_length")) {
        // Setting gate_length
        settings.gateLength = (uint16_t)(settingsFile.readStringUntil('\n').toInt());
      } else if (currentLine.equalsIgnoreCase("waveforms_default")) {
        // Setting waveforms_default 
        for (int i = 0; i < (MAX_CYCLES - 1); i++) {
          tempValue = settingsFile.readStringUntil(',').toInt();
          XENOKINETICS_DEBUG_PRINTLN(tempValue);
          settings.waveformsDefault[i] = (bool)tempValue;
        }

        tempValue = settingsFile.readStringUntil('\n').toInt();
        settings.waveformsDefault[MAX_CYCLES - 1] = (bool)tempValue;
      } else if (currentLine.equalsIgnoreCase("gates_default")) {
        // Setting gates_default
        for (int i = 0; i < (MAX_CYCLES - 1); i++) {
          tempValue = settingsFile.readStringUntil(',').toInt();
          settings.gatesDefault[i] = (bool)tempValue;
        }

        tempValue = settingsFile.readStringUntil('\n').toInt();
        settings.gatesDefault[MAX_CYCLES - 1] = (bool)tempValue;
      // DIVISORS
      } else if (currentLine.equalsIgnoreCase("divisors_set1")) {
        // Setting divisors
        for (int i = 0; i < (MAX_CYCLES); i++) {
          tempValueFloat = settingsFile.readStringUntil(',').toFloat();
          settings.divisors[0][i] = tempValueFloat;
        }

        tempValueFloat = settingsFile.readStringUntil('\n').toFloat();
        settings.divisors[0][MAX_CYCLES] = tempValueFloat;
      } else if (currentLine.equalsIgnoreCase("divisors_set2")) {
        // Setting divisors
        for (int i = 0; i < (MAX_CYCLES); i++) {
          tempValueFloat = settingsFile.readStringUntil(',').toFloat();
          settings.divisors[1][i] = tempValueFloat;
        }

        tempValueFloat = settingsFile.readStringUntil('\n').toFloat();
        settings.divisors[1][MAX_CYCLES] = tempValueFloat;
      } else if (currentLine.equalsIgnoreCase("divisors_set3")) {
        // Setting divisors
        for (int i = 0; i < (MAX_CYCLES); i++) {
          tempValueFloat = settingsFile.readStringUntil(',').toFloat();
          settings.divisors[2][i] = tempValueFloat;
        }

        tempValueFloat = settingsFile.readStringUntil('\n').toFloat();
        settings.divisors[2][MAX_CYCLES] = tempValueFloat;
      } else if (currentLine.equalsIgnoreCase("divisors_set4")) {
        // Setting divisors
        for (int i = 0; i < (MAX_CYCLES); i++) {
          tempValueFloat = settingsFile.readStringUntil(',').toFloat();
          settings.divisors[3][i] = tempValueFloat;
        }

        tempValueFloat = settingsFile.readStringUntil('\n').toFloat();
        settings.divisors[3][MAX_CYCLES] = tempValueFloat;
      } else if (currentLine.equalsIgnoreCase("divisors_set5")) {
        // Setting divisors
        for (int i = 0; i < (MAX_CYCLES); i++) {
          tempValueFloat = settingsFile.readStringUntil(',').toFloat();
          settings.divisors[4][i] = tempValueFloat;
        }

        tempValueFloat = settingsFile.readStringUntil('\n').toFloat();
        settings.divisors[4][MAX_CYCLES] = tempValueFloat;
      } else if (currentLine.equalsIgnoreCase("divisors_set6")) {
        // Setting divisors
        for (int i = 0; i < (MAX_CYCLES); i++) {
          tempValueFloat = settingsFile.readStringUntil(',').toFloat();
          settings.divisors[5][i] = tempValueFloat;
        }

        tempValueFloat = settingsFile.readStringUntil('\n').toFloat();
        settings.divisors[5][MAX_CYCLES] = tempValueFloat;
      } else if (currentLine.equalsIgnoreCase("divisors_set7")) {
        // Setting divisors
        for (int i = 0; i < (MAX_CYCLES); i++) {
          tempValueFloat = settingsFile.readStringUntil(',').toFloat();
          settings.divisors[6][i] = tempValueFloat;
        }

        tempValueFloat = settingsFile.readStringUntil('\n').toFloat();
        settings.divisors[6][MAX_CYCLES] = tempValueFloat;
      } else if (currentLine.equalsIgnoreCase("divisors_set8")) {
        // Setting divisors
        for (int i = 0; i < (MAX_CYCLES); i++) {
          tempValueFloat = settingsFile.readStringUntil(',').toFloat();
          settings.divisors[7][i] = tempValueFloat;
        }

        tempValueFloat = settingsFile.readStringUntil('\n').toFloat();
        settings.divisors[7][MAX_CYCLES] = tempValueFloat;

      // DIVISORS INDEX
      } else if (currentLine.equalsIgnoreCase("divisorsIndex_set1_default")) {
        // Setting divisorsIndex_default
        for (int i = 0; i < (MAX_CYCLES - 1); i++) {
          tempValue = settingsFile.readStringUntil(',').toInt();
          settings.divisorsIndexDefault[0][i] = tempValue;
        }

        tempValue = settingsFile.readStringUntil('\n').toInt();
        settings.divisorsIndexDefault[0][MAX_CYCLES - 1] = tempValue;
      } else if (currentLine.equalsIgnoreCase("divisorsIndex_set2_default")) {
        // Setting divisorsIndex_default
        for (int i = 0; i < (MAX_CYCLES - 1); i++) {
          tempValue = settingsFile.readStringUntil(',').toInt();
          settings.divisorsIndexDefault[1][i] = tempValue;
        }

        tempValue = settingsFile.readStringUntil('\n').toInt();
        settings.divisorsIndexDefault[1][MAX_CYCLES - 1] = tempValue;
      } else if (currentLine.equalsIgnoreCase("divisorsIndex_set3_default")) {
        // Setting divisorsIndex_default
        for (int i = 0; i < (MAX_CYCLES - 1); i++) {
          tempValue = settingsFile.readStringUntil(',').toInt();
          settings.divisorsIndexDefault[2][i] = tempValue;
        }

        tempValue = settingsFile.readStringUntil('\n').toInt();
        settings.divisorsIndexDefault[2][MAX_CYCLES - 1] = tempValue;
      } else if (currentLine.equalsIgnoreCase("divisorsIndex_set4_default")) {
        // Setting divisorsIndex_default
        for (int i = 0; i < (MAX_CYCLES - 1); i++) {
          tempValue = settingsFile.readStringUntil(',').toInt();
          settings.divisorsIndexDefault[3][i] = tempValue;
        }

        tempValue = settingsFile.readStringUntil('\n').toInt();
        settings.divisorsIndexDefault[3][MAX_CYCLES - 1] = tempValue;
      } else if (currentLine.equalsIgnoreCase("divisorsIndex_set5_default")) {
        // Setting divisorsIndex_default
        for (int i = 0; i < (MAX_CYCLES - 1); i++) {
          tempValue = settingsFile.readStringUntil(',').toInt();
          settings.divisorsIndexDefault[4][i] = tempValue;
        }

        tempValue = settingsFile.readStringUntil('\n').toInt();
        settings.divisorsIndexDefault[4][MAX_CYCLES - 1] = tempValue;
      } else if (currentLine.equalsIgnoreCase("divisorsIndex_set6_default")) {
        // Setting divisorsIndex_default
        for (int i = 0; i < (MAX_CYCLES - 1); i++) {
          tempValue = settingsFile.readStringUntil(',').toInt();
          settings.divisorsIndexDefault[5][i] = tempValue;
        }

        tempValue = settingsFile.readStringUntil('\n').toInt();
        settings.divisorsIndexDefault[5][MAX_CYCLES - 1] = tempValue;
      } else if (currentLine.equalsIgnoreCase("divisorsIndex_set7_default")) {
        // Setting divisorsIndex_default
        for (int i = 0; i < (MAX_CYCLES - 1); i++) {
          tempValue = settingsFile.readStringUntil(',').toInt();
          settings.divisorsIndexDefault[6][i] = tempValue;
        }

        tempValue = settingsFile.readStringUntil('\n').toInt();
        settings.divisorsIndexDefault[6][MAX_CYCLES - 1] = tempValue;
      } else if (currentLine.equalsIgnoreCase("divisorsIndex_set8_default")) {
        // Setting divisorsIndex_default
        for (int i = 0; i < (MAX_CYCLES - 1); i++) {
          tempValue = settingsFile.readStringUntil(',').toInt();
          settings.divisorsIndexDefault[7][i] = tempValue;
        }

        tempValue = settingsFile.readStringUntil('\n').toInt();
        settings.divisorsIndexDefault[7][MAX_CYCLES - 1] = tempValue;
      } else if (currentLine.equalsIgnoreCase("envelopes_default")) {
        // Setting envelopes_default 
        for (int i = 0; i < (MAX_CYCLES - 1); i++) {
          tempValue = settingsFile.readStringUntil(',').toInt();
          settings.envelopesDefault[i] = (bool)tempValue;
        }

        tempValue = settingsFile.readStringUntil('\n').toInt();
        settings.envelopesDefault[MAX_CYCLES - 1] = (bool)tempValue;
      } else if (currentLine.equalsIgnoreCase("frequencies_default")) {
        // Setting frequencies_default
        for (int i = 0; i < (MAX_CYCLES - 1); i++) {
          tempValue = settingsFile.readStringUntil(',').toInt();
          settings.frequenciesDefault[i] = (bool)tempValue;
        }

        tempValue = settingsFile.readStringUntil('\n').toInt();
        settings.frequenciesDefault[MAX_CYCLES - 1] = (bool)tempValue;
      } else if (currentLine.equalsIgnoreCase("voices_enabled")) {
        // Setting voices_enabled 
        for (int i = 0; i < (MAX_CYCLES - 1); i++) {
          tempValue = settingsFile.readStringUntil(',').toInt();
          settings.voicesEnabled[i] = (bool)tempValue;
        }

        tempValue = settingsFile.readStringUntil('\n').toInt();
        settings.voicesEnabled[MAX_CYCLES - 1] = (bool)tempValue;
      } else if (currentLine.equalsIgnoreCase("knobs_random")) { 
        // Setting knobs_random
        settings.knobsRandom = (bool)(settingsFile.readStringUntil('\n').toInt());
      } else if (currentLine.equalsIgnoreCase("set_cv_trigger")) { 
        // Setting CV trigger
        settings.setCVTrigger = (bool)(settingsFile.readStringUntil('\n').toInt());

      } else {
        // Unknown key
        settingsFile.readStringUntil('\n');
      }

    }
    settingsFile.close();
  } else {
    XENOKINETICS_DEBUG_ERROR_PRINT("ERROR: ");
    XENOKINETICS_DEBUG_ERROR_PRINTLN("Error reading settings file! Using default values.");
  }
}


/**
 * Read through the voice scales file. Ensure that it is in the proper format!
 */  
void readDivisors(String filename) {
  File divisorsFile = SD.open(filename.c_str(), FILE_READ);
  uint8_t currentDivisorsItem = 0;
  float currentDivisor = 0.0;
  String currentLine;

  if (divisorsFile) {
    char* pEnd;
    while (divisorsFile.available()) {
      currentLine = divisorsFile.readStringUntil('\n');
      currentDivisor = strtof(currentLine.c_str(), &pEnd);

      if (XENOKINETICS_MEGA_DEBUG) {
          XENOKINETICS_DEBUG_PRINT("Current divisor: ");
          XENOKINETICS_DEBUG_PRINTLN(currentDivisor);
      }

      divisorsSet[currentDivisorsItem] = currentDivisor;
      currentDivisorsItem += 1;

    }
    divisorsFile.close();
  } else {
    XENOKINETICS_DEBUG_ERROR_PRINT("ERROR: ");
    XENOKINETICS_DEBUG_ERROR_PRINTLN("Error reading voice scales file! Using default values.");
    for (int i = 0; i < (MAX_VOICES + 1); i++) {
      divisorsSet[i] = defaultDivisorsSet[i];
    }
  }
}

/**
 * Read through the sigils file. Ensure that it is in the proper format!
 */  
void readSigils(String filename) {
  File sigilsFile = SD.open(filename.c_str(), FILE_READ);
  uint8_t currentSigilItem= 0;
  unsigned long long currentSigil = 0.0;
  String currentLine;

  if (sigilsFile) {
    char* pEnd;
    while (sigilsFile.available()) {
      currentLine = sigilsFile.readStringUntil('\n');
      currentSigil = strtoull(currentLine.c_str(), &pEnd, 16);

      if (XENOKINETICS_MEGA_DEBUG) {
          XENOKINETICS_DEBUG_PRINT("Current sigil: ");
          //XENOKINETICS_DEBUG_PRINTLN(currentSigil);
          Serial.println(currentSigil, HEX);
      }

      sigilList[currentSigilItem] = (uint64_t) currentSigil;
      currentSigilItem += 1;

    }
    sigilsFile.close();

    if (currentSigilItem != MAX_CYCLES) {
      // TODO
      // I think this still needs to be tested...
      XENOKINETICS_DEBUG_ERROR_PRINT("ERROR: ");
      XENOKINETICS_DEBUG_ERROR_PRINTLN("Error in length of sigils file, taking from default set...");
      for (int i = currentSigilItem; i < MAX_CYCLES; i++) {
        sigilList[i] = defaultSigilList[i];
      }
    }
  } else {
    XENOKINETICS_DEBUG_ERROR_PRINT("ERROR: ");
    XENOKINETICS_DEBUG_ERROR_PRINTLN("Error reading sigils file or no file present! Using default values.");
    for (int i = 0; i < (MAX_VOICES + 1); i++) {
      sigilList[i] = defaultSigilList[i];
    }
  }
}

/**
 * Read through the waveforms file. Ensure that it is in the proper format!
 */  
void readWaveforms(String filename) {
  File waveformsFile = SD.open(filename.c_str(), FILE_READ);
  uint8_t currentWaveformItem= 0;
  uint8_t currentWaveform = 0;
  String currentLine;


  if (waveformsFile) {
    char* pEnd;
    while (waveformsFile.available()) {
      currentLine = waveformsFile.readStringUntil('\n');
      if (currentLine.substring(0, 3).equals("SIN")) {
        currentWaveform = WAVEFORM_SINE;
      } else if (currentLine.substring(0, 3).equals("SAW")) {
        currentWaveform = WAVEFORM_SAWTOOTH;
      } else if (currentLine.substring(0, 3).equals("TRI")) {
        currentWaveform = WAVEFORM_TRIANGLE;
      } else if (currentLine.substring(0, 3).equals("PUL")) {
        currentWaveform = WAVEFORM_PULSE;
      } else {
        currentWaveform = WAVEFORM_SAWTOOTH;
      }

      if (XENOKINETICS_MEGA_DEBUG) {
          XENOKINETICS_DEBUG_PRINT("Current waveform: ");
          //XENOKINETICS_DEBUG_PRINTLN(currentSigil);
          //Serial.println(currentWaveform);
          Serial.println(currentLine);
      }

      // TODO fix this
      waveformList[currentWaveformItem] = currentWaveform;
      currentWaveformItem += 1;

    }
    waveformsFile.close();

    if (currentWaveformItem != MAX_CYCLES) {
      // TODO
      // I think this still needs to be tested...
      XENOKINETICS_DEBUG_ERROR_PRINT("ERROR: ");
      XENOKINETICS_DEBUG_ERROR_PRINTLN("Error in length of waveforms file, taking from default set...");
      for (int i = currentWaveformItem; i < MAX_CYCLES; i++) {
        waveformList[i] = defaultWaveformList[i];
      }
    }
  } else {
    XENOKINETICS_DEBUG_ERROR_PRINT("ERROR: ");
    XENOKINETICS_DEBUG_ERROR_PRINTLN("Error reading waveforms file or no file present! Using default values.");
    for (int i = 0; i < MAX_CYCLES; i++) {
      waveformList[i] = defaultWaveformList[i];
    }
  }
}



/**
 * Read the frequencies file according to the given format.
 * Hopefully this is robust to some kinds of problems, but not all of them.
 * So be sure to have the properly formatted file!
*/
void readFrequencies(String filename) {
  File frequencesFile = SD.open(filename.c_str(), FILE_READ);
  uint8_t currentLineIndex = 0;
  String currentLine = "";
  String valueType = "ratio";
  float currentFrequency = 0.0;
  float currentValue = 0.0;
  float numerator = 0.0;
  float denominator = 1.0;
  float frequencyRatios[MAX_VOICES] = {1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0};


  if (frequencesFile) {
    char* pEnd;
    while (frequencesFile.available()) {
      if (currentLineIndex == 0) {
        currentLine = frequencesFile.readStringUntil('\n');
        currentFrequency = strtof(currentLine.c_str(), &pEnd);
        currentLineIndex += 1;
      } else if (currentLineIndex == 1) {
        currentLine = frequencesFile.readStringUntil('\n');

        if (currentLine.startsWith("decimal")) {
          valueType = "decimal";
        } else if (currentLine.startsWith("ratio")) {
          valueType = "ratio";
        } else {
          // TODO
          // is this a good default?
          valueType = "decimal";
        }

        currentLineIndex += 1;
      } else if (currentLineIndex > 1) {
        // TODO check on the decimal side of things
        if (valueType.equals("decimal")) {
          currentLine = frequencesFile.readStringUntil('\n');
          currentValue = strtof(currentLine.c_str(), &pEnd);
          frequencyRatios[currentLineIndex - 2] = currentValue;
        } else if (valueType.equals("ratio")) {
          currentLine = frequencesFile.readStringUntil('/');
          numerator = strtof(currentLine.c_str(), &pEnd);
          currentLine = frequencesFile.readStringUntil('\n');
          denominator = strtof(currentLine.c_str(), &pEnd);
          frequencyRatios[currentLineIndex - 2] = numerator/denominator;
        }

        // TODO
        // Actually implement the usage of these frequencies, like above
        noteRatios[currentLineIndex - 2] = frequencyRatios[currentLineIndex - 2];
        XENOKINETICS_DEBUG_PRINT("Current ratio: ");
        XENOKINETICS_DEBUG_PRINTLN(frequencyRatios[currentLineIndex - 2]);
        currentLineIndex += 1;
      }
    }
  } else {
    // TODO setup default values
    XENOKINETICS_DEBUG_ERROR_PRINT("ERROR: ");
    XENOKINETICS_DEBUG_ERROR_PRINTLN("Error reading frequences file! Using default values.");
    fundamentalFrequency = DEFAULT_FUNAMDENTAL_FREQUENCY;

    for (int i = 0; i < MAX_CYCLES; i++) {
      noteRatios[i] = defaultNoteRatios[i];
    }

  }

}

/**
 * Read a particular cycle set and save it to our global set of cycles
 * 
 * NOTE: There is no data checking on the cycleIndex, so don't
 * overflow, please!
*/
void readCycleSet(String filename, uint8_t cycleIndex) {
  String period;
  String scale;
  String offset;
  String comment;
  uint64_t periodVal = 0;
  float scaleVal = 0;
  uint32_t offsetVal = 0;
  
  File cycleSetFile = SD.open(filename.c_str(), FILE_READ);

  int currentCycle = 0;
  char commentVal[MAX_COMMENT_LEN];
  bool hasEnvelopesFile = !settings.envelopesDefault[cycleIndex];
  bool hasWaveformsFile = !settings.waveformsDefault[cycleIndex];
  bool hasGatesFile = !settings.gatesDefault[cycleIndex];
  bool hasFrequenciesFile = !settings.frequenciesDefault[cycleIndex];

  if (hasEnvelopesFile) {
    XENOKINETICS_DEBUG_PRINT("Has envelopes file for cycle: ")
    XENOKINETICS_DEBUG_PRINTLN(cycleIndex);
  }

  if (cycleSetFile) {
    while (cycleSetFile.available()) {
      period = cycleSetFile.readStringUntil(',');
      char* pEnd;
      periodVal = strtoll(period.c_str(), &pEnd, 0);

      scale = cycleSetFile.readStringUntil(',');
      scaleVal = scale.toFloat();

      offset = cycleSetFile.readStringUntil(',');
      offsetVal = strtol(offset.c_str(), &pEnd, 0);

      comment = cycleSetFile.readStringUntil('\n');
      strncpy(commentVal, comment.c_str(), MAX_COMMENT_LEN);

      cyclesSetsClass[cycleIndex].cyclesSet[currentCycle].setPeriod(periodVal);
      cyclesSetsClass[cycleIndex].cyclesSet[currentCycle].setScale(scaleVal);
      cyclesSetsClass[cycleIndex].cyclesSet[currentCycle].setPeriodCorrected((uint64_t)((float)(periodVal - offsetVal) * scaleVal));
      cyclesSetsClass[cycleIndex].cyclesSet[currentCycle].setOffset(offsetVal);
      cyclesSetsClass[cycleIndex].cyclesSet[currentCycle].setComment(commentVal);
      // TODO
      // Write the code that deals with these aspects in a better way
      // i.e., that is based on envelopes for each cycle, etc
      // and probably split this out into a separate method
      cyclesSetsClass[cycleIndex].cyclesSet[currentCycle].setWaveform(waveformList[currentCycle]);
      cyclesSetsClass[cycleIndex].cyclesSet[currentCycle].setEnvelope(envelopeValues[currentCycle]);

      cyclesSets[cycleIndex].cyclesSet[currentCycle].period = periodVal;
      cyclesSets[cycleIndex].cyclesSet[currentCycle].scale = scaleVal;
      // TODO not sure if this is a good idea or not :)
      cyclesSets[cycleIndex].cyclesSet[currentCycle].periodCorrected = (uint64_t)((float)(periodVal - offsetVal) * scaleVal);
      cyclesSets[cycleIndex].cyclesSet[currentCycle].offset = offsetVal;
      //cycleSet[currentCycle].comment = comment;
      strncpy(cyclesSets[cycleIndex].cyclesSet[currentCycle].comment, comment.c_str(), MAX_COMMENT_LEN);

      if (XENOKINETICS_MEGA_DEBUG) {
          /*
          XENOKINETICS_DEBUG_PRINT("Cycle: ");
          XENOKINETICS_DEBUG_PRINTLN(currentCycle);
          XENOKINETICS_DEBUG_PRINT("Period (before division): ");
          XENOKINETICS_DEBUG_PRINTLN(cyclesSets[cycleIndex].cyclesSet[currentCycle].period);
          XENOKINETICS_DEBUG_PRINT("Scale: ");
          XENOKINETICS_DEBUG_PRINTLN(cyclesSets[cycleIndex].cyclesSet[currentCycle].scale);
          XENOKINETICS_DEBUG_PRINT("Period (after scaling and offset): ");
          XENOKINETICS_DEBUG_PRINTLN(cyclesSets[cycleIndex].cyclesSet[currentCycle].periodCorrected);
          XENOKINETICS_DEBUG_PRINT("Comment: ");
          XENOKINETICS_DEBUG_PRINTLN(cyclesSets[cycleIndex].cyclesSet[currentCycle].comment);
          */

          XENOKINETICS_DEBUG_PRINTLN("");
          XENOKINETICS_DEBUG_PRINT("Cycle (USING CLASS): ");
          XENOKINETICS_DEBUG_PRINTLN(currentCycle);
          XENOKINETICS_DEBUG_PRINT("Period (before division): ");
          XENOKINETICS_DEBUG_PRINTLN(cyclesSetsClass[cycleIndex].cyclesSet[currentCycle].getPeriod());
          XENOKINETICS_DEBUG_PRINT("Scale: ");
          XENOKINETICS_DEBUG_PRINTLN(cyclesSetsClass[cycleIndex].cyclesSet[currentCycle].getScale());
          XENOKINETICS_DEBUG_PRINT("Period (after scaling and offset): ");
          XENOKINETICS_DEBUG_PRINTLN(cyclesSetsClass[cycleIndex].cyclesSet[currentCycle].getPeriodCorrected());
          XENOKINETICS_DEBUG_PRINT("Comment: ");
          char commentClass[MAX_COMMENT_LEN];
          cyclesSetsClass[cycleIndex].cyclesSet[currentCycle].getComment(commentClass);
          XENOKINETICS_DEBUG_PRINTLN(commentClass);

      }
      currentCycle += 1;
    }

    cycleSetFile.close();

    //cyclesSets[1]->cyclesSet = testSet;
    //testCyclesSet.cycle001.period; 
    //testCyclesSet.cyclesSet[0].period;
  } else {
    // TODO setup default values here, stored in config.h and used if for some reason
    // we can't read the SD card
    XENOKINETICS_DEBUG_ERROR_PRINT("ERROR: ");
    XENOKINETICS_DEBUG_ERROR_PRINT("Error reading ");
    XENOKINETICS_DEBUG_ERROR_PRINT(filename);
    XENOKINETICS_DEBUG_ERROR_PRINTLN("!");
  }


}

/**
 * Read through all of our cycles sets from the given filenames,
 * and set the default to the first one. Save it in our global
 * cycles array.
*/
void readCyclesSets(const String (&filenames)[MAX_CYCLES]) {
  // Read through our 8 files and store them in our global
  // cyclesSet
  for (int i = 0; i < MAX_CYCLES; i++) {
    XENOKINETICS_DEBUG_PRINT("Reading cycle set file: ");
    XENOKINETICS_DEBUG_PRINTLN(filenames[i]);
    readCycleSet(filenames[i], i);
  }
}

