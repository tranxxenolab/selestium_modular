/*

TODO

* Decide on what kind of chaotic function to model, and how to incorporate that
  * Or, do some kind of diode chaos circuit?
* What are the CV inputs?
* How do I deal with scaling? DONE
* Setup RTC (is this still needed? I guess so if I'm going to do sky calculations)
* Watch Mortiz' videos again to see how to do various modules in an analog sense
* Do I need MIDI in/out? Should it also involve OSC?
* What about DMX?

File formatting:

* Only use CSV!
* "main.csv" which contains all of the cycles that we know about, along with suggested default scaling
* Set of files names "set001.csv", "set002.csv", etc, which contain subsets of 8 of the cycles that we want to use
* Button will be used to select which set we are working with, in order with wrapping
* This enables us to not have to parse the entire "main.csv" at the beginning

*/

#include <TaskScheduler.h>
#include "main.h"

#if ENABLE_MIDI
//MIDI_CREATE_DEFAULT_INSTANCE();
MIDI_CREATE_INSTANCE(HardwareSerial, Serial7, MyMIDI);
#endif


const int chipSelect = BUILTIN_SDCARD;
File defaultCycles;
File testCycles;
float oscFreq = 440.0;
float detuneFreq = 1.5;
String number;
String period;
String scale;
String offset;
String comment;
//String filename = "solar_system.csv";
String setFilename = "set001.csv";
uint64_t periodVal = 0;
float scaleVal = 0;
uint32_t offsetVal = 0;

int testAnimationAdvanced = 0;

File locFile;
String locFilename = "loc.txt";
String locStringValue;

int noteDelay = 4;

uint64_t cycleVal;
float cycleFloat;

String divisorsFilename = "voice_scales.txt";
String frequenciesFilename = "frequencies.txt";
String sigilsFilename = "sigils.txt";
String waveformsFilename = "waveforms.txt";
String settingsFilename = "settings.txt";

//int chaosIntensitiesButtonPin = 5;
//uint8_t chaosIntensitiesIndex = 0;
//Bounce chaosIntensitiesButton(chaosIntensitiesButtonPin, 50);


/*
int cycleRatioPotPin = A8;

int cycleRatioLEDPin0 = 30;
int cycleRatioLEDPin1 = 31;
int cycleRatioLEDPin2 = 32;

ResponsiveAnalogRead cycleRatioPot(cycleRatioPotPin, true);
*/

int printIndex = 0;
int printIndexDivisor = 100;

// How often (in milliseconds) to check whether we should
// turn a note off or not
uint32_t noteCheckInterval = 5;
uint64_t currentMillis = 0;
uint64_t previousMillis = 0;


void setup() {

  XENOKINETICS_DEBUG_BEGIN(XENOKINETICS_DEBUG_PORT_SPEED);
  // TODO
  // deal with this, not sure what is the best thing to do
  // given that we have to wait for the serial port to connect
  if (XENOKINETICS_DEBUG) {
    // wait for serial port to connect
    delay(200);
  }
  //while (!XENOKINETICS_DEBUG) {
  //  ; // wait for serial port to connect.
  //}

  XENOKINETICS_DEBUG_INFO_PRINTLN("");
  XENOKINETICS_DEBUG_INFO_PRINTLN("------------------------");
  XENOKINETICS_DEBUG_INFO_PRINTLN("");
  XENOKINETICS_DEBUG_INFO_PRINTLN("     XENOKINETICS");
  XENOKINETICS_DEBUG_INFO_PRINTLN("");
  XENOKINETICS_DEBUG_INFO_PRINTLN("------------------------");
  XENOKINETICS_DEBUG_INFO_PRINTLN("");
  XENOKINETICS_DEBUG_INFO_PRINTLN("Starting up...");

  XENOKINETICS_DEBUG_INFO_PRINTLN("Checking first-run state....");
  if (EEPROM.read(FIRST_RUN_ADDR) == NEVER_RUN_STATE) {
    XENOKINETICS_DEBUG_INFO_PRINTLN("Setting EEPROM first-run state and default values.");
    EEPROM.put(FIRST_RUN_ADDR, POST_FIRST_RUN_STATE);
    EEPROM.put(EEPROM_LAT_ADDR, (float) DEFAULT_LAT);
    EEPROM.put(EEPROM_LON_ADDR, (float) DEFAULT_LON);
  } else {
    XENOKINETICS_DEBUG_INFO_PRINTLN("Firmware has been run before.");
  }

  XENOKINETICS_DEBUG_INFO_PRINT("Initializing SD card subsystem...");

  if (!SD.begin(chipSelect)) {
    XENOKINETICS_DEBUG_ERROR_PRINT("ERROR: ");
    XENOKINETICS_DEBUG_ERROR_PRINTLN("Initialization failed! Restarting....");
    return;
  }
  XENOKINETICS_DEBUG_INFO_PRINTLN("Initialization done.");

  // Audio connections require memory to work.  For more
  // detailed information, see the MemoryAndCpuUsage example
  XENOKINETICS_DEBUG_INFO_PRINTLN("Setting audio memory....");
  AudioMemory(AUDIO_MEMORY);

  //XENOKINETICS_DEBUG_INFO_PRINTLN("Enabling audio board....");
  //mqs1.begin();
  //sgtl5000_1.enable();
  //sgtl5000_1.volume(0.3); 

  XENOKINETICS_DEBUG_INFO_PRINTLN("Configuring pins....");
  for (int i = 0; i < MAX_CYCLES; i++) {
    pinMode(triggerPins[i], OUTPUT);
    digitalWrite(triggerPins[i], LOW);
  }

  /*
  for (int i = 0; i < MAX_CYCLES; i++) {
    pinMode(triggerPins[i], OUTPUT);
    digitalWrite(triggerPins[i], HIGH);
    chaosIntensitiesPots[i]->setAnalogResolution(1023);
    chaosIntensitiesPots[i]->enableEdgeSnap();
    chaosIntensitiesPots[i]->setActivityThreshold(20.0);
  }

  for (int i = 0; i < MAX_CHAOS_PINS; i++) {
    chaosPinsReads[i]->setAnalogResolution(CHAOS_PINS_RESOLUTION);
    chaosPinsReads[i]->enableEdgeSnap();
  }

  */
  initializeChaosPinsAvgs();

//  pinMode(chaosIntensitiesButtonPin, INPUT_PULLUP);
  pinMode(ENCODER_BUTTON, INPUT_PULLUP);
  pinMode(SET_BUTTON, INPUT_PULLUP);
  pinMode(DIVISOR_BUTTON, INPUT_PULLUP);
  pinMode(TRIG_GATE_SW, INPUT);

  // Switch is wired backwards...oops
  processTrigGateSwitch();
  //trigGateState = !(digitalRead(TRIG_GATE_SW));

  //pinMode(MUX_S0, OUTPUT);
  //pinMode(MUX_S1, OUTPUT);
  //pinMode(MUX_S2, OUTPUT);
  //pinMode(MUX_S3, OUTPUT);
  //muxPot.setAnalogResolution(1023);
  //muxPot.enableEdgeSnap();

  initADC();
  initADCInputs();
 
  //chaosCV.setAnalogResolution(1023);
  //chaosCV.enableEdgeSnap();
  //scaleCV.setAnalogResolution(1023);
  //scaleCV.disableEdgeSnap();

/*
  pinMode(cycleRatioLEDPin0, OUTPUT);
  pinMode(cycleRatioLEDPin1, OUTPUT);
  pinMode(cycleRatioLEDPin2, OUTPUT);
  digitalWrite(cycleRatioLEDPin0, LOW);
  digitalWrite(cycleRatioLEDPin1, LOW);
  digitalWrite(cycleRatioLEDPin2, LOW);
  cycleRatioPot.setAnalogResolution(1023);
  cycleRatioPot.setActivityThreshold(1);
  cycleRatioPot.enableEdgeSnap();
*/

  XENOKINETICS_DEBUG_INFO_PRINTLN("Assigning base values for chaos intensity pots....")
  for (int i = 0; i < MAX_CYCLES; i++) {
    chaosIntensityPotsValues[i] = readChaosIntensityPot(i);
  }

  XENOKINETICS_DEBUG_INFO_PRINTLN("Setting up LED display....");
  display.begin(CS_PIN);
  display.shutdown(0, false);
  display.setIntensity(0, 1);
  display.clearDisplay(0);
  displayImage(bootImages[0]);

  displayImage(bootImages[1]);

  XENOKINETICS_DEBUG_INFO_PRINTLN("Starting task runner....");
  runner.init();

  displayImage(bootImages[2]);

  XENOKINETICS_DEBUG_INFO_PRINTLN("Checking latitude and longitude file....");
  locFile = SD.open(locFilename.c_str(), FILE_READ);
  if (locFile) {
    XENOKINETICS_DEBUG_INFO_PRINTLN("Updating latitude and longitude values.");
    while (locFile.available()) {
      locStringValue = locFile.readStringUntil('\n');
      latValue = locStringValue.toFloat();
      EEPROM.put(EEPROM_LAT_ADDR, latValue);

      locStringValue = locFile.readStringUntil('\n');
      lonValue = locStringValue.toFloat();
      EEPROM.put(EEPROM_LON_ADDR, lonValue);
    }

    XENOKINETICS_DEBUG_INFO_PRINTLN("Removing latitude and longitude file.");
    locFile.close();
    SD.remove(locFilename.c_str());
  } else {
    XENOKINETICS_DEBUG_INFO_PRINTLN("No new file, using saved latitude and longitude values.");
    EEPROM.get(EEPROM_LAT_ADDR, latValue);
    EEPROM.get(EEPROM_LON_ADDR, lonValue);
  }

  XENOKINETICS_DEBUG_INFO_PRINT("Setting latitude and longitude to ");
  XENOKINETICS_DEBUG_INFO_PRINT(latValue);
  XENOKINETICS_DEBUG_INFO_PRINT(" ");
  XENOKINETICS_DEBUG_INFO_PRINT(lonValue);
  XENOKINETICS_DEBUG_INFO_PRINTLN(".");

  displayImage(bootImages[3]);

  XENOKINETICS_DEBUG_INFO_PRINTLN("Trying to read settings file....");
  initDivisorsSettings();
  readSettings(settingsFilename);

  // Output info from the settings file after reading
  // This is all mega debug stuff and can be removed at some point
  XENOKINETICS_DEBUG_PRINTLN("Gate Length:");
  XENOKINETICS_DEBUG_PRINTLN(settings.gateLength)
  for (int i = 0; i < MAX_CYCLES; i++) {
    XENOKINETICS_DEBUG_PRINT(settings.envelopesDefault[i]);
  }
  XENOKINETICS_DEBUG_PRINTLN("");
  XENOKINETICS_DEBUG_PRINTLN("Waveforms default:");
  for (int i = 0; i < MAX_CYCLES; i++) {
    XENOKINETICS_DEBUG_PRINT(settings.waveformsDefault[i]);
  }
  XENOKINETICS_DEBUG_PRINTLN("");
  XENOKINETICS_DEBUG_PRINTLN("Frequencies default:");
  for (int i = 0; i < MAX_CYCLES; i++) {
    XENOKINETICS_DEBUG_PRINT(settings.frequenciesDefault[i]);
  }
  XENOKINETICS_DEBUG_PRINTLN("");
  XENOKINETICS_DEBUG_PRINTLN("Gates default:");
  for (int i = 0; i < MAX_CYCLES; i++) {
    XENOKINETICS_DEBUG_PRINT(settings.gatesDefault[i]);
  }
  XENOKINETICS_DEBUG_PRINTLN("");
  XENOKINETICS_DEBUG_PRINTLN("divisorsIndex default (set1):");
  for (int i = 0; i < MAX_CYCLES; i++) {
    XENOKINETICS_DEBUG_PRINT(settings.divisorsIndexDefault[0][i]);
  }
  XENOKINETICS_DEBUG_PRINTLN("");
  XENOKINETICS_DEBUG_PRINTLN("divisorsIndex default (set2):");
  for (int i = 0; i < MAX_CYCLES; i++) {
    XENOKINETICS_DEBUG_PRINT(settings.divisorsIndexDefault[1][i]);
  }
  XENOKINETICS_DEBUG_PRINTLN("");
  XENOKINETICS_DEBUG_PRINTLN("divisorsIndex default (set3):");
  for (int i = 0; i < MAX_CYCLES; i++) {
    XENOKINETICS_DEBUG_PRINT(settings.divisorsIndexDefault[2][i]);
  }
  XENOKINETICS_DEBUG_PRINTLN("");
  XENOKINETICS_DEBUG_PRINTLN("divisorsIndex default (set4):");
  for (int i = 0; i < MAX_CYCLES; i++) {
    XENOKINETICS_DEBUG_PRINT(settings.divisorsIndexDefault[3][i]);
  }
  XENOKINETICS_DEBUG_PRINTLN("");
  XENOKINETICS_DEBUG_PRINTLN("divisorsIndex default (set5):");
  for (int i = 0; i < MAX_CYCLES; i++) {
    XENOKINETICS_DEBUG_PRINT(settings.divisorsIndexDefault[4][i]);
  }
  XENOKINETICS_DEBUG_PRINTLN("");
  XENOKINETICS_DEBUG_PRINTLN("divisorsIndex default (set6):");
  for (int i = 0; i < MAX_CYCLES; i++) {
    XENOKINETICS_DEBUG_PRINT(settings.divisorsIndexDefault[5][i]);
  }
  XENOKINETICS_DEBUG_PRINTLN("");
  XENOKINETICS_DEBUG_PRINTLN("divisorsIndex default (set7):");
  for (int i = 0; i < MAX_CYCLES; i++) {
    XENOKINETICS_DEBUG_PRINT(settings.divisorsIndexDefault[6][i]);
  }
  XENOKINETICS_DEBUG_PRINTLN("");
  XENOKINETICS_DEBUG_PRINTLN("divisorsIndex default (set8):");
  for (int i = 0; i < MAX_CYCLES; i++) {
    XENOKINETICS_DEBUG_PRINT(settings.divisorsIndexDefault[7][i]);
  }
  XENOKINETICS_DEBUG_PRINTLN("");

  XENOKINETICS_DEBUG_PRINTLN("");
  XENOKINETICS_DEBUG_PRINTLN("divisors (set1):");
  for (int i = 0; i < MAX_CYCLES + 1; i++) {
    XENOKINETICS_DEBUG_PRINT(settings.divisors[0][i]);
    XENOKINETICS_DEBUG_INFO_PRINT(" ");
  }
  XENOKINETICS_DEBUG_PRINTLN("");
  XENOKINETICS_DEBUG_PRINTLN("divisors (set2):");
  for (int i = 0; i < MAX_CYCLES + 1; i++) {
    XENOKINETICS_DEBUG_PRINT(settings.divisors[1][i]);
    XENOKINETICS_DEBUG_INFO_PRINT(" ");
  }
  XENOKINETICS_DEBUG_PRINTLN("");
  XENOKINETICS_DEBUG_PRINTLN("divisors (set3):");
  for (int i = 0; i < MAX_CYCLES + 1; i++) {
    XENOKINETICS_DEBUG_PRINT(settings.divisors[2][i]);
    XENOKINETICS_DEBUG_INFO_PRINT(" ");
  }
  XENOKINETICS_DEBUG_PRINTLN("");
  XENOKINETICS_DEBUG_PRINTLN("divisors (set4):");
  for (int i = 0; i < MAX_CYCLES + 1; i++) {
    XENOKINETICS_DEBUG_PRINT(settings.divisors[3][i]);
    XENOKINETICS_DEBUG_INFO_PRINT(" ");
  }
  XENOKINETICS_DEBUG_PRINTLN("");
  XENOKINETICS_DEBUG_PRINTLN("divisors (set5):");
  for (int i = 0; i < MAX_CYCLES + 1; i++) {
    XENOKINETICS_DEBUG_PRINT(settings.divisors[4][i]);
    XENOKINETICS_DEBUG_INFO_PRINT(" ");
  }
  XENOKINETICS_DEBUG_PRINTLN("");
  XENOKINETICS_DEBUG_PRINTLN("divisors (set6):");
  for (int i = 0; i < MAX_CYCLES + 1; i++) {
    XENOKINETICS_DEBUG_PRINT(settings.divisors[5][i]);
    XENOKINETICS_DEBUG_INFO_PRINT(" ");
  }
  XENOKINETICS_DEBUG_PRINTLN("");
  XENOKINETICS_DEBUG_PRINTLN("divisors (set7):");
  for (int i = 0; i < MAX_CYCLES + 1; i++) {
    XENOKINETICS_DEBUG_PRINT(settings.divisors[6][i]);
    XENOKINETICS_DEBUG_INFO_PRINT(" ");
  }
  XENOKINETICS_DEBUG_PRINTLN("");
  XENOKINETICS_DEBUG_PRINTLN("divisors (set8):");
  for (int i = 0; i < MAX_CYCLES + 1; i++) {
    XENOKINETICS_DEBUG_PRINT(settings.divisors[7][i]);
    XENOKINETICS_DEBUG_INFO_PRINT(" ");
  }
  XENOKINETICS_DEBUG_PRINTLN("");



  XENOKINETICS_DEBUG_INFO_PRINTLN("Trying to read frequencies file....");
  readFrequencies(frequenciesFilename);

  XENOKINETICS_DEBUG_INFO_PRINTLN("Trying to read sigil design file");
  readSigils(sigilsFilename);

  XENOKINETICS_DEBUG_INFO_PRINTLN("Trying to read waveforms file");
  readWaveforms(waveformsFilename);

  XENOKINETICS_DEBUG_INFO_PRINTLN("Trying to read divisors file....");
  readDivisors(divisorsFilename);
  initializeAllDivisors();
  initEncoderKnobValues();

  // Read all of our cycles sets
  XENOKINETICS_DEBUG_INFO_PRINTLN("Setting up cycles....")
  readCyclesSets(cyclesSetsFilenames);

  displayImage(bootImages[4]);


  XENOKINETICS_DEBUG_INFO_PRINTLN("Initializing cycles and starting tasks....")

  // Initialize cycles to the first one in the set
  initCycleSet();

  // Add and start the tasks
  for (int i = 0; i < MAX_CYCLES; i++) {
    runner.addTask(*tasks[i]);
    /*
    tasks[i]->setInterval(cycles[i].periodCorrected);
    tasks[i]->enable();
    // Adjust everything so that we don't have all of the tasks firing
    // immediately on starting the main loop
    tasks[i]->adjust(cycles[i].periodCorrected - 1);
    */

    /* CLASS DEFINITION */
    tasks[i]->setInterval(currentCyclesListClass[i].getPeriodCorrected());
    tasks[i]->enable();
    // Adjust everything so that we don't have all of the tasks firing
    // immediately on starting the main loop
    tasks[i]->adjust(currentCyclesListClass[i].getPeriodCorrected() - 1);
  }

  displayImage(bootImages[5]);


  // TODO
  // Theoretically I should be able to use pins 0 and 1 for this...
  #if ENABLE_MIDI
  XENOKINETICS_DEBUG_INFO_PRINTLN("Setting up MIDI....");
  //MIDI_CREATE_INSTANCE(HardwareSerial, Serial7, MIDI);
  MyMIDI.begin(5);
  #endif

  displayImage(bootImages[6]);



  // TODO
  // Read some of the parameters from a file, or use default values
  XENOKINETICS_DEBUG_INFO_PRINTLN("Configuring and starting waveforms....");
  for (int i = 0; i < MAX_CYCLES; i++) {
    XENOKINETICS_DEBUG_PRINTLN(fundamentalFrequency * noteRatios[i]);
    waveforms[i]->begin(0.99, fundamentalFrequency * noteRatios[i], waveformList[i]);
  }

  // TODO
  // Read some of the parameters from a file, or use default values
  XENOKINETICS_DEBUG_INFO_PRINTLN("Configuring envelopes....");
  for (int i = 0; i < MAX_CYCLES; i++) {
    envelopes[i]->attack(envelopeValues[i].attack);
    envelopes[i]->decay(envelopeValues[i].decay);
    envelopes[i]->sustain(envelopeValues[i].sustain);
    envelopes[i]->release(envelopeValues[i].release);
  }

  XENOKINETICS_DEBUG_INFO_PRINTLN("Setting mixer levels....");
  mixer1.gain(0, 0.25);
  mixer1.gain(1, 0.25);
  mixer1.gain(2, 0.25);
  mixer1.gain(3, 0.25);

  mixer2.gain(0, 0.25);
  mixer2.gain(1, 0.25);
  mixer2.gain(2, 0.25);
  mixer2.gain(3, 0.25);

  mixer3.gain(0, 1.0);
  mixer3.gain(1, 1.0);



  // TODO
  // This is just for testing
  trigGate = false;

  displayImage(bootImages[7]);

  XENOKINETICS_DEBUG_INFO_PRINTLN("Setup complete!");
  XENOKINETICS_DEBUG_INFO_PRINTLN("");
  if (!XENOKINETICS_DEBUG) {
    XENOKINETICS_DEBUG_INFO_PRINTLN("This firmware has been compiled without debugging information.");
    XENOKINETICS_DEBUG_INFO_PRINTLN("As a result, no further information will be outputted");
    XENOKINETICS_DEBUG_INFO_PRINTLN("after the task loop begins.");
    XENOKINETICS_DEBUG_INFO_PRINTLN("Please flash a firmware with debugging info enabled");
    XENOKINETICS_DEBUG_INFO_PRINTLN("if you need to, well, debug something about XENOKINETICS.");
  } else {
    XENOKINETICS_DEBUG_INFO_PRINTLN("Firmware compiled with debugging information.");
    XENOKINETICS_DEBUG_INFO_PRINTLN("This may adversely affect performance with very quick cycles,");
    XENOKINETICS_DEBUG_INFO_PRINTLN("as information about each cycle, as the cycle is run,");
    XENOKINETICS_DEBUG_INFO_PRINTLN("is outputted to the serial console.");
    XENOKINETICS_DEBUG_INFO_PRINTLN("If you encounter problems please flash a firmware without");
    XENOKINETICS_DEBUG_INFO_PRINTLN("debugging information compiled in.");
  }
  XENOKINETICS_DEBUG_INFO_PRINTLN("Beginning task loop.");

}

// TODO
// There has to be a better way of doing this!
void toggleMux(uint8_t value, int MUX0, int MUX1, int MUX2, int MUX3) {
    // TODO
    // This needs to be done using a shift register, likely, but we're going to hard code it for now, ughhh
    switch (value) {
      case (0):
        digitalWrite(MUX0, LOW); digitalWrite(MUX1, LOW); digitalWrite(MUX2, LOW); digitalWrite(MUX3, LOW);
        break;
      case (1):
        digitalWrite(MUX0, HIGH); digitalWrite(MUX1, LOW); digitalWrite(MUX2, LOW); digitalWrite(MUX3, LOW);
        break;
      case (2):
        digitalWrite(MUX0, LOW); digitalWrite(MUX1, HIGH); digitalWrite(MUX2, LOW); digitalWrite(MUX3, LOW);
        break;
      case (3):
        digitalWrite(MUX0, HIGH); digitalWrite(MUX1, HIGH); digitalWrite(MUX2, LOW); digitalWrite(MUX3, LOW);
        break;
      case (4):
        digitalWrite(MUX0, LOW); digitalWrite(MUX1, LOW); digitalWrite(MUX2, HIGH); digitalWrite(MUX3, LOW);
        break;
      case (5):
        digitalWrite(MUX0, HIGH); digitalWrite(MUX1, LOW); digitalWrite(MUX2, HIGH); digitalWrite(MUX3, LOW);
        break;
      case (6):
        digitalWrite(MUX0, LOW); digitalWrite(MUX1, HIGH); digitalWrite(MUX2, HIGH); digitalWrite(MUX3, LOW);
        break;
      case (7):
        digitalWrite(MUX0, HIGH); digitalWrite(MUX1, HIGH); digitalWrite(MUX2, HIGH); digitalWrite(MUX3, LOW);
        break;
      default:
        digitalWrite(MUX0, LOW); digitalWrite(MUX1, LOW); digitalWrite(MUX2, LOW); digitalWrite(MUX3, LOW);
        break;
    }
}


void toggleLEDs(uint8_t value, int LED0, int LED1, int LED2) {
    // TODO
    // This needs to be done using a shift register, likely, but we're going to hard code it for now, ughhh
    switch (value) {
      case (0):
        digitalWrite(LED0, LOW); digitalWrite(LED1, LOW); digitalWrite(LED2, LOW);
        break;
      case (1):
        digitalWrite(LED0, HIGH); digitalWrite(LED1, LOW); digitalWrite(LED2, LOW);
        break;
      case (2):
        digitalWrite(LED0, LOW); digitalWrite(LED1, HIGH); digitalWrite(LED2, LOW);
        break;
      case (3):
        digitalWrite(LED0, HIGH); digitalWrite(LED1, HIGH); digitalWrite(LED2, LOW);
        break;
      case (4):
        digitalWrite(LED0, LOW); digitalWrite(LED1, LOW); digitalWrite(LED2, HIGH);
        break;
      case (5):
        digitalWrite(LED0, HIGH); digitalWrite(LED1, LOW); digitalWrite(LED2, HIGH);
        break;
      case (6):
        digitalWrite(LED0, LOW); digitalWrite(LED1, HIGH); digitalWrite(LED2, HIGH);
        break;
      case (7):
        digitalWrite(LED0, HIGH); digitalWrite(LED1, HIGH); digitalWrite(LED2, HIGH);
        break;
      default:
        digitalWrite(LED0, LOW); digitalWrite(LED1, LOW); digitalWrite(LED2, LOW);
        break;
    }


}

void loop() {
  runner.execute(); 

  //if (display.displayAnimate()) {
  //  display.displayReset();
  //}
  checkInterface();
  printIndex += 1;

  if ((printIndex % printIndexDivisor) == 0) {
    //XENOKINETICS_DEBUG_PRINTLN(tasks[2]->getInterval());
    //XENOKINETICS_DEBUG_PRINTLN(chaosIntensityPotsValues[2] * abs(chaosPinsAvgs[1]->get() - chaosPinsAvgs[0]->get())/MAX_CHAOS_VALUE);
    //printChaosIntensityPotValues();
    //XENOKINETICS_DEBUG_PRINT(chaosCVValue);
    //XENOKINETICS_DEBUG_PRINT(" ");
    //XENOKINETICS_DEBUG_PRINT(setCVValue);
    //XENOKINETICS_DEBUG_PRINTLN("");
/*
    XENOKINETICS_DEBUG_PRINT("chaos intensities pot values:");
    for (int i = 0; i < MAX_CYCLES; i++) {
      XENOKINETICS_DEBUG_PRINT(" ");
      XENOKINETICS_DEBUG_PRINT(chaosIntensities[i]);
      XENOKINETICS_DEBUG_PRINT(",");
    }
    XENOKINETICS_DEBUG_PRINTLN("");
*/
    printIndex = 1;
    //XENOKINETICS_DEBUG_PRINTLN(chaosCV.getValue());
    //int value = adc->adc0->analogRead(A0);
    //XENOKINETICS_DEBUG_PRINTLN(value);

/*
    XENOKINETICS_DEBUG_PRINT("Chaos pots values: ");
    for (int i = 0; i < MAX_CYCLES; i++) {
      XENOKINETICS_DEBUG_PRINT(chaosIntensityPotsValues[i]);
      XENOKINETICS_DEBUG_PRINT(" ");
    }
    XENOKINETICS_DEBUG_PRINTLN("");

    XENOKINETICS_DEBUG_PRINT("Chaos and Scale CV values: ");
    XENOKINETICS_DEBUG_PRINT(chaosCVValue);
    XENOKINETICS_DEBUG_PRINT(" ");
    XENOKINETICS_DEBUG_PRINT(scaleCVValue);
    XENOKINETICS_DEBUG_PRINTLN("");
*/
    //Serial.println(scaleImage, HEX);
  }


  // Check our voices every noteCheckInterval ms
  // This way we don't use the blocking of delay in the tasks
  currentMillis = millis();
  if (currentMillis - previousMillis >= noteCheckInterval) {
    previousMillis = currentMillis;
    for (int i = 0; i < MAX_CYCLES; i++) {
      checkVoice(i);
      checkTrigger(i);
#if ENABLE_MIDI
      // This is a hack due to my issues with creating a
      // MIDI object that works across all of the files.
      // I instead set a flag in the scheduling method
      // which I then check here to determine when to
      // send note-on, note-off messages, which is 
      // all we're doing with our MIDI.
      if (midiFlags[i] == 1) {
        MyMIDI.sendNoteOn(midiNotes[i], 127, 5);
        midiFlags[i] = -1;
      } else if (midiFlags[i] == 0) {
        MyMIDI.sendNoteOff(midiNotes[i], 0, 5);
        midiFlags[i] = -1;
      }
#endif
    }
  }

    /*
    if (chaosCV.hasChanged()) {
        XENOKINETICS_DEBUG_PRINT("Chaos CV raw value now: ");
        XENOKINETICS_DEBUG_PRINTLN(chaosCV.getValue());
    }
    */

}