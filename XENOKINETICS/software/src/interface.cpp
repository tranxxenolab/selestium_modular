#include "main.h"

uint8_t currentInterfaceState = STATE_SIGIL;

long encoderKnobValue = -999;
Encoder encoderKnob(ENCODER_1, ENCODER_2);
Bounce encoderButton(ENCODER_BUTTON, 50);
uint8_t currentVoice = 0;
bool stateDivisorsFirstProcess = true;

//int64_t encoderKnobValues[MAX_CYCLES] = {40, 40, 40, 40, 40, 40, 40, 40};
int64_t encoderKnobValues[MAX_SETS][MAX_CYCLES] = {
    {32, 32, 32, 32, 32, 32, 32, 32},
    {32, 32, 32, 32, 32, 32, 32, 32},
    {32, 32, 32, 32, 32, 32, 32, 32},
    {32, 32, 32, 32, 32, 32, 32, 32},
    {32, 32, 32, 32, 32, 32, 32, 32},
    {32, 32, 32, 32, 32, 32, 32, 32},
    {32, 32, 32, 32, 32, 32, 32, 32},
    {32, 32, 32, 32, 32, 32, 32, 32},
};
float divisors[MAX_CYCLES] = {1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0};
float currentDivisors[MAX_CYCLES] = {1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0};
int blinkPeriod= 350;
int blinkPeriodCounter[MAX_CYCLES] = {0, 0, 0, 0, 0, 0, 0, 0};
bool blinkDirection[MAX_CYCLES] = {false, false, false, false, false, false, false, false};

/**
 * Display an image based on a 64bit value.
 * Taken from <https://xantorohara.github.io/led-matrix-editor/>
 * 
*/
void displayImage(uint64_t image) {
  for (int i = 0; i < 8; i++) {
    byte row = (image >> i * 8) & 0xFF;
    for (int j = 0; j < 8; j++) {
      display.setLed(0, i, j, bitRead(row, j));
    }
  }
}

/**
 * Processes default encoder knob values
 * based on settings file.
 * 
*/
void initEncoderKnobValues() {
    for (int i = 0; i < MAX_SETS; i++) {
        for (int j = 0; j < MAX_CYCLES; j++) {
            encoderKnobValues[i][j] = settings.divisorsIndexDefault[i][j] * ENCODER_KNOB_UNIT;
        }
    }
}

/**
 * Processes the state based on the value of the encoder knob
 * and button.
*/
void processInterfaceState(uint8_t state) {
    //uint8_t voiceSetIndex = 4;
    uint8_t divisorIndex = settings.divisorsIndexDefault[setIndex][currentVoice];
    float tempDivisor;
    if (state == STATE_SIGIL) {
        //XENOKINETICS_DEBUG_PRINTLN("In SIGIL state");
    } else if (state == STATE_DIVISORS) {
        //XENOKINETICS_DEBUG_PRINT("In DIVISORS state on voice ");
        //XENOKINETICS_DEBUG_PRINTLN(currentVoice);
        //XENOKINETICS_DEBUG_PRINTLN(encoderKnobValue);
        encoderKnobValues[setIndex][currentVoice] = encoderKnobValue;
        encoderKnob.write(encoderKnobValues[setIndex][currentVoice]);

        divisorIndex = floor((int)map(encoderKnobValue, KNOB_MIN, KNOB_MAX, 0, 8));
        tempDivisor = divisorsSet[divisorIndex];
        /*
        if (tempDivisor != voiceScales[currentVoice]) {
            voiceScales[currentVoice] = tempDivisor;
            XENOKINETICS_DEBUG_PRINT("Changed divisor to: ");
            XENOKINETICS_DEBUG_PRINTLN(tempDivisor);
            updatePeriod(currentVoice, tempDivisor);
        }
        */
        if (tempDivisor != allDivisors[setIndex][currentVoice]) {
            allDivisors[setIndex][currentVoice] = tempDivisor;
            XENOKINETICS_DEBUG_PRINT("Changed divisor to: ");
            XENOKINETICS_DEBUG_PRINTLN(tempDivisor);
            updatePeriod(currentVoice, tempDivisor);
        }

        //XENOKINETICS_DEBUG_PRINT("Current divisor is set to ");
        //XENOKINETICS_DEBUG_PRINTLN(voiceScales[currentVoice]);
    }
}

/**
 * Process the incoming data from the chaos generator. We have an array of
 * ResponsiveAnalogRead's that we check and then save the values to a separate
 * Smoothed moving average array.
*/
void processChaosPins() {
    for (int i = 0; i < MAX_CHAOS_PINS; i++) {
        chaosPinsAvgs[i]->add(readADC(chaosPinsMapping[i]));
        /*
        chaosPinsReads[i]->update();
        if (chaosPinsReads[i]->hasChanged()) {
            chaosPinsAvgs[i]->add(chaosPinsReads[i]->getValue());
            //chaosPinsValues[i] = chaosPinsReads[i]->getValue();
        }
        */
    }

    //XENOKINETICS_DEBUG_PRINTLN(abs(chaosPinsAvgs[1]->get() - chaosPinsAvgs[0]->get()));
    //XENOKINETICS_DEBUG_PRINT("X: ");
    //XENOKINETICS_DEBUG_PRINTLN(chaosPinsAvgs[0]->get());
    //XENOKINETICS_DEBUG_PRINT("Y: ");
    //XENOKINETICS_DEBUG_PRINTLN(chaosPinsAvgs[1]->get());
    //XENOKINETICS_DEBUG_PRINT("Z: ");
    //XENOKINETICS_DEBUG_PRINTLN(chaosPinsAvgs[2]->get());
    

}

/**
 * Process the pots for the chaos intensities.
 * TODO still need to finish writing this one.
*/
/*
void processChaosIntensitiesPots() {
    for (int i = 0; i < MAX_CYCLES; i++) {
        chaosIntensitiesPots[i]->update();
        if (chaosIntensitiesPots[i]->hasChanged()) {
            XENOKINETICS_DEBUG_PRINT("pot changed: ");
            XENOKINETICS_DEBUG_PRINTLN(i + 1);
            chaosIntensities[i] = (float)map((float)chaosIntensitiesPots[i]->getValue(), 0.0, 1023.0, 1.0, 0.0);
            if (chaosIntensities[i] < 0.015) {
                chaosIntensities[i] = 0;
            };

            updateIntervalChaos(i);
            //tasks[i]->setInterval(1000);
        }
    }
}
*/

float readChaosIntensityPot(uint8_t i) {
    uint16_t tempADC = 0;
    float tempValue = 0.0f;

    tempADC = readADC(chaosIntensityPotsMapping[i]);
    if (chaosIntensitiesTransforms[i].invert) {
        // TODO
        // Bad assumption that the max value is the same on 
        // both channels...update this sometime
        tempADC = adc0MaxValue - tempADC;
    }

    // OLD WAY OF DOING THINGS
    //tempValue = 2.0f*((float(tempADC) - float(adc0MaxValue/2.0f)) / float(adc0MaxValue));
    //constrain(tempValue, -1.0f, 1.0f);

    // Incorporate a zero range around the top of the pot
    // Taken from: https://forum.arduino.cc/t/adding-a-dead-zone-to-a-analog-imput-on-arduino-leonardo/662508/6
    if (tempADC < CHAOS_POT_ZERO_LOW) {
        tempValue = map((float)tempADC, 0.0f, (float)CHAOS_POT_ZERO_LOW, -1.0f, 0.0f);
    } else if (tempADC > CHAOS_POT_ZERO_HIGH) {
        tempValue = map((float)tempADC, (float)CHAOS_POT_ZERO_HIGH, (float)adc0MaxValue, 0.0f, 1.0f);
    } else {
        tempValue = 0.0f;
    }

    return tempValue;
}

void processChaosIntensityPots() {
    //uint16_t tempADC = 0;
    float tempValue = 0.0f;

    for (int i = 0; i < MAX_CYCLES; i++) {
       tempValue = readChaosIntensityPot(i);

        // If the pot has moved a bit, update the value and save
        // the time it was updated
        if (abs(tempValue - chaosIntensityPotsValues[i]) > 0.03) {
            chaosIntensityPotsTimes[i] = millis();
            chaosIntensityPotsValues[i] = tempValue;
            //updateIntervalChaos(i);
        }

        // If the pot has settled for a bit, and it had previously moved,
        // update the interval, reset the updated time
        if (((millis() - chaosIntensityPotsTimes[i]) > CHAOS_POT_SETTLE_DELAY) && (chaosIntensityPotsTimes[i] != 0)) {
            XENOKINETICS_DEBUG_PRINTLN("HERE!!!");
            //chaosIntensityPotsValues[i] = tempValue;
            updateIntervalChaos(i);
            chaosIntensityPotsTimes[i] = 0;

        }
        // TODO
        // figure out why when this is called, the tasks don't get called anymore
        //updateIntervalChaos(i);

        // TODO
        // Add in smoothing later
        //smoothed_pot_values[i] += PotTransforms[i].filterCoeff * (tempValue - smoothed_pot_values[i]);
        //constrain(smoothed_pot_values[i], 0.0f, 1.0f);
    }


}

/**
 * Process and update the display based on which state we're in,
 * SIGIL or DIVISORS.
*/
void processDisplay() {
    int index = 0;
    scaleImage = 0;

    for (int i = 0; i < MAX_DISPLAY_ROWS; i++) {
        // TODO
        // Need to figure out the right way of doing this...but this is basic for now
        // This may not be the most accurate way of doing things and should be updated
        // OLD METHOD
        //index = floorf((float(encoderKnobValues[i]) / float(KNOB_MAX)) * 10);
        //index = int(index - 1);

        // NEW METHOD
        // this works, i.e., when we see the dot move to a new value, the divisor changes
        // but it still requires multiple turns of the knob
        index = floor((int)map(encoderKnobValues[setIndex][i], KNOB_MIN, KNOB_MAX, 0, 8));
        //index = encoderKnobValues[setIndex][i];

        if (index < 0) {index = 0;}
        if (index > (MAX_DISPLAY_ROWS)) {index = MAX_DISPLAY_ROWS;}
        // This is the way we try and blink the current voice that we're editing
        if (i == currentVoice) {
            if (blinkDirection[i] == false) {
                blinkPeriodCounter[i] += 1;
                scaleImage |= ((uint64_t)barValues[index] << ((MAX_DISPLAY_ROWS - i - 1)*8));
            } else {
                blinkPeriodCounter[i] -= 1;
                scaleImage |= ((uint64_t)0 << ((MAX_DISPLAY_ROWS - i - 1)*8));
            }

            if (blinkPeriodCounter[i] == blinkPeriod) {
                blinkDirection[i] = true;
            } else if (blinkPeriodCounter[i] == 0) {
                blinkDirection[i] = false;
            }

        } else {
            scaleImage |= ((uint64_t)barValues[index] << ((MAX_DISPLAY_ROWS - i - 1)*8));
        }

        /*
        if (i == currentVoice) {
            if (blinkDirection[i] == false) {
                blinkPeriodCounter[i] += 1;
                for (int j = 0; j < index; j++) {
                    tempValue += ((uint64_t)barValues[index] << (i*8));
                }
                scaleImage += tempValue;
            } else {
                // FOR NOW, NOT BLINKING
                //blinkPeriodCounter[i] -= 1;
                //scaleImage |= ((uint64_t)0 << (i*8));
            }

            if (blinkPeriodCounter[i] == blinkPeriod) {
                blinkDirection[i] = true;
            } else if (blinkPeriodCounter[i] == 0) {
                blinkDirection[i] = false;
            }

        } else {
                for (int j = 0; j < index; j++) {
                    tempValue += ((uint64_t)barValues[index] << (i*8));
                }
                scaleImage += tempValue;

        }
        */

    }

  if (currentInterfaceState == STATE_SIGIL) {
    displayImage(sigilList[setIndex]);
  } else if (currentInterfaceState == STATE_DIVISORS) {
    displayImage(scaleImage);
  }

}

/**
 * Process our set change button, switch sets,
 * update period based on current user interface state.
 * 
*/
void processButtons() {
  setButton.update();

  // If we have the falling edge...
  // update index or set back to 0
  if (setButton.fallingEdge() && (currentInterfaceState == STATE_SIGIL)) {
    setIndex += 1;
    updateSet();
  };

  divisorButton.update();

  // If we have the falling edge...
  // update index or set back to 0
  if (divisorButton.fallingEdge() && (currentInterfaceState == STATE_SIGIL)) {
    divisorIndex += 1;
    updateSet();
  };


}

void updateSet() {
    if (setIndex >= MAX_CYCLES) {setIndex = 0;}
    if (divisorIndex >= MAX_CYCLES) {divisorIndex = 0;}

    float divisorValue = 0;
    float minDivisorValue = divisorsSet[0];
    float maxDivisorValue = divisorsSet[MAX_VOICES];
    int encoderValue = 32;
    char commentVal[MAX_COMMENT_LEN];

    for (int i = 0; i < MAX_CYCLES; i++) {
        // cycles[MAX_CYCLES] is our global variable holding our current
        // set of cycles, so we update it here with the set that we
        // are on now.
        // We also update the period using the current voiceScales
        // that have been previously saved based on the interface state.
        cycles[i].period = cyclesSets[setIndex].cyclesSet[i].period;
        cycles[i].periodCorrected = cyclesSets[setIndex].cyclesSet[i].periodCorrected;
        cycles[i].offset = cyclesSets[setIndex].cyclesSet[i].offset;
        cycles[i].scale = cyclesSets[setIndex].cyclesSet[i].scale;
        strncpy(cycles[i].comment, cyclesSets[setIndex].cyclesSet[i].comment, MAX_COMMENT_LEN);

        currentCyclesListClass[i].setPeriod(cyclesSetsClass[setIndex].cyclesSet[i].getPeriod());
        currentCyclesListClass[i].setPeriodCorrected(cyclesSetsClass[setIndex].cyclesSet[i].getPeriodCorrected());
        currentCyclesListClass[i].setOffset(cyclesSetsClass[setIndex].cyclesSet[i].getOffset());
        currentCyclesListClass[i].setScale(cyclesSetsClass[setIndex].cyclesSet[i].getScale());
        cyclesSetsClass[setIndex].cyclesSet[i].getComment(commentVal);
        currentCyclesListClass[i].setComment(commentVal);
        currentCyclesListClass[i].setEnvelope(cyclesSetsClass[setIndex].cyclesSet[i].getEnvelope());
        currentCyclesListClass[i].setWaveform(cyclesSetsClass[setIndex].cyclesSet[i].getWaveform());


        //updatePeriod(i, voiceScales[i]);
        updatePeriod(i, allDivisors[setIndex][i]);
        for (int j = 0; j < (MAX_VOICES + 1); j++) {
            if (areEqualRel(divisorsSet[j], allDivisors[setIndex][i])) {
                divisorValue = divisorsSet[j];
                // CHANGING
                encoderValue = (int) ceil(map(j, 0.0, (float) MAX_VOICES, (float) KNOB_MIN, (float) KNOB_MAX));
                //encoderValue = j;
                //XENOKINETICS_DEBUG_PRINT("ENCODER VALUE IS: ");
                //XENOKINETICS_DEBUG_PRINTLN(encoderValue);
                encoderKnobValues[setIndex][i] = encoderValue;
            }
        }
    }
    //XENOKINETICS_DEBUG_PRINTF("Current voice scales for set %d: \n", setIndex);
    //XENOKINETICS_DEBUG_PRINTF("%f %f %f %f %f %f %f %f\n", allVoiceScales[setIndex][0], allVoiceScales[setIndex][1], allVoiceScales[setIndex][2], allVoiceScales[setIndex][3], allVoiceScales[setIndex][4], allVoiceScales[setIndex][5], allVoiceScales[setIndex][6], allVoiceScales[setIndex][7]);

}

/**
 * Simple method to set our global variable for the trig/gate state based on the switch.
 * 
 * Switch is wired backwards, which is why we have the inverted logic here.
 * 
*/
void processTrigGateSwitch() {
    trigGateState = !(digitalRead(TRIG_GATE_SW));
}

void processCVs() {
    uint16_t tempADC = 0;
    float tempValue = 0;

    // TODO
    // Make this so that we have a range from -1.0 to 1.0
    tempADC = readADC(CHAOS_CV_PIN);
    if (chaosCVTransform.invert) {
        tempADC = adc1MaxValue - tempADC;
    }
    tempValue = tempADC / float(adc1MaxValue);
    constrain(tempValue, 0.0f, 1.0f);
    chaosCVValue = tempValue;

    tempADC = readADC(SET_CV_PIN);
    if (setCVTransform.invert) {
        tempADC = adc1MaxValue - tempADC;
    }
    tempValue = tempADC / float(adc1MaxValue);
    constrain(tempValue, 0.0f, 1.0f);
    setCVValue = tempValue;

    // TODO
    // But question is: what happens if we're in STATE_DIVISORS when this changes? 
    // How should we handle this? The display should be updated accordingly, no?
    // and is there a way to tell that we have a CV being inputted? hard to know, yes?
    // especially as we don't have a way to distinguish a cable being inserted
    // Then...is this the best CV value for us to use? Or should this be repurposed?
    uint8_t setCVIndex = processSetCVValue();
    if (setCVIndex != setIndex) {
        setIndex = setCVIndex;
        updateSet();
    }
}

uint8_t processSetCVValue() {
    uint8_t setCVIndex;

    if (((setCVValue) >= 0.0f) && (setCVValue < 0.125f)) {
        setCVIndex = 0;
    } else if (((setCVValue) >= 0.125f) && (setCVValue < 0.25f)) {
        setCVIndex = 1;
    } else if (((setCVValue) >= 0.25f) && (setCVValue < 0.375f)) {
        setCVIndex = 2;
    } else if (((setCVValue) >= 0.375f) && (setCVValue < 0.5f)) {
        setCVIndex = 3;
    } else if (((setCVValue) >= 0.5f) && (setCVValue < 0.625f)) {
        setCVIndex = 4;
    } else if (((setCVValue) >= 0.625f) && (setCVValue < 0.75f)) {
        setCVIndex = 5;
    } else if (((setCVValue) >= 0.75f) && (setCVValue < 0.875f)) {
        setCVIndex = 6;
    } else if (((setCVValue) >= 0.875f) && (setCVValue <= 1.0f)) {
        setCVIndex = 7;
    } else {
        setCVIndex = 0;
    }

    return setCVIndex;
}

/**
 * The main method for checking all of our interface things...display,
 * pins, pots, buttons.
 * 
*/
void checkInterface() {
    processChaosPins();
    //processCVs();
    processChaosIntensityPots();

    /*
    if (chaosCV.hasChanged()) {
        XENOKINETICS_DEBUG_PRINT("Chaos CV raw value now: ");
        chaosCVValue = chaosCV.getValue();
        XENOKINETICS_DEBUG_PRINTLN(chaosCVValue);
    }

    if (scaleCV.hasChanged()) {
        XENOKINETICS_DEBUG_PRINT("Scale CV raw value now: ");
        scaleCVValue = scaleCV.getValue();
        XENOKINETICS_DEBUG_PRINTLN(scaleCVValue);
    }
    */


    encoderButton.update();
    // If we have the falling edge...
    // update state machine
    if (encoderButton.fallingEdge()) {
        if (currentInterfaceState == STATE_SIGIL) {
            XENOKINETICS_DEBUG_PRINTLN("switching to state divisors");
            currentInterfaceState = STATE_DIVISORS;
            encoderKnob.write(encoderKnobValues[setIndex][currentVoice]);
        } else if ((currentInterfaceState == STATE_DIVISORS) && (currentVoice == (MAX_CYCLES - 1))) {
            XENOKINETICS_DEBUG_PRINTLN("switching to state sigil");
            currentInterfaceState = STATE_SIGIL;
            currentVoice = 0;
            encoderKnob.write(encoderKnobValues[setIndex][currentVoice]);
        } else if ((currentInterfaceState == STATE_DIVISORS) && (currentVoice != (MAX_CYCLES - 1))) {
            XENOKINETICS_DEBUG_PRINTLN("continuing in state divisors");
            currentVoice += 1;
            encoderKnob.write(encoderKnobValues[setIndex][currentVoice]);

        }
    }

    long newInputKnobValue = 0;

    // Read the input knob
    newInputKnobValue = encoderKnob.read();

    if (newInputKnobValue != encoderKnobValue) {
        encoderKnobValue = newInputKnobValue;
        //XENOKINETICS_DEBUG_PRINTLN(newInputKnobValue);
    }

    if (encoderKnobValue <= KNOB_MIN) {
        encoderKnobValue = KNOB_MIN;
    } else if (encoderKnobValue >= KNOB_MAX) {
        encoderKnobValue = KNOB_MAX;
    }

    processInterfaceState(currentInterfaceState);
    processDisplay();
    processButtons();
    processTrigGateSwitch();
}

uint16_t readADC(int adcChannel) {
// A12, A13, A14, A15 only on ADC2

    if ((adcChannel == A12) || (adcChannel == A13) || (adcChannel == A14) || (adcChannel == A15)) {
        return (uint16_t)(adc->adc1->analogRead(adcChannel));
    } else {
        return (uint16_t)(adc->adc0->analogRead(adcChannel));
    }
}

void printChaosIntensityPotValues() {
    for (int i = 0; i < MAX_CYCLES; i++) {
       XENOKINETICS_DEBUG_PRINT(chaosIntensityPotsValues[i]);
       XENOKINETICS_DEBUG_PRINT(" ");
    }

    XENOKINETICS_DEBUG_PRINTLN("");
}