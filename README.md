# selestium modular

Modules developed by selestium modular.

All modules have a `hardware` subfolder that contains the hardware schematics, PCB layours, and gerbers for the module.

If there is software for the module, that is available under the `software` folder.

## MULTIPLIER+

Dual 1 to 3 buffered mult. Output of top half normaled to the bottom half, so can be run as a 1 to 6 buffered mult.

3hp

## COMBINER

3 channel mixer.

3hp

## JOINER 8x

8x OR combiner.

2hp

## JOINER 3xby2

Dual 3x OR combiners.

2hp

## MANIFESTER

High-gain, high-impedance amplifier for biological signals.

? hp

## Xenokinetics

Code and hardware for the Xenokinetics Eurorack module.
