<!-- Being considered as part of the selestium, this chapter describes the JOINER3xby2 module: its motivations, its usage, and some suggested avenues of exploration. -->

*ARCHIVAL DISCLAIMER*: As is common with the other artifacts from the dig AG-105, we believe this "module" also comes from "selestium modular", the producer of a set of sound production modules from the early 21st century. For how to link the modules together, please refer to the appropriate chapters of the *selestium* or the æther-net. Our re-creation of it for this edition has tried to follow as closely as possible the design of the module found in the dig, but built using contemporary processes. As a result, there may be some minor deviations from the versions that are seen in the archives.

# Rationale

From our research we believe that JOINER3xby2 is what is called an "OR-combiner"; that is, it outputs the highest value amongst all of its inputs^[In the re-creation of the ancient æther-net, more information about OR-combiners can be found at this address, given in the ancient form: <https://doepfer.de/DIY/a100_diy.htm>.]. There are two versions of JOINER known to us: this one, JOINER3xby2, which has two sets of three inputs and one output, with normalization (explained below), and JOINER8x, which has 8 inputs and one output.

We believe the main use of JOINER3xby2 was to combine triggers or gates together, especially from the module that is often found co-located in digs called XENOKINETICS. Through some research and experimentation, we have found that JOINER3xby2 was constructed for those who wanted some of the benefits of JOINER8x but with fewer numbers of inputs. Not everyone needed 8 inputs to be combined together at once, and JOINER3xby2 would allow for two sets of OR-combinations to occur within the same horizontal space as JOINER8x.

Now, to explain a bit more about the layout of the module, based on our experimentation. The module is divided into two (nearly) identical halves. Both halves have three inputs which are OR-combined into one output. However, the output of the top OR-combiner is *normalized* into the input of the bottom OR-combiner. So, if nothing is input into the first input of the bottom OR-combiner, then "magically" the module becomes a 5 to 1 OR-combiner (that is, all three of the top inputs, and the last two of the bottom inputs). The output of this specialized 5 to 1 OR-combiner would come from the output of the bottom OR-combiner. Thus, JOINER3xby2 is a more versatile module than JOINER8x, as long as you don't need to combine 8 inputs together.

Yet combining triggers or gates was not the only thing the set of JOINER modules could do. If more complex signals were fed into the inputs--such as audio-rate signals or low-frequency oscillators--the resulting output could become quite complex. This was a non-standard usage of the JOINER modules but could be seen as an experimental mode.

As in all modules of the "passive" type--and we dislike their use of the word "passive", as these modules do quite a lot of work, and thus maybe a better way to describe it would be "un-powered"--there is a voltage drop on the output. This could cause issues if multiple JOINER modules were chained together. However, this can be mitigated through the use of what was called a "buffered multiple", and indeed, the "selestium modular" producers created such a module for exactly this purpose which they called "MULTIPLIER+". Check the relevant chapter of the *selestium* for more details.

# Datums

These are the measurements we have for the JOINER3xby8 module, in the units of the time:

* Width: 2HP (9,8mm)
* Depth: 15,2mm
* Power: unpowered
* *Note*: voltage drop on the output of around 0,6V

# Quick usage

As with all of the modules from "selestium modular" very little written text is found on the front of the modules; instead, custom iconography is present, the basics of which are found at the beginnings of each chapter in the *selestium* for the modules. In this section we describe some possible avenues of use and experimentation based on our research.

* Send trigger/gate signals into each of the inputs and use the output to trigger or gate another module to create complex poly-rhythms.
* Use the output poly-rhythmic signal as the clock for a delay module for warped delay sounds.
* Use this signal to also act as a clock for a sequencer.
* Send LFOs into the inputs and use the output as a hyper-complex LFO.
* Send audio signals into the inputs and use the output as a hyper-complex sound/noise source.
